const moment = require("moment");
const firebase = require("firebase");
import formatDayCharacter from "./FormatDayCharacter";

export default class DatabaseService {
  constructor() {
    require("./FirebaseInit").init(firebase);
    // Initialize Cloud Firestore through Firebase
    this.db = firebase.firestore();
    // Disable deprecated features
  }

  makeRandomId() {
    var text = "";
    var possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 15; i++)
      text += possible.charAt(Math.random() * possible.length);
    return text;
  }

  updateStudentAppointment(studId, profId, apptId, onFinish) {
    let that = this;
    return this.db
      .collection("students")
      .doc(studId)
      .update({
        appts: firebase.firestore.FieldValue.arrayUnion(
          that.db
            .collection("professors")
            .doc(profId)
            .collection("appointments")
            .doc(apptId)
        )
      });
  }

  pushAppointmentByDbRef(appointment, profId, cache = null, onFinish) {
    let apptId = this.makeRandomId();
    let that = this;
    console.log(appointment);
    var sessionRef = cache
      ? this.db
          .collection("professors")
          .doc(profId)
          .collection("cache")
          .doc(cache)
          .collection("sessions")
          .doc(appointment.id)
      : this.db
          .collection("professors")
          .doc(profId)
          .collection("sessions")
          .doc(appointment.id);

    this.db
      .collection("professors")
      .doc(profId)
      .collection("appointments")
      .doc(apptId)
      .set({
        //the ":00" is because it needs seconds
        end_time: firebase.firestore.Timestamp.fromDate(
          new Date(appointment.end + ":00")
        ),
        start_time: firebase.firestore.Timestamp.fromDate(
          new Date(appointment.start + ":00")
        ),
        course:
          appointment.course == ""
            ? "None"
            : this.db.collection("courses").doc(appointment.course),
        student: this.db.collection("students").doc(appointment.student),
        topic: appointment.topic,
        professor: profId,
        session: sessionRef,
        day: appointment.day
      })
      .then(a => {
        this.updateStudentAppointment(appointment.student, profId, apptId).then(
          () => {
            return onFinish(a);
          }
        );
      });
  }

  pushAppointment(appointment, onFinish) {
    this.db
      .collection("appointments")
      .add(appointment)
      .then(a => {
        onFinish(a);
      });
  }

  //gets the student of the logged in client
  getTypeOfLoggedInUser(email, onFinish) {
    var moment1 = moment();
    var that = this;
    this.db
      .collection("students")
      .where("email", "==", email)
      .get()
      .then(querySnapshot => {
        if (!querySnapshot.empty) {
          return onFinish("student");
        }
      })
      .then(function() {
        that.db
          .collection("professors")
          .where("email", "==", email)
          .get()
          .then(querySnapshot => {
            if (!querySnapshot.empty) {
              return onFinish("professor");
            }
          });
      });
  }

  //gets the student of the logged in client
  getLoggedInUser(email, onFinish) {
    var that = this;
    var user = null;
    return (
      this.db
        .collection("students")
        .where("email", "==", email)
        .get()
        .then(querySnapshot => {
          !querySnapshot.empty &&
            querySnapshot.docs.some(doc => {
              user = Object.assign({ type: "student", id: doc.id }, doc.data());
              onFinish(user);
              return true;
            });
          if (!user) {
            throw null;
          }
          return user;
        })
        //if not a student, error thrown and caught to go query profs
        .catch(() => {
          return that.db
            .collection("professors")
            .where("email", "==", email)
            .get()
            .then(querySnapshot => {
              !querySnapshot.empty &&
                querySnapshot.docs.some(doc => {
                  user = Object.assign(
                    { type: "professor", id: doc.id },
                    doc.data()
                  );
                  onFinish(user);
                  return true;
                });
              return user;
            });
        })
    );
  }

  //TODO: should just merge into getLoggedInUser (see above)
  //gets the student id of the logged in client
  getLoggedInUserID(email, onFinish) {
    var that = this;
    this.db
      .collection("students")
      .where("email", "==", email)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          if (doc.data().email === email) {
            return onFinish(doc.id);
          }
        });
      });
  }

  //gets all of the students in a class with the course id of 'courseId'
  getAllMembersOfCourse(courseId, onFinish) {
    this.db
      .collection("courses")
      .doc(String(courseId))
      .get()
      .then(docSnapshot => {
        if (docSnapshot.exists) {
          return onFinish(docSnapshot.data().members);
        }
      });
  }

  clearTheQueue(profId, onFinish) {
    this.getCurrentSession(profId, inSession => {
      var proms = [],
        studIds = [],
        queueIds = [];

      this.db
        .collection("queue")
        .doc(profId)
        .collection("visits")
        .get()
        .then(querySnapshot => {
          querySnapshot.forEach(doc => {
            if (inSession) {
              if (doc.data().session.id != inSession) {
                queueIds.push(doc.id);
                proms.push(doc.data().student.get());
              }
            } else {
              queueIds.push(doc.id);
              proms.push(doc.data().student.get());
            }
          });
        })
        .then(() => {
          return Promise.all(proms);
        })
        .then(queries => {
          for (var i = 0; i < queries.length; i++) {
            this.deleteStudentFromQueue(profId, queries[i].id, queueIds[i]);
          }
          return onFinish(true);
        });
    });
  }

  //gets all of the students in a class with the course id of 'courseId'
  getAllMembersEmailOfCourse(courseId, onFinish) {
    var proms = [],
      emails = [];
    this.db
      .collection("courses")
      .doc(String(courseId))
      .get()
      .then(docSnapshot => {
        if (docSnapshot.exists) {
          for (var mem in docSnapshot.data().members) {
            proms.push(docSnapshot.data().members[mem].get());
          }
        }
      })
      .then(() => {
        return Promise.all(proms);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          emails.push(queries[i].data().email);
        }
        return onFinish(emails);
      });
  }

  // TODO: remove?
  //returns the last person in queue
  getLastInQueue(profId) {
    var last = null;
    return this.db
      .collection("queue")
      .doc(profId)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          //if there is no student in the queue
          if (doc === null) {
            return null;
          } else if (last === null) {
            //if there is someone in the queue, and this is the first doc seen
            last = doc;
          } else {
            if (last.data().place_in_line < doc.data().place_in_line) {
              last = doc;
            }
          }
        });
        return last;
      });
  }

  updateStudentQueue(studId, profId, queueId) {
    //alert(studId, profId, queueId);
    var that = this;
    return this.db
      .collection("students")
      .doc(studId)
      .update({
        queue: firebase.firestore.FieldValue.arrayUnion(
          //this.db.collection("professors").doc(profId)
          that.db
            .collection("queue")
            .doc(profId)
            .collection("visits")
            .doc(queueId)
        )
      });
  }

  getHighestTicketNumber(profId, onFinish) {
    var highest = null;
    this.db
      .collection("queue")
      .doc(profId)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          //if there is no student in the queue
          if (doc === null) {
            return onFinish(null);
          } else if (highest === null) {
            //if there is someone in the queue, and this is the first doc seen
            highest = doc;
          } else {
            if (highest.data().ticket_number < doc.data().ticket_number) {
              highest = doc;
            }
          }
        });
        return onFinish(highest);
      });
  }

  //add the student 'studId' to the live queue (not an appt), with the topic and course id
  addStudentToQueue(studId, topic, courseId, profId, onFinish) {
    let queueId = this.makeRandomId();
    var lastTicketNumber = 1;
    var lastPlaceInLine = 1;
    var that = this;
    this.getCurrentSession(profId, sessionId => {
      if (!sessionId) {
        return false;
      }
      return this.getLastInQueue(profId)
        .then(function(last) {
          that.getHighestTicketNumber(profId, tix_number => {
            //if there isnt anyone in the queue
            if (last != null) {
              lastPlaceInLine = Number(last.data().place_in_line) + 1;
            }
            if (tix_number != null) {
              lastTicketNumber = Number(tix_number.data().ticket_number) + 1;
            }
            if (courseId == "None" || courseId == "") {
              return that.db
                .collection("queue")
                .doc(profId)
                .collection("visits")
                .doc(queueId)
                .set({
                  session: that.db
                    .collection("professors")
                    .doc(profId)
                    .collection("sessions")
                    .doc(sessionId),
                  professor: profId,
                  student: that.db.collection("students").doc(studId),
                  timestamp: firebase.firestore.FieldValue.serverTimestamp(),
                  ticket_number: lastTicketNumber,
                  place_in_line: lastPlaceInLine,
                  topic: topic
                });
            } else {
              return that.db
                .collection("queue")
                .doc(profId)
                .collection("visits")
                .doc(queueId)
                .set({
                  course: that.db.collection("courses").doc(courseId),
                  professor: profId,
                  session: that.db
                    .collection("professors")
                    .doc(profId)
                    .collection("sessions")
                    .doc(sessionId),
                  student: that.db.collection("students").doc(studId),
                  timestamp: firebase.firestore.FieldValue.serverTimestamp(),
                  ticket_number: lastTicketNumber,
                  place_in_line: lastPlaceInLine,
                  topic: topic
                });
            }
          });
        })
        .then(a => {
          onFinish(this.updateStudentQueue(studId, profId, queueId));
        });
    });
  }

  getCurrentSession(profId, onFinish) {
    let that = this;
    this.getProfessorSessionsCacheForDay(profId, moment().day()).then(
      querySnapshot => {
        if (querySnapshot.empty) {
          this.getDaySessions(profId, formatDayCharacter(moment())).then(
            recurringSnapshot => {
              var doc = recurringSnapshot.docs.find(doc => {
                return (
                  moment().format("HHmm") >= doc.data().start &&
                  moment().format("HHmm") <= doc.data().end
                );
              });
              if (doc) {
                return onFinish(doc.id);
              } else {
                return onFinish(false);
              }
            }
          );
        } else {
          var doc = querySnapshot.docs.find(doc => {
            return (
              moment().format("HHmm") >= doc.data().start &&
              moment().format("HHmm") <= doc.data().end
            );
          });
          if (doc) {
            return onFinish(doc.id);
          } else {
            return onFinish(false);
          }
        }
      }
    );
  }

  getCurrentSessionWholeDoc(profId, onFinish) {
    let that = this;
    this.getProfessorSessionsCacheForDay(profId, moment().day()).then(
      querySnapshot => {
        if (querySnapshot.empty) {
          this.getDaySessions(profId, formatDayCharacter(moment())).then(
            recurringSnapshot => {
              var doc = recurringSnapshot.docs.find(doc => {
                return (
                  moment().format("HHmm") >= doc.data().start &&
                  moment().format("HHmm") <= doc.data().end
                );
              });
              if (doc) {
                return onFinish(doc);
              } else {
                return onFinish(false);
              }
            }
          );
        } else {
          var doc = querySnapshot.docs.find(doc => {
            return (
              moment().format("HHmm") >= doc.data().start &&
              moment().format("HHmm") <= doc.data().end
            );
          });
          if (doc) {
            return onFinish(doc);
          } else {
            return onFinish(false);
          }
        }
      }
    );
  }

  /**
   * deleteStudentFromQueue - delete given student from given professor's queue
   *
   * @param  {String} professorId id of professor who's queue the student belongs to
   * @param  {String} studentId   id of student to be deleted
   *
   */
  deleteStudentFromQueue(professorId, studentId, queueId) {
    const that = this;
    var doc_to_delete;
    var place_in_line_deleted = 0;
    // find the schedule for the given student id for the given professor id
    return this.db
      .collection("queue")
      .doc(professorId)
      .collection("visits")
      .doc(queueId)
      .get()
      .then(doc => {
        doc_to_delete = doc;
        place_in_line_deleted = doc.data().place_in_line;
      })
      .then(() => {
        // 'some()' is mainly needed for testing, should only over be one in application
        // finds the matching person in the prof's queue session who is NOT an appointment

        // copies visit data to historical_visits
        return that.db
          .collection("historical_visits")
          .doc(professorId)
          .collection("visits")
          .doc()
          .set(doc_to_delete.data())
          .then(() => {
            // deletes current visit, since it is finished
            return that.db
              .collection("queue")
              .doc(professorId)
              .collection("visits")
              .doc(doc_to_delete.id)
              .delete()
              .then(function() {
                return that.db
                  .collection("queue")
                  .doc(professorId)
                  .collection("visits")
                  .where("place_in_line", ">", place_in_line_deleted)
                  .get()
                  .then(querySnapshot => {
                    querySnapshot.forEach(doc => {
                      that.db
                        .collection("queue")
                        .doc(professorId)
                        .collection("visits")
                        .doc(doc.id)
                        .update({
                          place_in_line: doc.data().place_in_line - 1
                        });
                    });
                  })
                  .then(() => {
                    return that.db
                      .collection("students")
                      .doc(studentId)
                      .update({
                        queue: firebase.firestore.FieldValue.arrayRemove(
                          that.db
                            .collection("queue")
                            .doc(professorId)
                            .collection("visits")
                            .doc(queueId)
                        )
                      });
                  });
              })
              .catch(function(error) {
                console.error("Error removing document: ", error);
              });
          });
        return true;
      });
  }

  //todo: is this function needed?
  sendRequestMessage(req) {
    req.timestamp = firebase.firestore.Timestamp.now();
    this.db
      .collection("server_requests")
      .doc()
      .set(req);
  }

  // TODO: remove?
  //returns the first person in the queue
  getFirstInQueue(profId, onFinish) {
    var first = null;
    this.db
      .collection("queue")
      .doc(profId)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          //if there is no student in the queue
          if (doc === null) {
            return onFinish(null);
          } else if (first === null) {
            //if there is someone in the queue, and this is the first doc seen
            first = doc;
          } else {
            if (first.data().place_in_line > doc.data().place_in_line) {
              first = doc;
            }
          }
        });
      })
      .then(function() {
        return onFinish(first);
      });
  }

  //push back a student visit 'visitId' that number of spots
  pushbackStudent(profId, numSpots) {
    var that = this;
    var firstVisit;
    that.getFirstInQueue(profId, function(first) {
      that.db
        .collection("queue")
        .doc(profId)
        .collection("visits")
        .get()
        .then(querySnapshot => {
          querySnapshot.forEach(doc => {
            firstVisit = first;
            if (1 < doc.data().place_in_line) {
              if (numSpots + 1 >= doc.data().place_in_line) {
                that.db
                  .collection("queue")
                  .doc(profId)
                  .collection("visits")
                  .doc(doc.id)
                  .update({
                    place_in_line: doc.data().place_in_line - 1
                  })
                  .then(function() {
                    that.db
                      .collection("queue")
                      .doc(profId)
                      .collection("visits")
                      .doc(firstVisit.id)
                      .update({
                        place_in_line:
                          firstVisit.data().place_in_line + numSpots
                      });
                  });
              }
            }
          });
        });
    });
  }

  getName(email, onFinish) {
    this.getLoggedInUser(email, function(user) {
      return onFinish(user.name);
    });
  }

  getNameOfProf(email, onFinish) {
    return this.db
      .collection("prof_metadata")
      .doc(email)
      .get()
      .then(doc => {
        return onFinish(doc.data().name);
      });
  }

  // recurring hours
  getAllSessions(profId) {
    return this.db
      .collection("professors")
      .doc(profId)
      .collection("sessions")
      .get();
  }

  /**
   * getDaySession - get's specific day of week's session
   *
   * @param  {String} profId description
   * @param  {String} day    character of day desired
   * @return {type}        description
   */
  getDaySessions(profId, day) {
    return this.db
      .collection("professors")
      .doc(profId)
      .collection("sessions")
      .where("day", "==", day)
      .get();
  }

  //returns the topics of all the historical visits
  getTopicsFromHistoricalVisits(prof_id, onFinish) {
    this.db
      .collection("historical_visits")
      .doc(prof_id)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        var arr = [];
        querySnapshot.forEach(doc => {
          arr.push(doc.data().topic);
        });
        let unique = [...new Set(arr)];
        return onFinish(unique);
      });
  }

  //Takes an input string and returns all professors containing that sabstring
  //MAY NEED TO CLEANSE INPUT FOR SECURITY PURPOSES
  //CHANGE TO METADATA
  testSearchStringAgaintsProfs(search_string, onFinish) {
    var nameArray = [];
    this.db
      .collection("professors")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          if (
            doc
              .data()
              .name.toLowerCase()
              .includes(search_string.toLowerCase())
          ) {
            nameArray.push(doc.data().name);
          }
        });
        onFinish(nameArray);
      });
    //return nameArray;
  }

  //gets prof name from id
  getProfUsingID(profID, onFinish) {
    this.db
      .collection("prof_metadata")
      .doc(profID)
      .get()
      .then(doc => {
        return onFinish(doc.data().name);
      });
  }

  //gets prof id from name
  getProfIdUsingName(profName, onFinish) {
    this.db
      .collection("prof_metadata")
      .where("name", "==", profName)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          if (doc.data().name == profName) {
            onFinish(doc.id);
          }
        });
      });
  }

  getMomentsForWeekWithDate(date) {
    var sunday = date.startOf("week");
    var dates = [];
    //gets day for M - F
    for (var i = 0; i < 5; i++) {
      dates.push(sunday.clone().add(i + 1, "d"));
    }
    return dates;
  }

  /**
   * getProfessorSessionsCache - return professors cache for the week range which includes 'day'
   *
   * @param  {String} profId
   * @param  {String} day day whose week we are finding
   * @return {Promise([querySnapshots])}
   */
  getProfessorSessionsCacheForWeekWithDate(profId, date) {
    const dates = this.getMomentsForWeekWithDate(date);
    // makes array of tasks to get all sessions for each day
    var promises = dates.map(day =>
      this.db
        .collection("professors")
        .doc(String(profId))
        .collection("cache")
        .doc(day.format("DDDD-YY"))
        .collection("sessions")
        .get()
    );
    return Promise.all(promises);
  }

  getPromiseMapForWeekWithDateFunction(func, profId, date) {
    const dates = this.getMomentsForWeekWithDate(date);

    // given a range of dates, call a 'firestore promise'-returning function for each date
    //    when that function finishes, add that snapshot to a map of the dates
    const promiseMap = dates.reduce((acc, d) => {
      const dFormatted = d.format();
      acc[dFormatted] = func.call(this, profId, d).then(snapshot => {
        acc[dFormatted] = snapshot;
      });
      return acc;
    }, {});

    //return a promise that finishes once each days' promise completes
    return Promise.all(Object.values(promiseMap)).then(() => promiseMap);
  }

  getAdditionsForWeekWithDate(profId, date) {
    return this.getPromiseMapForWeekWithDateFunction(
      this.getAdditionsForDate,
      profId,
      date
    );
  }

  getDeletionsForWeekWithDate(profId, date) {
    return this.getPromiseMapForWeekWithDateFunction(
      this.getDeletionsForDate,
      profId,
      date
    );
  }

  getProfAppointmentsForWeek(profId, todayChar) {
    var days = ["M", "T", "W", "R", "F"];
    //remove everything before today because those appointments are now irrelevant
    days.splice(0, days.indexOf(todayChar));
    //refer to getPromiseMapForWeekWithDateFunction for explanation on what this does
    const promiseMap = days.reduce((acc, d) => {
      acc[d] = this.getProfessorApptsForDay(profId, d).then(snapshot => {
        acc[d] = snapshot;
      });
      return acc;
    }, {});
    return Promise.all(Object.values(promiseMap)).then(() => promiseMap);
  }

  /**
   * getProfessorSessionsCache - return professors cache for the week range which includes 'day'
   *
   * @param  {String} profId
   * @param  {String} day index of the day of the week we need
   * @return {Promise([querySnapshots])}
   */
  getProfessorSessionsCacheForDay(profId, day) {
    var targetDay = moment().day(day);
    // makes array of tasks to get all sessions for each day
    return this.db
      .collection("professors")
      .doc(String(profId))
      .collection("cache")
      .doc(targetDay.format("DDDD-YY"))
      .collection("sessions")
      .get();
  }

  addProfToFavorites(studId, profId) {
    this.db
      .collection("students")
      .doc(studId)
      .update({
        favorites: firebase.firestore.FieldValue.arrayUnion(
          this.db.collection("professors").doc(profId)
        )
      });
  }

  //removes a prof from students favorites
  removeProfFromFavorites(studId, profId) {
    this.db
      .collection("students")
      .doc(studId)
      .update({
        favorites: firebase.firestore.FieldValue.arrayRemove(
          this.db.collection("professors").doc(profId)
        )
      });
  }

  //returns the favorites of a the student id passed in

  getStudentFavorites(studId, onFinish) {
    var favArr = [];
    this.db
      .collection("students")
      .doc(studId)
      .get()
      .then(doc => {
        doc.data().favorites.forEach(fav => {
          favArr.push(fav.id);
        });
        return onFinish(favArr);
      });
  }

  //returns true if student has this prof favorited already
  isFavorited(studentID, profID, onFinish) {
    this.db
      .collection("students")
      .doc(studentID)
      .get()
      .then(doc => {
        doc.data().favorites.forEach(fav => {
          if (fav.id == profID) {
            return onFinish(true);
          }
        });
      });
    return onFinish(false);
  }

  getProfAppointments(profID, onFinish) {
    var appts = [];
    var proms = [];
    this.db
      .collection("professors")
      .doc(profID)
      .collection("appointments")
      .orderBy("start_time", "asc")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          appts.push([
            doc.id,
            doc.data().start_time.toDate(),
            doc.data().topic,
            doc.data().end_time.toDate()
          ]);
          proms.push(doc.data().student.get());
        });
      })
      .then(() => {
        return Promise.all(proms);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          appts[i].push(queries[i].data().name);
        }
        return onFinish(appts);
      });
  }

  getProfAppointmentsOfWeek(profID, onFinish) {
    var appts = [];
    var proms = [];
    this.db
      .collection("professors")
      .doc(profID)
      .collection("appointments")
      .where("start_time", ">", moment().toDate())
      .orderBy("start_time", "asc")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          appts.push([
            doc.id,
            doc.data().start_time.toDate(),
            doc.data().topic,
            doc.data().end_time.toDate()
          ]);
          proms.push(doc.data().student.get());
        });
      })
      .then(() => {
        return Promise.all(proms);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          appts[i].push(queries[i].data().name);
        }
        return onFinish(appts);
      });
  }

  /**
   * getAdditionsForDate
   *
   * @param  {String} professorId
   * @param  {String} date
   * @return {Promise(querySnapshot)}
   */

  getAdditionsForDate(professorId, date) {
    return this.db
      .collection("professors")
      .doc(String(professorId))
      .collection("cache")
      .doc(date.format("DDDD-YY"))
      .collection("additions")
      .get();
  }

  /**
   * getDeletionsForDate
   *
   * @param  {String} professorId
   * @param  {String} date
   * @return {Promise(querySnapshot)}
   */

  getDeletionsForDate(professorId, date) {
    return this.db
      .collection("professors")
      .doc(String(professorId))
      .collection("cache")
      .doc(date.format("DDDD-YY"))
      .collection("deletions")
      .get();
  }

  /**
   * updateAdditions - update professors addition table
   *
   * @param  {String} professorId
   * @param  {JSON Array} additionList
   * @return {Promise(?)}
   */
  updateAdditions(professorId, additionList) {
    var promises = [];
    for (var day in additionList) {
      for (var id in additionList[day]) {
        const addition = additionList[day][id];
        var addCollection = this.db
          .collection("professors")
          .doc(String(professorId))
          .collection("cache")
          .doc(day)
          .collection("additions");
        var entry = {
          start: addition.start,
          end: addition.end,
          day: addition.day
        };
        //var task = addition.isNew ? addCollection.doc() : addCollection.doc(id);
        promises.push(addCollection.doc(id).set(entry));
      }
    }
    return Promise.all(promises);
  }

  /**
   * updateDeletions - update professors deletions table
   *
   * @param  {String} professorId
   * @param  {JSON Array} deletionList
   * @return {Promise(?)}
   */
  updateDeletions(professorId, deletionList) {
    var promises = [];
    for (var day in deletionList) {
      for (var id in deletionList[day]) {
        const deletion = deletionList[day][id];
        var delCollection = this.db
          .collection("professors")
          .doc(String(professorId))
          .collection("cache")
          .doc(day)
          .collection("deletions");

        //var task = deletion.isNew ? delCollection.doc() : delCollection.doc(id);
        promises.push(
          delCollection.doc(id).set({ deleting: deletion.deleting })
        );
      }
    }
    return Promise.all(promises);
  }

  /**
   * updateAdditions - update professors cache table
   *    only updates days present in caches, assumes proper format
   *
   * @param  {String} professorId
   * @param  {JSON Array} caches
   * @return {Promise(?)}
   */
  updateCache(professorId, caches) {
    // TODO: double check this is correct
    //    reasoning - assumedly should work, all edited caches are stored during the editing session
    //    all are formatted properly (day, start, end, and outer id of day-year)

    var cacheDeletionPromises = [];
    var cacheDeletionDayPromises = [];
    // cleanse all accessed caches
    for (var day in caches) {
      cacheDeletionDayPromises.push(
        this.db
          .collection("professors")
          .doc(String(professorId))
          .collection("cache")
          .doc(day)
          .collection("sessions")
          .get()
          .then(querySnapshot => {
            querySnapshot.docs.forEach(doc => {
              cacheDeletionPromises.push(doc.ref.delete());
            });
          })
      );
      // an extra check to make document real
      this.db
        .collection("professors")
        .doc(String(professorId))
        .collection("cache")
        .doc(day)
        .set({});
    }

    //when all docs have been deleted
    //repopulate all accessed caches with given data
    const additions = () => {
      for (var day in caches) {
        for (var sessionId in caches[day]) {
          const session = caches[day][sessionId];
          var cacheCollection = this.db
            .collection("professors")
            .doc(String(professorId))
            .collection("cache")
            .doc(day)
            .collection("sessions");
          cacheCollection.doc(sessionId).set({
            start: session.start,
            end: session.end,
            day: session.day
          });
        }
      }
    };

    return Promise.all(cacheDeletionDayPromises)
      .then(Promise.all(cacheDeletionPromises))
      .then(additions);
  }

  getStudentQueueNumber(studentId, professorId) {
    return this.db
      .collection("queue")
      .doc(String(professorId))
      .collection("visits")
      .where(
        "student",
        "==",
        this.db.collection("students").doc(String(studentId))
      )
      .get()
      .then(querySnapshot => {
        if (querySnapshot.docs.length) {
          return querySnapshot.docs
            .find(doc => {
              return doc.exists;
            })
            .data().ticket_number;
        }
        return null;
      });
  }

  deleteStudentAppointment(profId, apptId) {
    //get the student id before you delete it
    let that = this;
    var prom = [],
      studId;
    return this.db
      .collection("professors")
      .doc(profId)
      .collection("appointments")
      .doc(apptId)
      .get()
      .then(doc => {
        prom.push(doc.data().student.get());
      })
      .then(() => {
        return Promise.all(prom);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          studId = queries[i].id;
        }
        //once we have the studId, we are all good
        return this.db
          .collection("professors")
          .doc(profId)
          .collection("appointments")
          .doc(apptId)
          .delete()
          .then(function() {
            console.log("Document successfully deleted!");
            // Atomically remove a appt from the appt array
            return that.db
              .collection("students")
              .doc(studId)
              .update({
                appts: firebase.firestore.FieldValue.arrayRemove(
                  that.db
                    .collection("professors")
                    .doc(profId)
                    .collection("appointments")
                    .doc(apptId)
                )
              });
          })
          .catch(function(error) {
            console.error("Error removing document: ", error);
          });
      });
  }

  getApptOfAStudent(studId, onFinish) {
    var that = this;
    var apptProms = [],
      appts = [];
    that.db
      .collection("students")
      .doc(studId)
      .get()
      .then(docSnapshot => {
        docSnapshot.data().appts.forEach(appt => {
          apptProms.push(appt.get());
        });
      })
      .then(() => {
        return Promise.all(apptProms);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          appts.push({ id: queries[i].id, data: queries[i].data() });
        }
        return onFinish(appts);
      });
  }

  getQueuesOfAStudent(studId, onFinish) {
    var that = this;
    var qProms = [],
      qs = [];
    that.db
      .collection("students")
      .doc(studId)
      .get()
      .then(docSnapshot => {
        if (docSnapshot.data().queue) {
          docSnapshot.data().queue.forEach(q => {
            qProms.push(q.get());
          });
        }
      })
      .then(() => {
        return Promise.all(qProms);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          qs.push({ id: queries[i].id, data: queries[i].data() });
        }
        return onFinish(qs);
      });
  }

  getCoursesOfstudent(email, onFinish) {
    var courseArray = [],
      proms = [],
      docIds = [];
    this.db
      .collection("courses")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          docIds.push(doc.id);
          proms.push(doc.data().members.get());
        });
      })
      .then(() => {
        return Promise.all(proms);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          if (queries[i].data().email == email) {
            courseArray.push(docIds[i]);
          }
        }
        return onFinish(courseArray);
      });
  }

  getScheduleOfStudent(studId, onFinish) {
    var courseArray = [],
      timeArray = [],
      that = this,
      innerProm = [];
    return this.db
      .collection("schedules")
      .doc(studId)
      .get()
      .then(doc => {
        for (var i = 0; i < doc.data().courses.length; i++) {
          innerProm.push(
            that.db
              .collection("courses")
              .doc(doc.data().courses[i].id)
              .get()
          );
        }
      })
      .then(() => {
        return Promise.all(innerProm);
      })
      .then(listOfQueries => {
        listOfQueries.forEach(query => {
          for (var i = 0; i < query.data().sessions[0].days.length; i++) {
            timeArray.push([
              query.data().sessions[0].start,
              query.data().sessions[0].days[i]
            ]);
          }
        });
        return timeArray;
      });
  }

  getNextAppointment(profId, onFinish) {
    var appt = [],
      proms = [];
    this.db
      .collection("professors")
      .doc(profId)
      .collection("appointments")
      .where("start_time", ">", moment().toDate())
      .orderBy("start_time", "asc")
      .limit(1)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          appt.push([
            doc.id,
            doc.data().start_time.toDate(),
            doc.data().topic,
            doc.data().end_time.toDate()
          ]);
          proms.push(doc.data().student.get());
        });
      })
      .then(() => {
        return Promise.all(proms);
      })
      .then(queries => {
        for (var i = 0; i < queries.length; i++) {
          appt[i].push(queries[i].data().name);
        }
        return onFinish(appt);
      });
  }

  getNumOfPeopleInLine(profId, onFinish) {
    this.db
      .collection("queue")
      .doc(profId)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        var count = 0;
        querySnapshot.forEach(doc => {
          count++;
        });
        return onFinish(count);
      });
  }

  getStudUsingID(studEmail, onFinish) {
    this.db
      .collection("students")
      .where("email", "==", studEmail)
      .get()
      .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
          return onFinish(doc.id);
        });
      });
  }

  getAllProfsCourses(profId, onFinish) {
    var storeCourses = [];
    this.db
      .collection("courses")
      .where("professor", "==", this.db.collection("professors").doc(profId))
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          if (doc.data().professor.id == profId) {
            storeCourses.push(doc.id);
          }
        });
        return onFinish(storeCourses);
      });
  }

  getListOfHistoricalTopics(
    profId,
    selectedCourse,
    selectedTime,
    now,
    onFinish
  ) {
    var storeTopics = {};
    var startRange;
    var endRange = now.clone();
    switch (selectedTime) {
      case "week":
        startRange = now.subtract(7, "days");
        break;
      case "month":
        startRange = now.subtract(1, "months");
        break;
      case "semester":
        startRange = now.subtract(5, "months");
        break;
      default:
    }
    this.db
      .collection("historical_visits")
      .doc(profId)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          //check course first
          if (doc.data().course) {
            if (
              selectedCourse.toLowerCase() == doc.data().course.id.toLowerCase()
            ) {
              //then check time range
              var visitTime = doc.data().timestamp.toDate();
              var visitMoment = moment(visitTime);
              if (visitMoment.isBetween(startRange, endRange)) {
                if (!(doc.data().topic in storeTopics)) {
                  storeTopics[doc.data().topic.toLowerCase()] = 1;
                } else {
                  storeTopics[doc.data().topic.toLowerCase()] =
                    storeTopics[doc.data().topic.toLowerCase()] + 1;
                }
              }
            }
          }
        });
        return onFinish(storeTopics);
      });
  }

  getProfsOfficeHours(profId, onFinish) {
    class officeHour {
      constructor(day, startTime, endTime) {
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
      }
    }
    var profOfficeHourSessions = [];
    //first get all the office hours sessions that the prof has
    this.db
      .collection("professors")
      .doc(profId)
      .collection("sessions")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          var day;
          switch (doc.data().day) {
            case "U":
              day = "Sunday";
              break;
            case "M":
              day = "Monday";
              break;
            case "T":
              day = "Tuesday";
              break;
            case "W":
              day = "Wednesday";
              break;
            case "R":
              day = "Thursday";
              break;
            case "F":
              day = "Friday";
              break;
            case "S":
              day = "Saturday";
              break;
          }

          var start = moment(doc.data().start, "HHmm");
          start.day(day);
          var end = moment(doc.data().end, "HHmm");
          end.day(day);
          var insertSlot = new officeHour(start.day(), start, end);
          profOfficeHourSessions.push(insertSlot);
        });
        return onFinish(profOfficeHourSessions);
      });
  }

  getListOfHistoricalOfficeHourVisits(
    profId,
    selectedTime,
    profSessions,
    courses,
    now,
    onFinish
  ) {
    var startRange;
    var endRange = now.clone();
    switch (selectedTime) {
      case "week":
        startRange = now.subtract(7, "days");
        break;
      case "month":
        startRange = now.subtract(1, "months");
        break;
      case "semester":
        startRange = now.subtract(5, "months");
        break;
      default:
    }

    var sortedVisits = {};
    for (var i = 0; i < profSessions.length; i++) {
      var key =
        profSessions[i].day +
        " " +
        profSessions[i].startTime.hour() +
        "-" +
        profSessions[i].endTime.hour();
      sortedVisits[key] = {};
      for (var j = 0; j < courses.length; j++) {
        var key2 = courses[j];
        sortedVisits[key][key2] = 0;
      }
    }

    //next get all the historical visits for that prof and sort them
    this.db
      .collection("historical_visits")
      .doc(profId)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          var visitMoment = moment(doc.data().timestamp.toDate());
          if (visitMoment.isBetween(startRange, endRange)) {
            for (var i = 0; i < profSessions.length; i++) {
              //test day
              if (visitMoment.day() == profSessions[i].day) {
                //test if in endRange
                if (
                  visitMoment.hour() >= profSessions[i].startTime.hour() &&
                  visitMoment.hour() <= profSessions[i].endTime.hour()
                ) {
                  var key =
                    profSessions[i].day +
                    " " +
                    profSessions[i].startTime.hour() +
                    "-" +
                    profSessions[i].endTime.hour();
                  if (doc.data().course) {
                    sortedVisits[key][doc.data().course.id] =
                      sortedVisits[key][doc.data().course.id] + 1;
                  }
                }
              }
            }
          }
        });
        //console.log(sortedVisits);
        return onFinish(sortedVisits);
      });
  }

  getSessionDataById(profId, sessionId, onFinish) {
    this.db
      .collection("professors")
      .doc(profId)
      .collection("sessions")
      .doc(sessionId)
      .get()
      .then(doc => {
        return onFinish(doc.data());
      });
  }

  doesProfUseAppts(profId, onFinish) {
    this.db
      .collection("professors")
      .doc(profId)
      .get()
      .then(doc => {
        return onFinish(doc.data().settings.appts_enabled);
      });
  }

  changeProfUseAppts(profId, state) {
    this.db
      .collection("professors")
      .doc(profId)
      .update({
        settings: {
          appts_enabled: state
        }
      })
      .then(function() {
        console.log("Document successfully updated!");
      });
  }

  getProfessorApptsForDay(profId, dayCharacter) {
    return this.db
      .collection("professors")
      .doc(profId)
      .collection("appointments")
      .where("day", "==", dayCharacter)
      .get();
  }

  updateAppointments(profId, appointments) {
    var promises = [];
    for (var day in appointments) {
      const dayAppts = appointments[day];
      var dayPromises = Object.keys(dayAppts).flatMap(k => {
        const apptRef = this.db
          .collection("professors")
          .doc(profId)
          .collection("appointments")
          .doc(k);
        return [
          apptRef.delete(),
          dayAppts[k].student.update({
            appts: firebase.firestore.FieldValue.arrayRemove(apptRef)
          })
        ];
      });
      promises.push(Promise.all(dayPromises));
    }
    return promises;
  }

  getStudentInfo(studId) {
    return this.db
      .collection("students")
      .doc(studId)
      .get()
      .then(doc => doc.data());
  }
}
