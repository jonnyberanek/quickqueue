import DatabaseService from "./DatabaseService";

//this system assumes a two user type system
// if not the one user type, must be the other
class UserAuthService {
  notLoggedInRedirect = "/login";

  /**
   * @param  {Firebase} firebase
   * @param  {String} userType
   * @param  {String} incorrectRedirect
   */
  constructor(firebase, userType, incorrectRedirect) {
    this.auth = firebase.auth();
    this.dbs = new DatabaseService();
    this.incorrectRedirect = incorrectRedirect;
    this.userType = userType;
  }

  onValidUserLoad(callback) {
    this.validUserLoaded = callback;
    //call this here, since there is actually a callback for its completion now
    this.auth.onAuthStateChanged(this.authStateChanged);
  }

  validUserLoaded(user) {}

  authStateChanged = user => {
    // user is not null if signed in
    if (user) {
      // get user
      this.dbs
        .getLoggedInUser(user.email, () => {})
        .then(user => {
          if (!user) {
            window.location.href = this.notLoggedInRedirect;
          }
          //if not correct type, redirect to proper homepage
          if (user.type !== this.userType) {
            window.location.href = this.incorrectRedirect;
          }
          //else find the user and return it's info for the page to use
          else {
            this.validUserLoaded(user);
          }
        });
    }
    // redirect if user DNE
    else {
      window.location.href = this.notLoggedInRedirect;
    }
  };
}

export default UserAuthService;
