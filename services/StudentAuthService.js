import UserAuthService from "./UserAuthService";

class StudentAuthService extends UserAuthService {
  constructor(firebase) {
    super(firebase, "student", "/professor/queue");
  }
}

export default StudentAuthService;
