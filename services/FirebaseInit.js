module.exports.init = firebase => {
  //INITIALIZE A DB INSTANCE
  if (!firebase.apps.length) {
    firebase.initializeApp({
      apiKey: "AIzaSyAPzUTtsItVS5P9m1x4A4P72mXXh5D6i-o",
      authDomain: "quickqueue-dev-fd630.firebaseapp.com",
      projectId: "quickqueue-dev-fd630"
    });

    firebase
      .firestore()
      .enablePersistence()
      .catch(function(err) {
        if (err.code) {
          console.error(`Could not enable persistence: ${err.code}`);
        }
      });
  }
};
