import UserAuthService from "./UserAuthService";

class ProfessorAuthService extends UserAuthService {
  constructor(firebase) {
    super(firebase, "professor", "/student/home");
  }
}

export default ProfessorAuthService;
