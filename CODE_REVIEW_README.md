# QuickQueue - Code Review Instructions

<!-- TOC -->

- [Set-up](#set-up)
- [Log-in credentials and General Info](#log-in-credentials-and-general-info)
- [File Structure](#file-structure)

<!-- /TOC -->

**Note:** For a quick peak, the live, public web app can be found here: https://quickqueue-dev-fd630.firebaseapp.com

### Set-up

1. Make sure npm is installed
2. Double-check that your gmail account was granted dev access to the Firebase project.
3. Either from the file explorer or command line, run `full-setup.bat`  
   a. The first few commands should take a few minutes  
   b. After that, you'll be asked to log in to firebase (USE THE ACCOUNT FROM STEP 2)  
   c. The last step will set the server live at http://localhost:5000

To rerun any part of this process, run the commands below:

> `npm run <command>` where command is:  
> `login` - Logs out of firebase-cli and logs back in asking for an account in the browser  
> `build` - compiles project, you shouldn't need to run this again  
> `serve` - equiv. to `firebase serve`; hosts the website on your computer

To run the test code, run `npm run jest`.

### Log-in credentials and General Info

Professor credentials (Dr. Wolfe):

> user: w@w.com
>
> pass: password

Student credentials (Karen):

> user: kp@kp.com
>
> pass: password

Dead links:

- Professor 'Appointments' tab
- Professor 'Announce' tab
- Clicking Student's "search professor" results
  - (Favorites above search bar work, though)
- Clicking professor office hours on student side (gives alert)

Quirks:

- Student side: No professors have populated hours except for Dr. Wolfe
- Professor side: Click the classes in availability to expand a list of when students are available (denoted by numbers with times)

### File Structure

The file structure should be straightforward:

- Almost irrelevant is the server logic file (`index.js`), located in `/functions`
- All page indexes and are located in `/public` in their respective folders
  - Don't worry about the `bundle.js` files, they are compiled, uglified, auto-generated files.
- All React components are in `/src/components`
  - (most) associated styles are in `/src/style`
- General use, non-React js files are in `/services`
