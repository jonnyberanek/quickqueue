import React, { Component } from "react";
import "./App.css";
import Queue from "./components/studentQueue.jsx";
import LogoutButton from "./components/logoutButton.jsx";

class App extends Component {
  render() {
    return (
      <span style={{ margin: "auto", display: "table" }}>
        <Queue />
        <LogoutButton styling={{ marginRight: "15px", float: "right" }} />
      </span>
    );
  }
}

export default App;
