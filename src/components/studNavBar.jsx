import React, { Component } from "react";
import LogoutButton from "./studentLogoutButton.jsx";
import Typography from "@material-ui/core/Typography";

class StudNavBar extends Component {
  state = {};
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <LogoutButton />
        <Typography
          id="site-title"
          variant="h4"
          //style={{ display: "inline" }}
          onClick={() => {
            window.location.href = "../home";
          }}
        >
          QuickQueue
        </Typography>
      </React.Fragment>
    );
  }
}

export default StudNavBar;
