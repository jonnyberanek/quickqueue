import React, { Component } from "react";

import DatabaseService from "./../../services/DatabaseService";
import ProfNameModule from "./profNameModule.jsx";
import TextField from "@material-ui/core/TextField";

import "../style/search.css";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);
//require("./FirebaseInit").init(firebase);
const dbs = new DatabaseService();

class SearchModule extends Component {
  state = { profs: [] };
  constructor(props) {
    super(props);
  }

  handleChange = event => {
    if (event.target.value) {
      dbs.testSearchStringAgaintsProfs(event.target.value, names => {
        this.setState({ profs: names });
      });
    } else {
      this.setState({ profs: [] });
    }
  };

  render() {
    var children = [];
    children.push(
      <span>
        <TextField
          type="search"
          label="Search Prof"
          className="mdc-text-field__input"
          margin="normal"
          onChange={this.handleChange}
        />
      </span>
    );
    for (name in this.state.profs) {
      children.push(
        <span>
          <ProfNameModule name={this.state.profs[name]} />
        </span>
      );
    }
    return <span id="search" children={children} />;
  }
}

export default SearchModule;
