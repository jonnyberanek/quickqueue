import React, { Component } from "react";

import DatabaseService from "../../services/DatabaseService.js";
const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

import Button from "@material-ui/core/Button";
import "../theme/reg-office-hours.css";

class CourseButton extends Component {
  state = {};
  constructor(props) {
    super(props);
  }

  render() {
    var variant, color;
    if (this.props.selected) {
      variant = "contained";
      color = "primary";
    } else {
      variant = "outlined";
      color = "inherit";
    }
    return (
      <span>
        <Button
          className="courseButton"
          onClick={() => this.props.onCourseSelect(this.props.name)}
          variant={variant}
          color={color}
        >
          {this.props.name}
        </Button>
      </span>
    );
  }
}

export default CourseButton;
