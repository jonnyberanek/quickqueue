import React, { Component } from "react";

import "../style/studentProfAppointmentTable.css";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import DatabaseService from "../../services/DatabaseService";
import formatDayCharacter from "../../services/FormatDayCharacter";

const moment = require("moment");

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

const sessionFormat = "h:mm A";
const firebaseFormat = "HH:mm";

export default class StudentProfAppointmentTable extends Component {
  state = {
    days: {
      M: [],
      T: [],
      W: [],
      R: [],
      F: []
    }
  };

  constructor(props) {
    super(props);
    var dbs = new DatabaseService();
    this.setUp(dbs);
  }

  setUp(dbs) {
    //let p = new Promise(function(resolve, reject) {});
    return dbs
      .getProfessorSessionsCacheForWeekWithDate(
        this.props.professorId,
        moment()
      )
      .then(daySnapshots => {
        var newDays = {
          M: [],
          T: [],
          W: [],
          R: [],
          F: []
        };
        var emptyDays = [];
        daySnapshots.forEach(querySnapshot => {
          if (querySnapshot.empty) {
            const day = formatDayCharacter(
              moment(querySnapshot._originalQuery.path.segments[3], "DDDD-yy")
            );
            emptyDays.push(
              dbs
                .getDaySessions(this.props.professorId, day)
                .then(recurringSnapshot => {
                  newDays[day] = recurringSnapshot.docs.map(doc => {
                    if (doc.exists) {
                      const data = doc.data();
                      let numOfDay = 0;
                      switch (doc.data().day) {
                        case "M":
                          numOfDay = 1;
                          break;
                        case "T":
                          numOfDay = 2;
                          break;
                        case "W":
                          numOfDay = 3;
                          break;
                        case "R":
                          numOfDay = 4;
                          break;
                        case "F":
                          numOfDay = 5;
                          break;
                        default:
                          numOfDay = 0;
                      }
                      if (
                        //if we are on a day that has already passed
                        //or at a time in today that is passed on office hour session
                        //do not enable the button
                        numOfDay < moment().day() ||
                        (numOfDay == moment().day() &&
                          data.start < moment().format("HHmm"))
                      ) {
                        return {
                          id: doc.id,
                          start: data.start,
                          end: data.end,
                          fromCache: false,
                          disabled: true
                        };
                      } else {
                        return {
                          id: doc.id,
                          start: data.start,
                          end: data.end,
                          fromCache: false,
                          disabled: false
                        };
                      }
                    }
                  });
                })
            );
            return;
          }
          querySnapshot.forEach(doc => {
            if (doc.exists) {
              const data = doc.data();
              let numOfDay = 0;
              switch (doc.data().day) {
                case "M":
                  numOfDay = 1;
                  break;
                case "T":
                  numOfDay = 2;
                  break;
                case "W":
                  numOfDay = 3;
                  break;
                case "R":
                  numOfDay = 4;
                  break;
                case "F":
                  numOfDay = 5;
                  break;
                default:
                  numOfDay = 0;
              }
              if (
                numOfDay < moment().day() ||
                (numOfDay == moment().day() &&
                  data.start < moment().format("HHmm"))
              ) {
                newDays[data.day].push({
                  id: doc.id,
                  start: data.start,
                  end: data.end,
                  fromCache: true,
                  disabled: true
                });
              } else {
                newDays[data.day].push({
                  id: doc.id,
                  start: data.start,
                  end: data.end,
                  fromCache: true,
                  disabled: false
                });
              }
            }
          });
        });
        Promise.all(emptyDays).then(() => {
          dbs.doesProfUseAppts(this.props.professorId, shouldRenderAppts => {
            if (shouldRenderAppts) {
              this.setState({ apptEnabled: true, days: newDays });
            } else {
              this.setState({ apptEnabled: false, days: newDays });
            }
          });
          //this needs to be the signal that the promise is complete
          return "hi";
        });
      });
  }

  //date.format("DDDD-YY") - for the cache
  gotoAppointmentScheduling = (professorId, sessionId, cache) => {
    window.location.href =
      "/student/appointments?professorId=" +
      professorId +
      "&sessionId=" +
      sessionId +
      "&cacheId=" +
      cache;
  };

  formatSession(startTime, endTime) {
    return (
      moment(startTime, firebaseFormat).format(sessionFormat) +
      " - " +
      moment(endTime, firebaseFormat).format(sessionFormat)
    );
  }

  render() {
    var childComponents = [],
      i = 0;
    if (!this.state.apptEnabled) {
      childComponents.push(
        <Typography
          variant="subtitle2"
          style={{
            display: "inline",
            color: "darkgrey",
            margin: "12px",
            textAlign: "center"
          }}
        >
          Appointments disabled for this professor
        </Typography>
      );
    }
    for (var day in this.state.days) {
      let cache_date = moment(moment().day(1)).add(i, "days");
      childComponents.push(
        <div className="day">
          <Typography
            className="day-letter"
            variant="h6"
            style={{ display: "inline" }}
          >
            {day}:
          </Typography>
          <div className="day-sessions">
            {this.state.days[day]
              // sorts each day to show first session first
              .sort((a, b) => {
                return moment(a.start, firebaseFormat).isAfter(
                  moment(b.start, firebaseFormat)
                )
                  ? 1
                  : -1;
              })
              // maps each day session to a React Component
              .map(info => {
                return (
                  <Button
                    className="day-session"
                    variant="outlined"
                    disabled={!this.state.apptEnabled || info.disabled}
                    onClick={() =>
                      this.gotoAppointmentScheduling(
                        this.props.professorId,
                        info.id,
                        info.fromCache ? cache_date.format("DDDD-YY") : ""
                      )
                    }
                  >
                    {this.formatSession(info.start, info.end)}
                  </Button>
                );
              })}
          </div>
        </div>
      );
      i++;
    }
    return (
      <div class="StudentProfAppointmentTable" children={childComponents} />
    );
  }
}
