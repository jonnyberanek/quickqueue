import React, { Component } from "react";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import Typography from "@material-ui/core/Typography";

import parsingUtils from "./../../services/GetDataParser";

import "../style/email.css";

var getData = parsingUtils.parseUrlGetData(window.location.href);

class Email extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <span>
        <br />
        <Typography variant="subtitle1">These times don't work?</Typography>
        <Button
          onClick={() => window.open("mailto:" + getData.professorId)}
          id="email-cluster"
        >
          <Icon id="email" style={{ marginRight: "10px" }}>
            email
          </Icon>
          Email Professor
        </Button>
      </span>
    );
  }
}

export default Email;
