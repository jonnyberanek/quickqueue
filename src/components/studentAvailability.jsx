import React, { Component } from "react";
import AvailabilityItem from "./availabilityCard.jsx";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import CircularProgress from "@material-ui/core/CircularProgress";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

import DatabaseService from "./../../services/DatabaseService";

import "../style/studentAvail.css";

import $ from "jquery";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);
const dbs = new DatabaseService();

var moment = require("moment");

var credentials =
  "api_token=47874e347e15e7635f5cb58d3c552dbf2a8e449f0e66f3e779&api_token_secret=A95Qj1Icl7gu2";
var apiToken = "47874e347e15e7635f5cb58d3c552dbf2a8e449f0e66f3e779";

class Availability extends Component {
  state = {
    courses: [],
    availability: [],
    selected: "",
    distributeLink: "",
    resultsLink: "",
    clicked: false,
    done: false,
    generateIsLoading: false
  };

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    var scope = this;
    dbs.getAllProfsCourses(this.props.profId, function(courseArray) {
      scope.setState({ courses: courseArray });
    });
  }

  handleChange = event => {
    this.setState(
      { selected: event.target.value, courseIsLoading: true },
      this.courseSelect(event.target.value)
    );
  };

  courseSelect = course => {
    var that = this;
    var members = [];
    var timeMasterArray = [];
    var timeProms = [];
    var bucket = [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
    dbs.getAllMembersOfCourse(course, function(membersArray) {
      members = membersArray;
      if (members) {
        for (var i = 0; i < members.length; i++) {
          //get schedule for each student in the courses
          timeProms.push(dbs.getScheduleOfStudent(members[i].id));
        }
        Promise.all(timeProms).then(arrayOfTimes => {
          //Sort the times for each person --> 0 -> 8, 1 -> 9, 2 -> 10, 3 -> 11, 4 -> 12, 5 -> 13, 6 -> 14, 7 -> 15, 8 -> 16, 9 -> 17, 10 -> 18
          //for each student
          for (var i = 0; i < arrayOfTimes.length; i++) {
            //for each course in the student's schedule,
            //increase the count of the bucket if the student
            //is unavailable at that time
            for (var j = 0; j < arrayOfTimes[i].length; j++) {
              if (arrayOfTimes[i][j][1].includes("M")) {
                bucket[0][
                  Number(arrayOfTimes[i][j][0][0] + arrayOfTimes[i][j][0][1]) -
                    8
                ]++;
              }
              if (arrayOfTimes[i][j][1].includes("T")) {
                bucket[1][
                  Number(arrayOfTimes[i][j][0][0] + arrayOfTimes[i][j][0][1]) -
                    8
                ]++;
              }
              if (arrayOfTimes[i][j][1].includes("W")) {
                bucket[2][
                  Number(arrayOfTimes[i][j][0][0] + arrayOfTimes[i][j][0][1]) -
                    8
                ]++;
              }
              if (arrayOfTimes[i][j][1].includes("R")) {
                bucket[3][
                  Number(arrayOfTimes[i][j][0][0] + arrayOfTimes[i][j][0][1]) -
                    8
                ]++;
              }
              if (arrayOfTimes[i][j][1].includes("F")) {
                bucket[4][
                  Number(arrayOfTimes[i][j][0][0] + arrayOfTimes[i][j][0][1]) -
                    8
                ]++;
              }
            }
          }
          var avail = bucket;
          for (var i = 0; i < avail.length; i++) {
            for (var j = 0; j < avail[i].length; j++) {
              //reverse the numbers so that you get the number
              //of available students

              if (j + 8 < 12) {
                avail[i][j] = [
                  members.length - avail[i][j],
                  String(j + 8) + ":00 a.m."
                ];
              } else if (j + 8 > 12) {
                avail[i][j] = [
                  members.length - avail[i][j],
                  String(j - 4) + ":00 p.m."
                ];
              } else {
                avail[i][j] = [members.length - avail[i][j], "12:00 p.m."];
              }
            }
            avail[i] = avail[i]
              .reverse(
                avail[i].sort(function(a, b) {
                  return a[0] - b[0];
                })
              )
              .slice(0, 5);
          }
          that.setState({
            availability: {
              M: avail[0],
              T: avail[1],
              W: avail[2],
              R: avail[3],
              F: avail[4]
            },
            clicked: true,
            courseIsLoading: false
          });
        });
        return;
      } else {
        alert("No students registered in this course");
      }
    });
  };

  pollClick = () => {
    var scope = this;
    this.setState({ generateIsLoading: true });
    var surveyEncodedUri = "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey?" +
    credentials +
    "&_method=PUT&title=Student Availability&type=survey";
    //create survey
    $.ajax({
      url: surveyEncodedUri,
      crossDomain: true,
      success: function(response) {
        var surveyId = response["data"]["id"];
        scope.setState({
          distributeLink: response["data"]["links"]["default"]
        });

        //create page
        var pageEncodedUri = "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey/" +
        surveyId +
        "/surveypage?_method=PUT&" +
        credentials +
        "&title=Potential Times";
        $.ajax({
          url: pageEncodedUri,
          crossDomain: true,
          success: function(response) {
            var pageId = response["data"]["id"];

            //create question
            var encodedQuestionUri = "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey/" +
            surveyId +
            "/surveypage/" +
            pageId +
            "/surveyquestion?_method=PUT&" +
            credentials +
            "&type=radio&title=Choose The Best Time For You";
            $.ajax({
              url: encodedQuestionUri,
              crossDomain: true,
              success: function(response) {
                var questionId = response["data"]["id"];

                //create answers

                $.ajax({
                  url:
                    "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey/" +
                    surveyId +
                    "/surveypage/" +
                    pageId +
                    "/surveyquestion/" +
                    questionId +
                    "/surveyoption?_method=PUT&" +
                    credentials +
                    "&value=test&title=Monday " +
                    scope.state.availability["M"][0][1],
                  crossDomain: true,
                  success: function(response) {}
                });
                $.ajax({
                  url:
                    "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey/" +
                    surveyId +
                    "/surveypage/" +
                    pageId +
                    "/surveyquestion/" +
                    questionId +
                    "/surveyoption?_method=PUT&" +
                    credentials +
                    "&value=test&title=Tuesday " +
                    scope.state.availability["T"][0][1],
                  crossDomain: true,
                  success: function(response) {}
                });
                $.ajax({
                  url:
                    "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey/" +
                    surveyId +
                    "/surveypage/" +
                    pageId +
                    "/surveyquestion/" +
                    questionId +
                    "/surveyoption?_method=PUT&" +
                    credentials +
                    "&value=test&title=Wednesday " +
                    scope.state.availability["W"][0][1],
                  crossDomain: true,
                  success: function(response) {}
                });
                $.ajax({
                  url:
                    "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey/" +
                    surveyId +
                    "/surveypage/" +
                    pageId +
                    "/surveyquestion/" +
                    questionId +
                    "/surveyoption?_method=PUT&" +
                    credentials +
                    "&value=test&title=Thursday " +
                    scope.state.availability["R"][0][1],
                  crossDomain: true,
                  success: function(response) {}
                });
                $.ajax({
                  url:
                    "https://cors-anywhere.herokuapp.com/https://restapi.surveygizmo.com/v5/survey/" +
                    surveyId +
                    "/surveypage/" +
                    pageId +
                    "/surveyquestion/" +
                    questionId +
                    "/surveyoption?_method=PUT&" +
                    credentials +
                    "&value=test&title=Friday " +
                    scope.state.availability["F"][0][1],
                  crossDomain: true,
                  success: function(response) {}
                });

                scope.setState({ done: true, generateIsLoading: false });
              }
            });
          }
        });
      }
    });
  };

  render() {
    var that = this;
    var children = [],
      childEachDay = [],
      storeCourses = [];
    for (var course in that.state.courses) {
      storeCourses.push(
        <FormControlLabel
          id={that.state.courses[course]}
          value={that.state.courses[course]}
          control={<Radio />}
          label={that.state.courses[course]}
        />
      );
    }

    children.push(
      <span>
        <FormControl id="pickCourse" component="fieldset">
          <FormLabel component="legend">
            {"Select a course to see the students' availability"}
          </FormLabel>

          <RadioGroup
            id="pickCourse"
            onChange={this.handleChange}
            value={this.state.selected}
          >
            {storeCourses}
          </RadioGroup>
        </FormControl>
      </span>
    );

    if (that.state.availability.length != 0) {
      children.push(
        <Typography variant="subheading" style={{ margin: "24px" }}>
          Key: # of students available at a specific hour
        </Typography>
      );
      var i = 1;
      for (var avail in that.state.availability) {
        childEachDay = [];
        childEachDay.push(
          <Typography variant="h6">
            {moment()
              .weekday(i)
              .format("dddd")}
          </Typography>
        );
        for (var slot in that.state.availability[avail]) {
          childEachDay.push(
            <span>
              <List>
                {
                  <AvailabilityItem
                    time={this.state.availability[avail][slot][1]}
                    freq={this.state.availability[avail][slot][0]}
                  />
                }
              </List>
            </span>
          );
        }
        if (i != 5) {
          children.push(
            <span
              id="eachDay"
              style={{ borderRight: "0.1px solid grey", paddingRight: "54px" }}
              children={childEachDay}
            />
          );
        } else {
          children.push(<span id="eachDay" children={childEachDay} />);
        }

        i++;
      }
    }
    if (this.state.clicked) {
      children.push(
        <span>
          <br />
          <Button
            disabled={this.state.generateIsLoading}
            onClick={() => this.pollClick()}
          >
            Generate Student Poll
          </Button>

          <br />
        </span>
      );
      if (this.state.done) {
        children.push(
          <span>
            <Typography variant="subtitle2">
              <a href={this.state.distributeLink} target="_blank">
                Click Here to Get Link to Distribute
              </a>
            </Typography>
            <br />
            <Typography variant="subtitle2">
              <a
                href="https://data.surveygizmo.com/r/662563_5ca62758418ba9.62487497"
                target="_blank"
              >
                Click Here to View Results
              </a>
            </Typography>
          </span>
        );
      }
    }
    return (
      <React.Fragment>
        <span children={children} />
        {(this.state.generateIsLoading || this.state.courseIsLoading) && (
          <CircularProgress
            style={{ position: "absolute", left: "50%", top: "40%" }}
          />
        )}
      </React.Fragment>
    );
  }
}

export default Availability;
