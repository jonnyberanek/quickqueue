import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Star from "@material-ui/icons/Star";
import StarBorder from "@material-ui/icons/StarBorder";
import DatabaseService from "./../../services/DatabaseService.js";
import Typography from "@material-ui/core/Typography";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

class FavIcon extends Component {
  state = { isFavorited: false, profName: "" };

  constructor(props) {
    super(props);
    var that = this;
    this.dbs = new DatabaseService();
    this.state.prof = props.profId;
    this.state.stud = props.studId;
    this.dbs.isFavorited(props.studId, props.profId, function(trueOrFalse) {
      if (trueOrFalse) {
        that.setState({ isFavorited: true });
      }
    });
    that.dbs.getProfUsingID(props.profId, function(name) {
      that.setState({ profName: name });
    });
  }

  favToggle = () => {
    var that = this;
    if (that.state.isFavorited) {
      that.setState({ isFavorited: false });
      that.dbs.removeProfFromFavorites(this.state.stud, this.state.prof);
    } else {
      that.setState({ isFavorited: true });
      that.dbs.addProfToFavorites(this.state.stud, this.state.prof);
    }
  };

  render() {
    var that = this;

    return (
      <div
        style={{ display: "flex", alignItems: "baseline", marginTop: "25px" }}
      >
        <Typography
          variant="h4"
          style={{ marginRight: "5px", marginBottom: "20px" }}
        >
          {that.state.profName}
        </Typography>
        <Button id="favButton" onClick={() => this.favToggle()}>
          {this.state.isFavorited == true ? (
            <Star style={{ fontSize: "200%" }} />
          ) : (
            <StarBorder style={{ fontSize: "200%" }} />
          )}
        </Button>
      </div>
    );
  }
}

export default FavIcon;
