import React, { Component } from "react";

import "../style/studentInLineCard.css";
import CalendarButton from "./calendarButton.jsx";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";

import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

import DatabaseService from "./../../services/DatabaseService";
const dbs = new DatabaseService();
import parsingUtils from "./../../services/GetDataParser";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

const moment = require("moment");

export default class UpcomingApptCard extends Component {
  state = {
    shouldRender: false,
    appts: []
  };

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    let that = this;
    firebase
      .firestore()
      .collection("students")
      .doc(this.props.studId)
      .onSnapshot(querySnapshot => {
        this.updateCard();
      });
  }

  updateCard() {
    let that = this;
    var newArray = [];
    this.dbs.getApptOfAStudent(that.props.studId, apptArray => {
      for (const appt in apptArray) {
        //if the date is after right now (so the seconds will be less than 0)
        if (
          Number(
            moment().diff(
              moment(apptArray[appt].data.start_time.toDate()),
              "minutes"
            )
          ) < 0
        ) {
          that.dbs
            .getNameOfProf(apptArray[appt].data.professor, name => {
              apptArray[appt].profName = name;
            })
            .then(() => {
              newArray.push(apptArray[appt]);
              that.setState({
                student: that.props.studId,
                appts: newArray,
                shouldRender: true
              });
            });
        }
      }
    });
  }
  deleteItem = (prof, id) => {
    this.dbs.deleteStudentAppointment(prof, id).then(() => {
      const newAppts = this.state.appts.filter(appt => appt.id !== id);
      console.log(newAppts.length);
      if (newAppts.length) {
        this.setState({ appts: newAppts });
      } else {
        this.setState({ appts: newAppts, shouldRender: false });
      }
    });
  };

  render() {
    console.log(this.state);
    if (this.state.shouldRender) {
      var children = [];
      var profName = "";
      children = this.state.appts.map(appt => (
        <span
          id="place-head"
          style={{ marginBottom: "30px", paddingLeft: "5px" }}
        >
          <CalendarButton
            prof={appt.profName}
            //start: [2018, 5, 30, 7, 30],
            date={moment(appt.data.start_time.toDate()).format(
              "YYYY MM DD h mm"
            )}
            end={moment(appt.data.end_time.toDate()).format("h mm")}
          />
          <span style={{ display: "block" }}>
            <Typography
              variant="subtitle1"
              style={{
                fontSize: "130%",
                paddingLeft: "20px",
                paddingRight: "20px"
              }}
            >
              Appointment scheduled
            </Typography>
            <Typography variant="subtitle1">with {appt.profName}</Typography>
            <Typography variant="subtitle1">
              at{" "}
              {moment(appt.data.start_time.toDate()).format(
                "MMM DD, YYYY h:mm a"
              )}
            </Typography>
          </span>
          <span id="action-buttons">
            <Button
              onClick={() => this.deleteItem(appt.data.professor, appt.id)}
            >
              Delete
            </Button>
          </span>
        </span>
      ));
      return (
        <Card
          className="GetInLineCard"
          style={{ width: "360px", align: "center", marginTop: "50px" }}
        >
          <span children={children} />
        </Card>
      );
    } else {
      return "";
    }
  }
}
