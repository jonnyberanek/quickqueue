const firebase = require("firebase/app");
import DatabaseService from "./../../services/DatabaseService";

import React, { Component } from "react";
import ReactDOM from "react-dom";
import Plot from "react-plotly.js";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

var plotly = require("plotly")("smithdr1", "Z7jY2rVwPsW0PRKlwtKw");
const moment = require("moment");

const $ = require("jquery");

class TopicGraph extends Component {
  state = { courseOptions: [], time: "week", class: "", data: [], layout: [] };

  constructor(props) {
    super(props);
    var scope = this;
    this.dbs = new DatabaseService();
    var nameArray = [];
    var cl = this.dbs.getAllProfsCourses(scope.props.profId, function(
      courseList
    ) {
      scope.setState({ courseOptions: courseList });
      scope.setState({ class: courseList[0] });

      var now = moment();
      var topicList = scope.dbs.getListOfHistoricalTopics(
        scope.props.profId,
        courseList[0],
        "week",
        now,
        function(topicList) {
          scope.setState({
            data: [
              {
                x: Object.keys(topicList),
                y: Object.values(topicList),
                type: "bar"
              }
            ]
          });
          scope.setState({
            layout: {
              xaxis: {
                title: {
                  text: "Topic"
                }
              },
              yaxis: {
                title: {
                  text: "Number of students"
                }
              },
              title: "Which topics were most talked about in sessions"
            }
          });
        }
      );
    });
    scope.setState({ time: "week" });
  }

  changeTime = event => {
    event.persist();
    var scope = this;
    scope.setState({ time: event.target.value }, () => {
      var now = moment();
      var topicList = scope.dbs.getListOfHistoricalTopics(
        scope.props.profId,
        scope.state.class,
        event.target.value,
        now,
        function(topicList) {
          scope.setState({
            data: [
              {
                x: Object.keys(topicList),
                y: Object.values(topicList),
                type: "bar"
              }
            ]
          });
          scope.setState({
            layout: {
              xaxis: {
                title: {
                  text: "Topic"
                }
              },
              yaxis: {
                title: {
                  text: "Number of students"
                }
              },
              title: "Which topics were most talked about in sessions"
            }
          });
        }
      );
    });
  };

  changeCourse = event => {
    event.persist();
    var scope = this;
    scope.setState({ class: event.target.value }, () => {
      var now = moment();
      var topicList = this.dbs.getListOfHistoricalTopics(
        scope.props.profId,
        event.target.value,
        this.state.time,
        now,
        function(topicList) {
          scope.setState({
            data: [
              {
                x: Object.keys(topicList),
                y: Object.values(topicList),
                type: "bar"
              }
            ]
          });
          scope.setState({
            layout: {
              xaxis: {
                title: {
                  text: "Topic"
                }
              },
              yaxis: {
                title: {
                  text: "Number of students"
                }
              },
              title: "Which topics were most talked about in sessions"
            }
          });
        }
      );
    });
  };

  render() {
    var that = this;
    var children = [];
    children.push(
      <span>
        <FormControl id="pickTime" component="fieldset">
          <FormLabel component="legend">Time Frame</FormLabel>
          <RadioGroup
            id="pickTime"
            onChange={this.changeTime}
            value={this.state.time}
          >
            <FormControlLabel value="week" control={<Radio />} label="Week" />
            <FormControlLabel value="month" control={<Radio />} label="Month" />
            <FormControlLabel
              value="semester"
              control={<Radio />}
              label="Semester"
            />
          </RadioGroup>
        </FormControl>
      </span>
    );

    var storeCourses = [];
    for (var course in that.state.courseOptions) {
      var inserRadio = that.state.courseOptions[course];
      storeCourses.push(
        <FormControlLabel
          id={that.state.courseOptions[course]}
          value={that.state.courseOptions[course]}
          control={<Radio />}
          label={that.state.courseOptions[course]}
        />
      );
    }

    children.push(
      <span>
        <FormControl id="pickCourse" component="fieldset">
          <FormLabel component="legend">Course</FormLabel>
          <RadioGroup
            id="pickCourse"
            onChange={this.changeCourse}
            value={this.state.class}
          >
            {storeCourses}
          </RadioGroup>
        </FormControl>
      </span>
    );

    children.push(
      <Plot
        data={this.state.data}
        layout={this.state.layout}
        config={{
          displayModeBar: false
        }}
      />
    );

    return <span children={children} />;
  }
}

export default TopicGraph;
