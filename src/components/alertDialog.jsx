class AlertDialog extends React.Component {
  handleClose = action => {
    action && action();
    this.props.onClose();
  };

  render() {
    const {
      title,
      body,
      confirmText,
      cancelText,
      onConfirm,
      ...dialogProps
    } = this.props;
    return (
      <Dialog className="AlertDialog" {...dialogProps}>
        {title ? (
          <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        ) : null}
        {body ? <DialogContent>{body}</DialogContent> : null}
        <DialogActions>
          {cancelText ? (
            <Button onClick={() => this.handleClose(null)}>{cancelText}</Button>
          ) : null}
          {confirmText ? (
            <Button onClick={() => this.handleClose(onConfirm)}>
              {confirmText}
            </Button>
          ) : null}
        </DialogActions>
      </Dialog>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string
};

const SimpleDialogWrapped = withStyles(null)(AlertDialog);
