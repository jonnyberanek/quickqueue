import React, { Component } from "react";

import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

import TopicGraph from "./TopicGraph.jsx";
import OfficeHourGraph from "./OfficeHourGraph.jsx";

class EditGraphTypePicker extends React.Component {
  state = {
    value: 0
  };

  constructor(props) {
    super(props);
    this.tabs = [
      {
        label: "By Topic",
        component: <TopicGraph profId={props.profId} />
      },
      {
        label: "By Office Hour Session",
        component: <OfficeHourGraph profId={props.profId} />
      }
    ];
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;
    return (
      <Paper square className="GraphTypePicker">
        <Tabs
          value={this.state.value}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          margin= "auto"

          onChange={this.handleChange}
        >
          {this.tabs.map(tab => (
            <Tab label={tab.label} />
          ))}
        </Tabs>
        {this.tabs.map((tab, index) => (
          <div
          className="tab-content"
            style={{ display: this.state.value != index ? "none" : "block" }}
          >
            {tab.component}
          </div>
        ))}
      </Paper>
    );
  }
}
export default EditGraphTypePicker;
