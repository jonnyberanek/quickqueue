import React, { Component } from "react";
import QueueItem from "./queueItem.jsx";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

import DatabaseService from "./../../services/DatabaseService";
import formatDayCharacter from "./../../services/FormatDayCharacter";

const firebase = require("firebase");
const moment = require("moment");
require("../../services/FirebaseInit").init(firebase);

class Queue extends Component {
  state = { items: [], shouldRender: false };

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    var scope = this;
    this.dbs.clearTheQueue(scope.props.profId, done => {
      this.dbs.getCurrentSession(scope.props.profId, inSession => {
        if (inSession) {
          //clear the queue of all of the people who are not from this session
          firebase
            .firestore()
            .collection("queue")
            .doc(scope.props.profId)
            .collection("visits")
            .orderBy("place_in_line", "asc")
            .onSnapshot(function(querySnapshot) {
              scope.refTasks(querySnapshot);
            });
        } else {
          this.dbs
            .getProfessorSessionsCacheForDay(this.props.profId, moment().day())
            .then(querySnapshot => {
              if (querySnapshot.empty) {
                this.dbs
                  .getDaySessions(
                    scope.props.profId,
                    formatDayCharacter(moment())
                  )
                  .then(recurringSnapshot => {
                    this.findClosestSession(recurringSnapshot);
                  });
              } else {
                this.findClosestSession(querySnapshot);
              }
            });
        }
      });
    });
  }

  addColon(time) {
    return time[0] + time[1] + ":" + time[2] + time[3];
  }

  removeColon(time) {
    return time[0] + time[1] + time[3] + time[4];
  }

  findClosestSession(recurringSnapshot) {
    var scope = this;
    var closestSession = null;
    var closestSessionMin;
    recurringSnapshot.forEach(doc => {
      var dateOfDoc = moment(
        moment().format("YYYY-MM-DD ") + scope.addColon(doc.data().start)
      );
      if (closestSession != null) {
        //if the session starts after right now
        console.log(dateOfDoc);
        if (dateOfDoc.isSameOrAfter(moment())) {
          console.log(
            closestSessionMin.diff(moment(), "hours") >
              dateOfDoc.diff(moment(), "hours")
          );
          //if the time until the doc's session is less than the closestSession
          if (
            closestSessionMin.diff(moment(), "hours") >
            dateOfDoc.diff(moment(), "hours")
          ) {
            closestSession = doc;
            closestSessionMin = moment(
              moment().format("YYYY-MM-DD ") + scope.addColon(doc.data().start)
            );
          }
        }
      } else {
        if (dateOfDoc.isSameOrAfter(moment())) {
          closestSession = doc;
          closestSessionMin = moment(
            moment().format("YYYY-MM-DD ") + scope.addColon(doc.data().start)
          );
        }
      }
    });
    console.log(closestSession);
    if (
      closestSession == null ||
      moment.duration(closestSessionMin.diff(moment(), "hours")) < 0
    ) {
      scope.setState({
        shouldRender: false,
        timeUntil: " - And there are no more sessions for today."
      });
    } else if (
      moment.duration(closestSessionMin.diff(moment(), "hours")) == 0
    ) {
      scope.setState({
        shouldRender: false,
        timeUntil: " - Approximately less than an hour until next session."
      });
    } else {
      scope.setState({
        shouldRender: false,
        timeUntil:
          " - Approximately " +
          moment.duration(closestSessionMin.diff(moment(), "hours")) +
          " hour(s) until next session"
      });
    }
  }

  refTasks = querySnapshot => {
    let that = this;
    let p = new Promise(function(resolve, reject) {
      resolve();
    });

    var items = [];
    var promises = [];
    if (querySnapshot.docs.length) {
      querySnapshot.forEach(doc => {
        //scoped for each doc, so this will persist as proper value for
        var stud_docs, course_docs;
        // the 'then' itself is a promise, don't need the outer promise code
        // grabbing the student from student collection
        var studentPromise = doc
          .data()
          .student.get()
          .then(docSnapshot => {
            stud_docs = docSnapshot;
          });
        // grabbing the course from course collection
        if (doc.data().course != null) {
          var coursePromise = doc
            .data()
            .course.get()
            .then(docSnapshot => {
              course_docs = docSnapshot;
            });
        } else {
          coursePromise = new Promise(function(resolve, reject) {
            resolve((course_docs = { id: "No course specified" }));
          });
        }
        var promise = Promise.all([studentPromise, coursePromise]);
        //push promise to overaching promise array to track when all are finished
        promises.push(promise);
        //once the sub promises are resolved add an html object
        promise.then(() => {
          if (stud_docs.exists != undefined || course_docs.id != undefined) {
            //studentIds[doc.id] = stud_docs.id;
            items.push({
              studentName: stud_docs.data().name,
              courseId: course_docs.id,
              topic: doc.data().topic,
              id: doc.id,
              studentId: stud_docs.id,
              place_in_line: doc.data().place_in_line,
              ticket_number: doc.data().ticket_number
            });
          }
        });
        //once all students in queue are loaded, queue can now be updated
        Promise.all(promises).then(() => {
          items[0].isFirst = true;
          that.setState({
            items: items,
            shouldRender: true
          });
          return p;
        });
      });
    } else {
      that.setState({
        items: [],
        shouldRender: true
      });
      return p;
    }
  };

  deleteItem = (studId, queueId) => {
    let that = this;
    let p = new Promise(function(resolve, reject) {
      resolve(
        that.dbs.deleteStudentFromQueue(that.props.profId, studId, queueId)
      );
    });
    return p;
  };

  pushbackItem = () => {
    let that = this;
    let p = new Promise(function(resolve, reject) {
      resolve(that.dbs.pushbackStudent(that.props.profId, 1));
    });
    return p;
  };

  render() {
    console.log(this.state);
    if (this.state.shouldRender) {
      if (this.state.items.length != 0) {
        return (
          <List
            style={{
              maxWidth: "550px",
              minWidth: "450px",
              marginBottom: "48px"
            }}
          >
            {this.state.items.map((item, index) => (
              <React.Fragment>
                <QueueItem
                  key={item.id}
                  item={item}
                  onDelete={this.deleteItem}
                  onPushback={this.pushbackItem}
                />
                {index != this.state.items.length - 1 && (
                  <Divider key={"divider-" + index} />
                )}
              </React.Fragment>
            ))}
          </List>
        );
      } else {
        return (
          <Typography
            variant="subtitle1"
            style={{
              marginBottom: "48px",
              color: "darkgrey",
              fontSize: "110%"
            }}
          >
            No one is in line
          </Typography>
        );
      }
    } else {
      return (
        <Typography
          variant="h6"
          style={{
            marginBottom: "48px",
            color: "darkgrey",
            fontSize: "110%"
          }}
        >
          Not in session {this.state.timeUntil}
        </Typography>
      );
    }
  }
}

export default Queue;
