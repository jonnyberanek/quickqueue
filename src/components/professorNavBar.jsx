import React, { Component } from "react";

import "../style/professorNavBar.css";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";

import LogoutButton from "./logoutButton.jsx";

const bimap = require("bimap");

const pageMap = new bimap();

// bimap, keys are titles, values are page names
pageMap.push({
  Home: "queue",
  Appointments: "appointments",
  "Change Hours": "change-hours",
  Announce: "mailgun",
  Availability: "availability",
  Analyze: "analytics"
});

export default class ProfessorNavBar extends Component {
  state = {
    isMobile: false,
    mobileOpen: false
  };

  componentWillMount() {
    this.checkResize();
  }

  componentDidMount() {
    this.checkResize();
    window.addEventListener("resize", this.checkResize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.checkResize);
  }

  checkResize = () => {
    //do something with this
    if (window.innerWidth < 1000) {
      if (!this.state.isMobile) {
        this.setState({ isMobile: true });
      }
    } else {
      if (this.state.isMobile) {
        this.setState({ isMobile: false, mobileOpen: false });
      }
    }
  };

  render() {
    var tabs = [];
    const splits = window.location.pathname.split("/");
    const thisPagename = splits[splits.length - 2];
    for (var pagename in pageMap.vk) {
      tabs.push(
        <Button
          color={thisPagename === pagename ? "primary" : "default"}
          href={"../" + pagename}
        >
          {pageMap.val(pagename)}
        </Button>
      );
    }

    return (
      <div className="ProfessorNavBar">
        <div
          id="nav-topper"
          onClick={
            this.state.isMobile
              ? () => {
                  this.setState({ mobileOpen: !this.state.mobileOpen });
                }
              : null
          }
        >
          <Typography
            id="site-title"
            variant="h4"
            style={{ display: "inline" }}
            onClick={
              !this.state.isMobile
                ? () => {
                    window.location.href = "../queue";
                  }
                : null
            }
          >
            QuickQueue
          </Typography>
          {this.state.isMobile ? (
            this.state.mobileOpen ? (
              <ExpandLess />
            ) : (
              <ExpandMore />
            )
          ) : null}
        </div>
        {this.state.isMobile ? (
          <Collapse in={this.state.mobileOpen} unmountOnExit>
            <div id="tabs">{tabs}</div>
            <LogoutButton />
          </Collapse>
        ) : (
          <React.Fragment>
            <div id="tabs">{tabs}</div>
            <LogoutButton />
          </React.Fragment>
        )}
      </div>
    );
  }
}
