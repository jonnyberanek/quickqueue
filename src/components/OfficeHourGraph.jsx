const firebase = require("firebase/app");
import DatabaseService from "./../../services/DatabaseService";
import formatDayCharacter from "../../services/FormatDayCharacter.js";

import React, { Component } from "react";
import ReactDOM from "react-dom";
import Plot from "react-plotly.js";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

var plotly = require("plotly")("smithdr1", "Z7jY2rVwPsW0PRKlwtKw");
const moment = require("moment");
const $ = require("jquery");

class OfficeHourGraph extends Component {
  state = { courseOptions: [], time: "week", data: [], layout: [] };

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    this.dbs = new DatabaseService();
    var scope = this;
    scope.setState({ time: "week" });
    this.updateScreen(this.state.time);
  }
  updateScreen(time) {
    //console.log(time);
    let scope = this;
    var now = moment();
    scope.dbs.getAllProfsCourses(scope.props.profId, function(courseList) {
      scope.setState({ courseOptions: courseList });
      scope.dbs.getProfsOfficeHours(scope.props.profId, function(profSessions) {
        scope.dbs.getListOfHistoricalOfficeHourVisits(
          scope.props.profId,
          time,
          profSessions,
          courseList,
          now,
          function(sortedVisits) {
            var dataStore = [];
            var finalKey;
            var finalVisits = {};
            for (var i = 0; i < courseList.length; i++) {
              var storeCourseCounts = [];
              for (var key in sortedVisits) {
                let result = key.split(" ");
                let resultOfTime = result[1].split("-");
                let begTime =
                  Number(resultOfTime[0]) > 11
                    ? (resultOfTime[0] == "12" ? "12" : resultOfTime[0] - 12) +
                      "pm-"
                    : resultOfTime[0] + "am-";
                let endTime = String(
                  Number(resultOfTime[1]) > 11
                    ? (resultOfTime[1] == "12" ? "12" : resultOfTime[1] - 12) +
                        "pm"
                    : resultOfTime[1] + "am"
                );
                finalKey =
                  formatDayCharacter(moment().day(result[0])) +
                  " " +
                  begTime +
                  "" +
                  endTime;
                finalVisits[finalKey] = sortedVisits[key];
              }
              for (var finalKey in finalVisits) {
                if (finalVisits.hasOwnProperty(finalKey)) {
                  console.log(finalVisits[finalKey][courseList[i]]);
                  storeCourseCounts.push(finalVisits[finalKey][courseList[i]]);
                }
              }
              var trace = {
                x: Object.keys(finalVisits),
                y: storeCourseCounts,
                type: "bar",
                name: courseList[i]
              };
              dataStore.push(trace);
            }
            scope.setState({
              data: dataStore
            });

            scope.setState({
              layout: {
                xaxis: {
                  title: {
                    text: "Office hour slot"
                  },
                  type: "category"
                },
                yaxis: {
                  title: {
                    text: "Number of students"
                  }
                },
                barmode: "stack",
                title: "Office Hour session interest"
              }
            });
          }
        );
      });
    });
  }

  changeTime = event => {
    event.persist();
    this.setState({ time: event.target.value });
    this.updateScreen(event.target.value);
  };

  render() {
    var that = this;
    var children = [];
    children.push(
      <span>
        <FormControl id="pickTime" component="fieldset">
          <FormLabel component="legend">Time Frame</FormLabel>
          <RadioGroup onChange={this.changeTime} value={that.state.time}>
            <FormControlLabel value="week" control={<Radio />} label="Week" />
            <FormControlLabel value="month" control={<Radio />} label="Month" />
            <FormControlLabel
              value="semester"
              control={<Radio />}
              label="Semester"
            />
          </RadioGroup>
        </FormControl>
      </span>
    );

    children.push(
      <Plot
        data={this.state.data}
        layout={this.state.layout}
        config={{
          displayModeBar: false
        }}
      />
    );

    return <span children={children} />;
  }
}

export default OfficeHourGraph;
