/*
testing suite for dbs

1 TEST FAILS

TODO:
- Greg - do work :)
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import DatabaseService from "../../../services/DatabaseService";

import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

const firebase = require("firebase/app");
const dbs = new DatabaseService();

const moment = require("moment");

describe("DatabaseService", () => {
  //does the dbs exist
  it("dbs should be defined", () => {
    expect(dbs).toBeDefined();
  });

  it("pushAppointmentByDbRef should be defined", () => {
    expect(dbs.pushAppointmentByDbRef).toBeDefined();
  });

  //TODO: TEST THE pushAppointmentByDbRef FUNCTIONALITY
  it("pushAppointmentByDbRef change the db", () => {
    var countBefore = 0,
      countAfter = 0;
    dbs.db
      .collection("appointments")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          countBefore++;
        });
      })
      .then(() => {
        dbs.pushAppointmentByDbRef(
          {
            end_time: firebase.firestore.Timestamp.fromDate(
              new Date(appointment.end + ":00")
            ),
            start_time: firebase.firestore.Timestamp.fromDate(
              new Date(appointment.start + ":00")
            ),
            course: "test",
            professor: "test",
            student: "test",
            topic: "Test"
          },
          a => {
            dbs.db
              .collection("appointments")
              .get()
              .then(querySnapshot => {
                querySnapshot.forEach(doc => {
                  countAfter++;
                });
              });
            expect(countAfter - countBefore).toBe(1);
          }
        );
      });
  });

  it("pushAppointment should be defined", () => {
    expect(dbs.pushAppointment).toBeDefined();
  });




  //TODO: TEST THE pushAppointment FUNCTIONALITY

  //test for getTypeOfLoggedInUser
  it("getTypeOfLoggedInUser should be defined", () => {
    expect(dbs.getTypeOfLoggedInUser).toBeDefined();
  });

  it("getTypeOfLoggedInUser returns the correct types", () => {
    dbs.getTypeOfLoggedInUser("w@w.com", user => {
      expect(user).toBe("professor");
    });
    dbs.getTypeOfLoggedInUser("kp@kp.com", user => {
      expect(user).toBe("student");
    });
    dbs.getTypeOfLoggedInUser("akjdhfkj", user => {
      expect(user).toBe("Not found");
    });
  });

  it("getLoggedInUser should be defined", () => {
    expect(dbs.getLoggedInUser).toBeDefined();
  });

  it("getLoggedInUser should be return data about the user", () => {
    dbs.getLoggedInUser("w@w.com", user => {
      expect(user).toBeDefined();
    });
    dbs.getTypeOfLoggedInUser("kp@kp.com", user => {
      expect(user).toBeDefined();
    });
    dbs.getTypeOfLoggedInUser("kp@kp.com", user => {
      expect(user).toMatchSnapshot(querySnapshot);
    });
  });

  it("getLoggedInUserID should be defined", () => {
    expect(dbs.getLoggedInUserID).toBeDefined();
  });

  it("getLoggedInUserID should be return data about the user", () => {
    dbs.getLoggedInUserID("kp@kp.com", user => {
      expect(user).toBe("10g49");
    });
  });

  it("getAllMembersOfCourse should be defined", () => {
    expect(dbs.getAllMembersOfCourse).toBeDefined();
  });

  it("getAllMembersOfCourse should return data", () => {
    dbs.getAllMembersOfCourse("COMP 141 A", members => {
      expect(members).toBeDefined();
    });
    dbs.getAllMembersOfCourse("COMP 141 A", members => {
      expect(members).toBeTruthy();
    });
  });
  

  it("getLastInQueue should be defined", () => {
    expect(dbs.getLastInQueue).toBeDefined();
  });

  it("getLastInQueue should return data", () => {
    dbs.getLastInQueue(last => {
      expect(last).toBeDefined();
    });
  });

  it("addStudentToQueue should be defined", () => {
    expect(dbs.addStudentToQueue).toBeDefined();
  });

  //TODO: TEST THAT WE CAN successfully ADD A STUDENT TO THE Queue

  it("deleteStudentFromQueue should be defined", () => {
    expect(dbs.deleteStudentFromQueue).toBeDefined();
  });

  //TODO: TEST THAT WE CAN successfully DELETE A STUDENT FROM THE Queue

  it("sendRequestMessage should be defined", () => {
    expect(dbs.sendRequestMessage).toBeDefined();
  });

  //TODO: TEST THAT WE CAN successfully SEND A REQUEST MESSAGE

  it("getFirstInQueue should be defined", () => {
    expect(dbs.getFirstInQueue).toBeDefined();
  });

  it("getFirstInQueue should return data", () => {
    dbs.getFirstInQueue(first => {
      expect(first).toBeDefined();
    });
  });



  it("pushbackStudent should change the place_in_line", () => {
    dbs.getFirstInQueue(first => {
      dbs.pushbackStudent(1);
      expect(first.data().place_in_line).toBe(2);
    });
  });

  it("getName should be defined", () => {
    expect(dbs.getName).toBeDefined();
  });

  it("getName should return data", () => {
    dbs.getName("kp@kp.com", name => {
      expect(name).toBeDefined();
    });
  });

  it("getAllSessions should be defined", () => {
    expect(dbs.getAllSessions).toBeDefined();
  });

  it("getAllSessions should return data", () => {
    let sessions = dbs.getAllSessions("wolfebd@gcc.edu");
    expect(sessions).toBeDefined();
  });

  it("getTopicsFromHistoricalVisits should be defined", () => {
    dbs.getTopicsFromHistoricalVisits("wolfebd@gcc.edu", topics => {
      expect(topics).toBeDefined();
     });
  });

  //TODO: TEST THE FUNCTIONALITY OF getTopicsFromHistoricalVisits FUNCTION

  it("testSearchStringAgaintsProfs should be defined", () => {
    expect(dbs.testSearchStringAgaintsProfs).toBeDefined();
  });

  it("testSearchStringAgaintsProfs should return data", () => {
    dbs.testSearchStringAgaintsProfs("wolfe", names => {
      expect(names).toBeDefined();
      expect(names).toBe(["Britton Wolfe"]);
    });
    dbs.testSearchStringAgaintsProfs("dellinger", names => {
      expect(names).toBeDefined();
      expect(names).toBe(["Brian Dellinger"]);
    });
  });

  it("getProfUsingID should be defined", () => {
    expect(dbs.getProfUsingID).toBeDefined();
  });

  it("getProfUsingID should return correct data", () => {
    dbs.getProfUsingID("wolfebd@gcc.edu", prof => {
      expect(prof).toBeDefined();
      expect(prof).toBe("Britton Wolfe");
    });
    dbs.getProfUsingID("200001", prof => {
      expect(prof).toBeDefined();
      expect(prof).toBe("Not found");
    });
  });

  it("getProfIdUsingName should be defined", () => {
    expect(dbs.getProfIdUsingName).toBeDefined();
  });

  it("getProfIdUsingName should return correct data", () => {
    dbs.getProfIdUsingName("Britton Wolfe", prof => {
      expect(prof).toBeDefined();
      expect(prof).toBe("wolfebd@gcc.edu");
    });
    dbs.getProfUsingID("ajkdf", prof => {
      expect(prof).toBeDefined();
      expect(prof).toBe("Not found");
    });
  });

  it("getProfessorSessionsCacheForWeekWithDate should be defined", () => {
    dbs.addProfToFavorites("300000", "M")

    expect(dbs.getProfessorSessionsCacheForWeekWithDate).toBeDefined();
  });

  //TODO: TEST THE getProfessorSessionsCache FUNCTION'S FUNCTIONALITY

  it("addProfToFavorites should be defined", () => {
    dbs.addProfToFavorites("10g49", "300000")
    expect(dbs.addProfToFavorites).toBeDefined();
  });

  //TODO: TEST THE addProfToFavorites FUNCTION'S FUNCTIONALITY

  it("removeProfFromFavorites should be defined", () => {
    dbs.removeProfFromFavorites("10g49", "300000")
    expect(dbs.removeProfFromFavorites).toBeDefined();
  });

  it("isFavorited should be defined", () => {
    dbs.isFavorited("10g49", "300000", favorites =>{
      expect(dbs.isFavorited).toBeDefined();
    })

  });

  //TODO: TEST THE removeProfFromFavorites FUNCTION'S FUNCTIONALITY

  it("getStudentFavorites should be defined", () => {
    expect(dbs.getStudentFavorites).toBeDefined();
  });

  it("getStudentFavorites should return data", () => {
    dbs.getStudentFavorites("10g49", favs => {
      expect(favs).toBeDefined();
    });
    dbs.getStudentFavorites("123509", favs => {
      expect(favs).toBeDefined();
      expect(favs).toBe("No favorites");
    });
  });

  it("updateAdditions should be defined", () => {
    expect(dbs.updateAdditions).toBeDefined();
  });

  //TODO: TEST THE updateAdditions FUNCTION'S FUNCTIONALITY

  it("updateDeletions should be defined", () => {
    expect(dbs.updateDeletions).toBeDefined();
  });

  //TODO: TEST THE updateDeletions FUNCTION'S FUNCTIONALITY

  it("getStudentQueueNumber should be defined", () => {
    expect(dbs.getStudentQueueNumber).toBeDefined();
  });

  it("getStudentQueueNumber should return data", () => {
    dbs.getStudentQueueNumber("10g49", "wolfebd@gcc.edu").then(num => {
      expect(num).toBeDefined();
      expect(num).toBe(13);
    });

    dbs.getStudentQueueNumber("123509", "wolfebd@gcc.edu").then(num => {
      expect(num).toBeDefined();
      expect(num).toBe(null);
    });
  });

  it("getAllAppointments should be defined", () => {
    expect(dbs.getAllAppointments).toBeDefined();
  });

  it("getAllAppointments should return data", () => {
    dbs.getAllAppointments("wolfebd@gcc.edu", num => {
      expect(num).toBeDefined();
    });
    dbs.getAllAppointments("200001", num => {
      expect(num).toBeDefined();
      expect(num).toBe([]);
    });
  });

  it("deleteStudentAppointment should be defined", () => {
    expect(dbs.deleteStudentAppointment).toBeDefined();
  });

  //TODO: TEST THE deleteStudentAppointment FUNCTION'S FUNCTIONALITY

  it("addStudentAppointment should be defined", () => {
    expect(dbs.addStudentAppointment).toBeDefined();
  });

  //TODO: TEST THE addStudentAppointment FUNCTION'S FUNCTIONALITY

  it("getScheduleOfStudent should be defined", () => {
    expect(dbs.getScheduleOfStudent).toBeDefined();
  });

  it("getScheduleOfStudent return data", () => {
    dbs.getScheduleOfStudent("10g49", timeArray => {
      expect(timeArray).toBeDefined();
      expect(timeArray).toBe(["0800", "0900"]);
    });
    dbs.getScheduleOfStudent("200001", num => {
      expect(timeArray).toBeDefined();
      expect(timeArray).toBe([]);
    });
  });

  it("getNextAppointment should be defined", () => {
    expect(dbs.getNextAppointment).toBeDefined();
  });

  it("getNextAppointment return data", () => {
    dbs.getNextAppointment("wolfebd@gcc.edu", appt => {
      expect(appt).toBeDefined();
      expect(appt).toBe([
        "UzaOsmo1Q6J9Z4RLRft2",
        "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
        "Arrays",
        "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
        "Karen Postupac"
      ]);
    });
    dbs.getNextAppointment("200001", num => {
      expect(timeArray).toBeDefined();
      expect(timeArray).toBe([]);
    });
  });

  it("getNumOfPeopleInLine should be defined", () => {
    expect(dbs.getNumOfPeopleInLine).toBeDefined();
  });

  it("getNumOfPeopleInLine return data", () => {
    dbs.getNumOfPeopleInLine("w@w.com", count => {
      expect(count).toBeDefined();
      expect(count).toBe(4);
    });
    dbs.getNumOfPeopleInLine("kasjdhf@gcc.edu", count => {
      expect(count).toBeDefined();
      expect(count).toBe(0);
    });
  });

  it("getStudUsingID should be defined", () => {
    expect(dbs.getStudUsingID).toBeDefined();
  });

  it("getStudUsingID return data", () => {
    dbs.getStudUsingID("kp@kp.com", count => {
      expect(count).toBeDefined();
      expect(count).toBe("10g49");
    });
    dbs.getStudUsingID("kasjdhf@gcc.edu", count => {
      expect(count).toBeDefined();
    });
  });

  it("getAllProfsCourses should be defined", () => {
    expect(dbs.getAllProfsCourses).toBeDefined();
  });

  it("getAllProfsCourses should return data", () => {
    dbs.getAllProfsCourses("w@w.com", courses => {
      expect(courses).toBeDefined();
      expect(courses).toBe(["COMP 141", "COMP 220"]);
    });
    dbs.getAllProfsCourses("asdkfj@gcc.edu", courses => {
      expect(courses).toBeDefined();
      expect(courses).toBe([]);
    });
  });

  it("getListOfHistoricalTopics should be defined", () => {
    expect(dbs.getListOfHistoricalTopics).toBeDefined();
  });

  it("getListOfHistoricalTopics return data", () => {
    dbs.getListOfHistoricalTopics("w@w.com", "COMP 141 A", topics => {
      expect(topics).toBeDefined();
    });
    dbs.getListOfHistoricalTopics("wolfebd@gcc.edu", "COMP 141 A", topics => {
      expect(topics).toBeDefined();
      expect(topics).toBe()
    });
  });

  it("getSessionDataById should be defined", () => {
    expect(dbs.getSessionDataById).toBeDefined();
  });

  it("getSessionDataById should return correct data", () => {
    dbs.getSessionDataById("wolfebd@gcc.edu", "0RTinoNy3sehnGiHaWGv", session => {
      expect(session).toBeDefined();
      expect(session).toBe(["T", "1300","1200"]);
    });
  });

  it("getListOfHistoricalOfficeHourVisits should be defined", () => {
    expect(dbs.getListOfHistoricalOfficeHourVisits).toBeDefined();
  });

  it("getListOfHistoricalOfficeHourVisits should return data", () => {
    dbs.getListOfHistoricalOfficeHourVisits("wolfebd@gcc.edu")
    expect(dbs.getListOfHistoricalOfficeHourVisits).toBeDefined();
  });

  it("getDaySessions should be defined", () => {
    expect(dbs.getDaySessions("wolfebd@gcc.edu", "M")).toBeDefined();
  });

  it("getProfAppointments should be defined", () => {
    expect(dbs.getProfAppointments("wolfebd@gcc.edu")).toBeDefined();
  });

  it("getAdditionsForDate should be defined", () => {
    expect(dbs.getProfAppointments("wolfebd@gcc.edu", "0001-19")).toBeDefined();
  });

});
