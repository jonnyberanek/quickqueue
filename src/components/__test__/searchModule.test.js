/*
testing suite for ProfessorScreenContainer

*/

import SearchModule from "../searchModule.jsx";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";

import Typography from "@material-ui/core/Typography";

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Adapter from "enzyme-adapter-react-16";
import ProfNameModule from "../profNameModule.jsx";
import DatabaseService from "../../../services/DatabaseService";
import ProfessorAuthService from "../../../services/ProfessorAuthService";
const dbs = new DatabaseService();
import TextField from "@material-ui/core/TextField";

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

//mocking the parseUrlGetData function so that it doesnt actually run
//jest.mock("../../../services/DatabaseService", () => ({

describe("SearchModule", () => {
  it("should be defined", () => {
    expect(SearchModule).toBeDefined();
  });

  it("SearchModule should be render", () => {
    const wrapper = render(<SearchModule />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new SearchModule();
    expect(check).toBeDefined();
  });

  it("change event triggered", () => {
    const wrapper = mount(<SearchModule />);
    let spy = jest.spyOn(wrapper.instance(), "handleChange");
    wrapper.setState({ profs: { name: "Wolfe" } });
    wrapper.find(TextField).simulate("change", { target: { value: "Wolfe" } });
    expect(wrapper.find(TextField).props.onChange).toHaveBeenCalled();
  });

  it("change event triggered", () => {
    const wrapper = mount(<SearchModule />);
    wrapper.instance().handleChange({ target: { value: "Wolfe" } });
    expect(wrapper.state().searchString).toBe("Wolfe");
  });
});
