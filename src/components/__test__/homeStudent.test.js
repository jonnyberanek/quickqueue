/*
testing suite for the home screen for the student

ALL TESTS PASS
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import DatabaseService from "../../../services/DatabaseService.js";
import Adapter from "enzyme-adapter-react-16";
import StudentHome from "../homeStudent.jsx";
import ProfNameModule from "../profNameModule.jsx";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const moment = require("moment");
const $ = require("jquery");

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("All StudentHome testing", () => {
  it("StudentHome - should be defined", () => {
    expect(StudentHome).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("StudentHome should render correctly", () => {
    const wrapper = shallow(<StudentHome />);
    expect(wrapper).toMatchSnapshot();
  });

  it("StudentHome- check constructor", () => {
    const check = new StudentHome();
    expect(check).toBeDefined();
  });

  it("StudentHome should render correctly when state is set", () => {
    DatabaseService.getStudentFavorites = jest.fn("prof", done => {
      done(["Britton Wolfe"]);
    });
    const wrapper = shallow(<StudentHome />);
    wrapper.setState({ name: "Britton Wolfe" });
    expect(wrapper).toMatchSnapshot();
  });

  it("ProfNameModule - should be defined", () => {
    expect(ProfNameModule).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("ProfNameModule - should render correctly", () => {
    const wrapper = shallow(<ProfNameModule />);
    expect(wrapper).toMatchSnapshot();
  });

  it("ProfNameModule - check constructor", () => {
    const check = new ProfNameModule();
    expect(check).toBeDefined();
  });

  it("ProfNameModule - check constructor", () => {
    const wrapper = shallow(<ProfNameModule />);
    let spy = jest.spyOn(wrapper.instance(), "goToProfProfile");
    wrapper.find(ListItemText).simulate("click");
    expect(spy).toHaveBeenCalled();
  });
});
