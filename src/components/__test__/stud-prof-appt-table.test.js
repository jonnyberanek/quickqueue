/*
testing suite for StudentProfAppointmentTable

1 TEST FAILS

TODO:
- get the last branch covered
*/

import React, { Component } from "react";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Enzyme, { shallow, render, mount } from "enzyme";
import DatabaseService from "../../../services/DatabaseService";
const dbs = new DatabaseService();
const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

import StudentProfAppointmentTable from "../studentProfAppointmentTable.jsx";

import Adapter from "enzyme-adapter-react-16";

const moment = require("moment");

Enzyme.configure({ adapter: new Adapter() });

const defaultState = {
  days: {
    M: [{ id: "id", start: "0800", end: "0900" }],
    T: [],
    W: [],
    R: [],
    F: []
  },
  professorId: wolfebd@gcc.edu
};

describe("StudentProfAppointmentTable", () => {
  //do appointments exist
  it("should be defined", () => {
    expect(StudentProfAppointmentTable).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("ApptTable should render correctly", () => {
    DatabaseService.getProfessorSessionsCacheForWeekWithDate = jest
      .fn()
      .mockResolvedValue({
        M: [],
        T: [{ id: "alkjdf", start: "0800", end: "0900" }],
        W: [{ id: "sNKta7HVufjdeYNFcPrv", start: "1700", end: "1800" }],
        R: [],
        F: []
      });
    const tree = shallow(<StudentProfAppointmentTable professorId="wolfebd@gcc.edu" />);
    expect(tree).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new StudentProfAppointmentTable({ professorId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  it("works to resolve in constructor", () => {
    const wrapper = mount(<StudentProfAppointmentTable professorId="wolfebd@gcc.edu" />);
    DatabaseService.getProfessorSessionsCacheForWeekWithDate = jest
      .fn()
      .mockResolvedValue([
        [],
        [{ id: "alkjdf", start: "0800", end: "0900" }],
        [{ id: "sNKta7HVufjdeYNFcPrv", start: "1700", end: "1800" }],
        [],
        []
      ]);
    let oldState = wrapper.state();
    return expect(wrapper.instance().setUp(dbs)).toHaveReturned("hi");

  });

  it("works to resolve in constructor when query is empty", () => {
    const wrapper = mount(<StudentProfAppointmentTable professorId="wolfebd@gcc.edu" />);
    DatabaseService.getProfessorSessionsCacheForWeekWithDate = jest
      .fn()
      .mockResolvedValue([[null], [], [], [], []]);
    let oldState = wrapper.state();
    return expect(wrapper.instance().setUp(dbs)).resolves.toBe("hi");

  });

  it("check existance of functions", () => {
    DatabaseService.getProfessorSessionsCacheForWeekWithDate = jest
      .fn()
      .mockResolvedValue({
        M: [],
        T: [{ id: "alkjdf", start: "0800", end: "0900" }],
        W: [{ id: "sNKta7HVufjdeYNFcPrv", start: "1700", end: "1800" }],
        R: [],
        F: []
      });
    const wrapper = shallow(<StudentProfAppointmentTable />);
    expect(wrapper.instance().gotoAppointmentScheduling).toBeDefined();
  });

  it("check setting of the state", () => {
    DatabaseService.getProfessorSessionsCacheForWeekWithDate = jest
      .fn()
      .mockResolvedValue({
        M: [],
        T: [{ id: "alkjdf", start: "0800", end: "0900" }],
        W: [{ id: "sNKta7HVufjdeYNFcPrv", start: "1700", end: "1800" }],
        R: [],
        F: []
      });
    const wrapper = shallow(
      <StudentProfAppointmentTable professorId="wolfebd@gcc.edu" />
    );
    wrapper.setState({ days: { M: [], T: [], W: [], R: [], F: [] } });
    expect(wrapper.state()).toBeDefined();
  });

  it("simulate pushback click", () => {
    const wrapper = mount(<StudentProfAppointmentTable professorId="wolfebd@gcc.edu" />);
    let spy = jest.spyOn(wrapper.instance(), "gotoAppointmentScheduling");
    wrapper.setState({
      days: { M: [], T: [["0800", "0830", "W"]], W: [], R: [], F: [] }
    });
    wrapper.update();
    wrapper.find(Button).simulate("click");
    expect(spy).toHaveBeenCalled();
  });
});
