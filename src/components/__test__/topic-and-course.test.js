/*
testing suite for TopicCourse, TopicButton, CourseButton

*** now spying and not mocking

TODO:
- get to 100%
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import Button from "@material-ui/core/Button";
import LogoutButton from "../logoutButton.jsx";
import TopicButton from "../topic-course-button.jsx";
import CourseButton from "../course-button.jsx";
import TopicCourse from "../topic-and-course.jsx";
import ListItem from "@material-ui/core/ListItem";
import TextField from "@material-ui/core/TextField";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

import Adapter from "enzyme-adapter-react-16";

const moment = require("moment");
const $ = require("jquery");

Enzyme.configure({ adapter: new Adapter() });

//mocking the parseUrlGetData function so that it doesnt actually run
jest.mock("../../../services/GetDataParser", () => ({
  parseUrlGetData: jest
    .fn()
    .mockReturnValue({ professorId: ["AgnewRL@gcc.edu"] })
}));

describe("TopicCourse", () => {
  /*----------------TOPIC AND COURSE TESTS ----------------*/
  //do appointments exist
  it("should be defined", () => {
    expect(TopicCourse).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("TandC should render correctly", () => {
    const tree = shallow(<TopicCourse />);
    expect(tree).toMatchSnapshot();
  });

  it("TandC should render correctly when the state is set", () => {
    const wrapper = shallow(<TopicCourse />);
    wrapper.setState({ topics: ["firebase"], courses: ["COMP 141"] });
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("topicSelected works", () => {
    const wrapper = shallow(<TopicCourse />);
    wrapper.instance().topicSelected("firebase");
    expect(wrapper.state().selected[0]).toBe("firebase");
  });

  it("topicSelected works when the topic is already selected", () => {
    const wrapper = shallow(<TopicCourse />);
    wrapper.setState({ selected: ["firebase", "COMP 141"] });
    wrapper.instance().topicSelected("firebase");
    expect(wrapper.state().selected[0]).toBe("");
  });

  it("courseSelected works", () => {
    const wrapper = shallow(<TopicCourse />);
    wrapper.instance().courseSelected("COMP 141");
    expect(wrapper.state().selected[1]).toBe("COMP 141");
  });

  it("courseSelected works when the course is already selected", () => {
    const wrapper = shallow(<TopicCourse />);
    wrapper.setState({ selected: ["firebase", "COMP 141"] });
    wrapper.instance().courseSelected("COMP 141");
    expect(wrapper.state().selected[1]).toBe("");
  });

  it("topicTextChange works", () => {
    const wrapper = shallow(<TopicCourse />);
    wrapper.instance().topicTextChange({ target: { value: "firebase" } });
    expect(wrapper.state().selected[0]).toBe("firebase");
  });

  it("topicTextChange updates when the user types", () => {
    const wrapper = mount(<TopicCourse />);
    wrapper.find(TextField).simulate("change", { target: { value: "test" } });
    expect(wrapper.state().selected[0]).toBe("test");
  });

  it("addToQueue exists", () => {
    const wrapper = mount(<TopicCourse />);
    let spy = jest.spyOn(wrapper.instance(), "addToQueue");
    wrapper.find(Button).simulate("click");
    expect(spy).toHaveBeenCalled();
  });

  /*----------------END OF TOPIC AND COURSE TESTS ----------------*/

  /*----------------TOPICBUTTON TESTS ----------------*/
  it("should be defined", () => {
    expect(TopicButton).toBeDefined();
  });

  it("TopicButton should render correctly (buttons should color)", () => {
    const tree = shallow(<TopicButton selected={true} />);
    expect(tree).toMatchSnapshot();
  });

  it("TopicButton should render correctly (buttons should color)", () => {
    const tree = shallow(<TopicButton selected={false} />);
    expect(tree).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new TopicButton();
    expect(check).toBeDefined();
  });

  it("clicked the course button", () => {
    const wrapper = mount(<TopicButton onTopicSelect={() => {}} />);
    wrapper.find(Button).simulate("click");
    expect(wrapper.instance().props.onTopicSelect).toHaveBeenCalled();
  });

  /*---------------- END OF TOPICBUTTON TESTS ----------------*/

  /*----------------COURSEBUTTON TESTS ----------------*/

  it("should be defined", () => {
    expect(CourseButton).toBeDefined();
  });

  it("CourseButton should render correctly (buttons are colored true)", () => {
    const tree = shallow(<CourseButton selected={true} />);
    expect(tree).toMatchSnapshot();
  });

  it("CourseButton should render correctly (buttons are colored false)", () => {
    const tree = shallow(<CourseButton selected={false} />);
    expect(tree).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new CourseButton();
    expect(check).toBeDefined();
  });

  it("clicked the course button", () => {
    const wrapper = mount(<CourseButton onCourseSelect={() => {}} />);
    wrapper.find(Button).simulate("click");
    expect(wrapper.instance().props.onClick).toHaveBeenCalled();
  });

  it("CourseButton should exist when there is a course in the state of TopicCourse", () => {
    const tree = shallow(<TopicCourse />);
    tree.state.courses = ["COMP 141 A"];
    tree.update();
    expect(tree.find(CourseButton)).toMatchSnapshot();
  });

  it("CourseButton onClick exists", () => {
    const wrapper = shallow(<CourseButton onClick={() => {}} />);
    expect(wrapper.find(Button).props().onClick).toBeDefined();
  });

  it("CourseButton name is defined", () => {
    const wrapper = shallow(<CourseButton />);
    expect(wrapper.name).toBeDefined();
  });

  /*----------------END OF COURSEBUTTON TESTS ----------------*/
});
