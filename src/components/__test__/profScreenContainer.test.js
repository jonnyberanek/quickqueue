/*
testing suite for ProfessorScreenContainer

*/

import ProfessorScreenContainer from "../professorScreenContainer.jsx";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";

import Typography from "@material-ui/core/Typography";

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Adapter from "enzyme-adapter-react-16";
import DatabaseService from "../../../services/DatabaseService";
import ProfessorAuthService from "../../../services/ProfessorAuthService";
const dbs = new DatabaseService();

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

//mocking the parseUrlGetData function so that it doesnt actually run
//jest.mock("../../../services/DatabaseService", () => ({

describe("ProfessorScreenContainer", () => {
  it("should be defined", () => {
    expect(ProfessorScreenContainer).toBeDefined();
  });

  it("ProfessorScreenContainer should be render", () => {
    const wrapper = render(<ProfessorScreenContainer />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new ProfessorScreenContainer();
    expect(check).toBeDefined();
  });
});
