/*
TESTING SUITE FOR STUDENTAPPOINTMENTS AND APPOINTMENTITEM

*** CHANGED FROM THE MOCKING STRUCTURE TO THE SPY 3.18.19 KP
*** over 60% 3.24.19 KP

1 TEST FAILS

TODO:
- get StudentAppointments to 100%
- figure out the props
*/
import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import StudentAppointments from "../studentAppointments.jsx";
import AppointmentItem from "../appointmentItem.jsx";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

import Adapter from "enzyme-adapter-react-16";

const moment = require("moment");

Enzyme.configure({ adapter: new Adapter() });

/* spying on example:
const video = require('./video');

test('plays video', () => {
  const spy = jest.spyOn(video, 'play');
  const isPlaying = video.play();

  expect(spy).toHaveBeenCalled();
  expect(isPlaying).toBe(true);

  spy.mockRestore();
});*/

//these props are for the AppointmentItem, when created
const defaultProps = {
  id: "id",
  start: moment(),
  topic: "firebase",
  end: moment(),
  onDelete: id => {
    StudentAppointments.deleteItem(id);
  }
};

describe("StudentAppointments", () => {
  //does the  StudentAppointments class exist
  it("should be defined", () => {
    expect(StudentAppointments).toBeDefined();
  });

  it("check constructor", () => {
    var check = new StudentAppointments();
    expect(check).toBeDefined();
  });

  //returns a StudentAppointments Snapshot that we can look at
  it("Appts should render correctly", () => {
    var wrapper = shallow(<StudentAppointments />);
    expect(wrapper).toMatchSnapshot();
  });

  it("the function deleteItem", () => {
    var wrapper = shallow(<StudentAppointments />);
    const spy = jest.spyOn(wrapper.instance(), "deleteItem");
    wrapper.instance().deleteItem("akjsdfljalsd");
    expect(spy).toHaveBeenCalledTimes(1);
    spy.mockRestore();
    //TODO: - NEEDS TO CHECK THAT THE ID HAS ACTUALLY BEEN DELETED
  });

  it("the setState changes the state as expected", () => {
    var wrapper = shallow(<StudentAppointments />);
    var newState = [
      {
        id: "alksdjflajsdvuei",
        start_time: moment(),
        topic: "test",
        end_time: moment(),
        name: "John Doe"
      }
    ];
    wrapper.setState({ items: newState });
    expect(wrapper.state().items).toBe(newState);
  });

  it("The deleteItem function should alter the state", () => {
    var wrapper = shallow(<StudentAppointments />);
    var newState = [
      {
        id: "alksdjflai",
        start_time: moment(),
        topic: "test",
        end_time: moment(),
        name: "John Doe"
      },
      {
        id: "alksdjflajsdvuei",
        start_time: moment(),
        topic: "test",
        end_time: moment(),
        name: "John Shmoe"
      }
    ];
    wrapper.setState({ items: newState });
    expect(wrapper.state().items.length).toBe(2);
    wrapper.instance().deleteItem("alksdjflajsdvuei");
    expect(wrapper.state().items.length).toBe(1);
  });

  //---------- AppointmentItem tests -------------------

  it("StudentAppts defines the props correctly", () => {
    var wrapper = mount(<AppointmentItem {...defaultProps} />);
    expect(wrapper.props().onDelete).toBeDefined();
  });

  it("simulate pushback click", () => {
    const wrapper = mount(<AppointmentItem onDelete={() => {}} />);
    wrapper.find(Button).simulate("click");
    expect(wrapper.find(Button).props.onDelete()).toHaveBeenCalled();
  });
});
