/*
testing suite for nextAppt

ALL TESTS PASS

TODO:
- get deleteItem tests to test the setting of the state
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import AppointmentItem from "../appointmentItem.jsx";
import NextAppt from "../nextAppt.jsx";
import DatabaseService from "../../../services/DatabaseService";
import Adapter from "enzyme-adapter-react-16";

const moment = require("moment");
const $ = require("jquery");

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("NextAppt", () => {
  it("should be defined", () => {
    expect(NextAppt).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    const wrapper = shallow(<NextAppt profId="wolfebd@gcc.edu" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new NextAppt();
    expect(check).toBeDefined();
  });

  it("setting the state should test a lot of the render function", () => {
    const wrapper = shallow(<NextAppt profId="wolfebd@gcc.edu" profId="wolfebd@gcc.edu" />);
    wrapper.setState({
      appt: [
        "UzaOsmo1Q6J9Z4RLRft2",
        "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
        "Arrays",
        "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
        "Karen Postupac"
      ]
    });
    //check that the wrapper returned in the render function
    expect(wrapper.type()).not.toEqual(null);
  });

  it("deleteItem should be called", () => {
    DatabaseService.getNextAppointment = jest.fn("wolfebd@gcc.edu", done => {
      done([[]]);
    });
    const wrapper = mount(<NextAppt profId="wolfebd@gcc.edu" />);
    wrapper.setState({
      appt: [
        "id",
        "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
        "Arrays",
        "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
        "Karen Postupac"
      ]
    });
    wrapper.instance().deleteItem("id");
    expect(wrapper.state()).toBe({ appt: [] });
  });
});
