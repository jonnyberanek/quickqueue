/*
testing suite for the topic graph

ALL TESTS PASS

TODO:
- get deleteItem tests to test the setting of the state
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import Button from "@material-ui/core/Button";
import Star from "@material-ui/icons/Star";
import StarBorder from "@material-ui/icons/StarBorder";
import DatabaseService from "../../../services/DatabaseService.js";
import Typography from "@material-ui/core/Typography";
import Adapter from "enzyme-adapter-react-16";
import FavIcon from "../favIcon.jsx";

const moment = require("moment");
const $ = require("jquery");

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("All FavIcon testing", () => {
  it("FavIcon - should be defined", () => {
    expect(FavIcon).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("FavIcon- should render correctly", () => {
    const wrapper = shallow(<FavIcon profId="wolfebd@gcc.edu" studId="10g49" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("FavIcon- check constructor", () => {
    const check = new FavIcon({ profId: "wolfebd@gcc.edu", studId: "10g49" });
    expect(check).toBeDefined();
  });

  it("FavIcon -  onChange when isFavorited is false", () => {
    const wrapper = mount(<FavIcon profId="wolfebd@gcc.edu" studId="10g49" />);
    wrapper.find(Button).simulate("click");
    expect(wrapper.instance().props.onClick).toHaveBeenCalled();
  });

  it("FavIcon -  onChange when isFavorited is true", () => {
    const wrapper = mount(<FavIcon profId="wolfebd@gcc.edu" studId="10g49" />);
    wrapper.setState({ isFavorited: true });
    wrapper.update();
    wrapper.find(Button).simulate("click");
    expect(wrapper.instance().props.onClick).toHaveBeenCalled();
  });

  it("FavIcon - change fav", () => {
    const wrapper = mount(<FavIcon profId="wolfebd@gcc.edu" studId="10g49" />);
    wrapper.setState({ isFavorited: true });
    wrapper.update();
    wrapper.instance().favToggle();
    expect(wrapper.state().isFavorited).toBeFalsy();
  });
});
