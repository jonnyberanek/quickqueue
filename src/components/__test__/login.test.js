/*
TESTING SUITE FOR LOGINMODULE

ALL TESTS PASS

TODO:
- get to 100%
*/
import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import LoginModule from "../LoginModule.jsx";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";

import TextField from "@material-ui/core/TextField";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

describe("LoginModule", () => {
  it("should be defined", () => {
    expect(LoginModule).toBeDefined();
  });

  it("should render correctly", () => {
    const tree = shallow(<LoginModule />);
    expect(tree).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new LoginModule();
    expect(check).toBeDefined();
  });

  it("check that the functions and props exist", () => {
    const wrapper = shallow(<LoginModule />);
    expect(wrapper.instance().login).toBeDefined();
    expect(wrapper.instance().formRef).toBeDefined();
    expect(wrapper.instance().inputData).toBeDefined();
  });

  it("check that the login button works", () => {
    const wrapper = shallow(<LoginModule />);
    wrapper.instance().login(mock => {
      expect(mock).toHaveBeenCalled;
    });
  });

  it("check that the first textfield change works", () => {
    const wrapper = shallow(<LoginModule />);
    wrapper
      .find(TextField)
      .at(0)
      .simulate("change", { target: { value: "hello" } });
    expect(wrapper.instance().inputData.email).toBe("hello");
  });

  it("check that the second textfield change works", () => {
    const wrapper = shallow(<LoginModule />);
    wrapper
      .find(TextField)
      .at(1)
      .simulate("change", { target: { value: "hello" } });
    expect(wrapper.instance().inputData.password).toBe("hello");
  });

  it("check that the third textfield change works", () => {
    const wrapper = shallow(<LoginModule />);
    let spy = jest.spyOn(wrapper.instance(), "login");
    wrapper.find(Button).simulate("click");
    expect(wrapper.find(Button).props.onClick).toHaveBeenCalled();
  });
});
