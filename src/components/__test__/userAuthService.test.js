/*
testing suite for student and prof auth services

ALL TESTS PASS

TODO:
- get deleteItem tests to test the setting of the state
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Adapter from "enzyme-adapter-react-16";

import StudentAuthService from "../../../services/StudentAuthService.js";
import ProfessorAuthService from "../../../services/ProfessorAuthService.js";
import DatabaseService from "../../../services/DatabaseService.js";
var dbs = new DatabaseService();

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("UserAuthService", () => {
  it("should be defined", () => {
    expect(StudentAuthService).toBeDefined();
    expect(ProfessorAuthService).toBeDefined();
  });

  it("check constructor of prof", () => {
    const profAuth = new ProfessorAuthService(firebase);
    expect(profAuth).toBeDefined();
  });

  it("check constructor of stud", () => {
    const studAuth = new StudentAuthService(firebase);
    expect(studAuth).toBeDefined();
  });

  it("check onValidUserLoad", () => {
    const studAuth = new StudentAuthService(firebase);
    studAuth.onValidUserLoad(user => {
      expect(user).toBeDefined();
    });
  });

  it("check authStateChanged", () => {
    let user = { email: "kp@kp.com" };
    const studAuth = new StudentAuthService(firebase);
    let spy = jest.spyOn(DatabaseService.prototype, "getTypeOfLoggedInUser");
    let spy2 = jest.spyOn(DatabaseService.prototype, "getLoggedInUser");
    studAuth.authStateChanged(user);
    dbs.getTypeOfLoggedInUser = jest.fn().mockReturnValue("student");
    studAuth.authStateChanged(user);
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
