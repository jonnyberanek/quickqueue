/*
testing suite for getInLineCard

3 tests fail

TODO:
- find a way to set the window.location.href
-----hopefully that will allow the two failing tests to pass
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import GetInLineCard from "../getInLineCard.jsx";

import "../../style/studentInLineCard.css";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";

import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import StudentInLineCard from "../studentInLineCard.jsx";

import parsingUtils from "../../../services/GetDataParser";

import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

import DatabaseService from "../../../services/DatabaseService.js";
const dbs = new DatabaseService();

const firebase = require("firebase/app");
require("../../../services/FirebaseInit").init(firebase);

//mocking the parseUrlGetData function so that it doesnt actually run
jest.mock("../../../services/GetDataParser", () => ({
  parseUrlGetData: jest
    .fn()
    .mockReturnValue({ professorId: ["AgnewRL@gcc.edu"] })
}));

describe("getInLineCard", () => {
  it("should be defined", () => {
    // parsingUtils.parseUrlGetData = jest
    //   .fn()
    //   .mockReturnValue({ professorId: ["AgnewRL@gcc.edu"] });
    expect(GetInLineCard).toBeDefined();
  });
  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    // parsingUtils.parseUrlGetData = jest
    //   .fn()
    //   .mockReturnValue({ professorId: ["AgnewRL@gcc.edu"] });
    //mocking the getNumOfPeopleInLine function so that it doesnt actually run
    dbs.getNumOfPeopleInLine = jest.fn().mockResolvedValue(2);
    const wrapper = shallow(<GetInLineCard />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new GetInLineCard();
    expect(check).toBeDefined();
  });

  it("check changing of state", () => {
    //mocking the getNumOfPeopleInLine function so that it doesnt actually run
    dbs.getNumOfPeopleInLine = jest.fn().mockResolvedValue(2);
    const wrapper = shallow(<GetInLineCard />);
    //checking to make sure the mocking of getNumOfPeopleInLine works
    expect(wrapper.state().people_in_line).toBe(2);
    wrapper.setState({ people_in_line: 4 });
    expect(wrapper).toMatchSnapshot();
  });

  it("simulate click", () => {
    let windowLocation = "";
    const wrapper = mount(
      <GetInLineCard
        onClick={() => {
          windowLocation =
            "/student/topic-and-course?professorId=AgnewRL@gcc.edu";
        }}
      />
    );
    wrapper.find(Button).simulate("click");
    expect(windowLocation).toBe(
      "/student/topic-and-course?professorId=AgnewRL@gcc.edu"
    );
  });
});
