import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

import DatabaseService from "../../../services/DatabaseService";
import Adapter from "enzyme-adapter-react-16";

import EditHoursTypePicker from "../EditHoursTypePicker.jsx";

const moment = require("moment");
const $ = require("jquery");

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

window.URL.createObjectURL = function() {};
let event = { target: { value: "61606" } };
event.persist = jest.fn(function() {});

Enzyme.configure({ adapter: new Adapter() });

describe("All analytics testing", () => {
  it("EditHoursTypePicker - should be defined", () => {
    expect(EditHoursTypePicker).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("EditHoursTypePicker - should render correctly", () => {
    const wrapper = shallow(<EditHoursTypePicker profId="wolfebd@gcc.edu" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("EditHoursTypePicker- check constructor", () => {
    const check = new EditHoursTypePicker({ profId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  it("EditHoursTypePicker-  onChange", () => {
    const wrapper = mount(<EditHoursTypePicker profId="wolfebd@gcc.edu" />);
    let spy = jest.spyOn(wrapper.instance(), "handleChange");
    wrapper.find(Tabs).simulate("change", event);
    expect(spy).toHaveBeenCalled();
  });
});
