/*
testing suite for the topic graph

ALL TESTS PASS

TODO:
- get deleteItem tests to test the setting of the state
*/

//jest.unmock("react-plotly.js");

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import DatabaseService from "../../../services/DatabaseService";
import Adapter from "enzyme-adapter-react-16";
import TopicGraph from "../TopicGraph.jsx";
import OfficeHourGraph from "../OfficeHourGraph.jsx";
import EditGraphTypePicker from "../graphTypePicker.jsx";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Plot from "react-plotly.js";

const moment = require("moment");
const $ = require("jquery");

window.URL.createObjectURL = function() {};
let event = { target: { value: "61606" } };
event.persist = jest.fn(function() {});

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("All analytics testing", () => {
  it("EditGraphTypePicker - should be defined", () => {
    expect(EditGraphTypePicker).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it(" EditGraphTypePicker - should render correctly", () => {
    const wrapper = shallow(<EditGraphTypePicker profId="wolfebd@gcc.edu" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("EditGraphTypePicker - check constructor", () => {
    const check = new EditGraphTypePicker({ profId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  it("EditGraphTypePicker -  onChange", () => {
    const wrapper = mount(<EditGraphTypePicker profId="wolfebd@gcc.edu" />);
    let spy = jest.spyOn(wrapper.instance(), "handleChange");
    wrapper.find(Tabs).simulate("change", event);
    expect(spy).toHaveBeenCalled();
  });

  it("TopicGraph - course change correctly sets state", () => {
    const wrapper = mount(
      <EditGraphTypePicker profId="wolfebd@gcc.edu" onChange={() => {}} />
    );
    wrapper.instance().handleChange(event, 0);
    expect(wrapper.state().value).toBe(0);
  });

  it("should be defined", () => {
    expect(TopicGraph).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    const wrapper = shallow(<TopicGraph />);
    wrapper.setState({ courseOptions: ["COMP 141"] });
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new TopicGraph();
    expect(check).toBeDefined();
  });

  it("TopicGraph - radio group time change", () => {
    const wrapper = mount(<TopicGraph onChange={() => {}} />);
    let spy = jest.spyOn(wrapper.instance(), "changeTime");
    wrapper
      .find(RadioGroup)
      .at(0)
      .simulate("change", { target: { value: "hi" } });
    expect(spy).toHaveBeenCalled();
  });

  it("TopicGraph - time change correctly sets state", () => {
    const wrapper = mount(<TopicGraph onChange={() => {}} />);
    wrapper.instance().changeTime({ target: { value: "61606" } });
    expect(wrapper.state().time).toBe("61606");
  });

  it("TopicGraph - radio group course change", () => {
    const wrapper = mount(<TopicGraph onChange={() => {}} />);
    let spy = jest.spyOn(wrapper.instance(), "changeCourse");
    wrapper
      .find(RadioGroup)
      .at(1)
      .simulate("change", { target: { value: "61606" } });
    expect(spy).toHaveBeenCalled();
  });

  it("TopicGraph - course change correctly sets state", () => {
    const wrapper = mount(<TopicGraph onChange={() => {}} />);
    wrapper.instance().changeCourse({ target: { value: "61606" } });
    expect(wrapper.state().course).toBe("61606");
  });

  it(" OfficeHourGraph should be defined", () => {
    expect(OfficeHourGraph).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    const wrapper = shallow(<OfficeHourGraph onChange={() => {}} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new OfficeHourGraph();
    expect(check).toBeDefined();
  });

  it("OfficeHourGraph - radio group onChange", () => {
    const wrapper = shallow(<OfficeHourGraph onChange={() => {}} />);
    let spy = jest.spyOn(wrapper.instance(), "changeTime");
    wrapper
      .find(RadioGroup)
      .at(0)
      .simulate("change", { target: { value: "this" } });
    expect(spy).toHaveBeenCalled();
  });
});
