/*
TESTING SUITE FOR STUDENTQUEUE & QUEUEITEM

1 TEST FAILS

TODO:
- get last test to pass

*/
import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import Queue from "../studentQueue.jsx";
import QueueItem from "../queueItem.jsx";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import DatabaseService from "../../../services/DatabaseService";

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

const defaultState = {
  items: [
    {
      studentName: "Karen Postupac",
      courseId: "COMP 141 A",
      topic: "firebase",
      id: "id",
      studentId: 10g49,
      place_in_line: 1,
      ticket_number: 2
    },
    {
      studentName: "Dan Smith",
      courseId: "COMP 220 A",
      topic: "for loops",
      id: "id2",
      studentId: 100003,
      place_in_line: 2,
      ticket_number: 5
    }
  ]
};

const queueItemProps = {
  item: [{ id: "id", isFirst: true }],
  key: "hello",
  onDelete: Queue.deleteItem
};

describe("Queue", () => {
  /* -------------------- QUEUE EXISTANCE TESTS ---------------------------------------------------   */
  //does the Queue exist
  it("should be defined", () => {
    expect(Queue).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("Queue should render correctly", () => {
    const tree = shallow(<Queue profId="wolfebd@gcc.edu" />);
    tree.state = defaultState;
    tree.update();
    expect(tree).toMatchSnapshot();
  });

  it("check constructor ", () => {
    const check = new Queue({ profId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  it("the function pushbackItem should exist when Queue is initialized", () => {
    DatabaseService.pushbackStudent = jest.fn().mockReturnValue(true);
    const wrapper = mount(<Queue profId="wolfebd@gcc.edu" />);
    return expect(wrapper.instance().pushbackItem()).resolves.toBe();
  });

  it("the function deleteItem should be defined and returns", () => {
    DatabaseService.deleteStudentFromQueue = jest.fn().mockReturnValue(true);
    const wrapper = mount(<Queue profId="wolfebd@gcc.edu" />);
    return expect(wrapper.instance().deleteItem()).resolves.toBe();
  });

  it("the function refTasks should exist when Queue is initialized", () => {
    //querySnapshot = new firebase.firestore.DocumentSnaphot();
    const wrapper = shallow(<Queue profId="wolfebd@gcc.edu" />);
    return expect(wrapper.instance().refTasks()).resolves.not.toBe(null);
  });

  /* -------------------- END OF QUEUE EXISTANCE TESTS ---------------------------------------------------   */

  /* -------------------- QUEUEITEM TESTS ---------------------------------------------------   */
  it("QueueItem should exist", () => {
    expect(QueueItem).toBeDefined();
  });

  it("QueueItem should render correctly", () => {
    const tree = shallow(<QueueItem {...queueItemProps} />);
    expect(tree).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new QueueItem();
    expect(check).toBeDefined();
  });

  it("QueueItem - simulate click on the delete button", () => {
    const wrapper = shallow(
      <QueueItem item={{ isFirst: false }} onDelete={() => {}} />
    );
    wrapper.find(Button).simulate("click");
    expect(wrapper.instance().props.onClick).toHaveBeenCalled();
  });

  it("QueueItem - simulate click on the pushback button", () => {
    const wrapper = shallow(
      <QueueItem item={{ isFirst: true }} onDelete={() => {}} />
    );
    wrapper
      .find(Button)
      .at(0)
      .simulate("click");
    expect(wrapper.instance().props.onClick).toHaveBeenCalled();
  });
  /* ------------- END OF QUEUEITEM TESTS ---------------------------------------------------   */
});
