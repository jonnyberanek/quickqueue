/*
testing suite for making appointments

*** Got rid of the mocking functions 3.18.19

all tests pass! hooray!

TODO:
- 100%
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import MakeAppointment from "../makeAppointment.jsx";

import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Chip from "@material-ui/core/Chip";

import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

import DatabaseService from "../../../services/DatabaseService";
import parsingUtils from "../../../services/GetDataParser.js";
const dbs = new DatabaseService();

//mocking the parseUrlGetData function so that it doesnt actually run
jest.mock("../../../services/GetDataParser", () => ({
  parseUrlGetData: jest
    .fn()
    .mockReturnValue({ professorId: ["AgnewRL@gcc.edu"] })
}));

describe("makeAppointment", () => {
  it("should be defined", () => {
    expect(MakeAppointment).toBeDefined();
  });

  it("should render correctly", () => {
    //  parseUrlGetData = jest.fn().mockReturnValue({ professorId: ["wolfebd@gcc.edu"] });
    var wrapper = mount(<MakeAppointment email="kp@kp.com" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new MakeAppointment("kp@kp.com");
    expect(check).toBeDefined();
  });

  it("check if functions are defined", () => {
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    expect(wrapper.instance().validate).toBeDefined();
    expect(wrapper.instance().componentDidMount).toBeDefined();
    expect(wrapper.instance().dateRangeOverlaps).toBeDefined();
    expect(wrapper.instance().submitForm).toBeDefined();
    expect(wrapper.instance().handleDateTimeChanges).toBeDefined();
    expect(wrapper.instance().handleTopicChange).toBeDefined();
    expect(wrapper.instance().handleChipClicked).toBeDefined();
    expect(wrapper.instance().handleCourseChip).toBeDefined();
    expect(wrapper.instance().handleCourseChange).toBeDefined();
    expect(wrapper.instance().handleChange).toBeDefined();
  });

  it("check validate", () => {
    DatabaseService.getProfAppointments = jest
      .fn()
      .mockResolvedValue([{ start_time: "0800", end_time: "0900" }]);
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.setState({
      teacher: "wolfebd@gcc.edu",
      topic: "test",
      student: "10g49",
      course: "COMP 141 A"
    });
    expect(wrapper.instance().validate()).toBeTruthy();
    wrapper.setState({
      teacher: "wolfebd@gcc.edu",
      topic: "test",
      student: "10g49",
      course: ""
    });
    expect(wrapper.instance().validate()).toBeFalsy();
    expect(wrapper.instance().submitForm()).toBeFalsy();
  });

  it("check componentDidMount", () => {
    /*
    const mockCallback = jest.fn(x => 42 + x);
forEach([0, 1], mockCallback);
*/
    DatabaseService.getLoggedInUserID = jest.fn("kp@kp.com", done => {
      done("kp@kp.com");
    });
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.instance().componentDidMount();
    expect(wrapper.state().student).toBeDefined();
  });

  it("check dateRangeOverlaps", () => {
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    expect(wrapper.instance().dateRangeOverlaps()).toBeFalsy();
    expect(wrapper.instance().dateRangeOverlaps(8, 9, 8.5, 9.5)).toBeTruthy();
    expect(wrapper.instance().dateRangeOverlaps(8.5, 9.5, 8, 9)).toBeTruthy();
    expect(wrapper.instance().dateRangeOverlaps(8.5, 10.5, 9, 10)).toBeTruthy();
    expect(wrapper.instance().dateRangeOverlaps(9, 10, 8.5, 10.5)).toBeTruthy();
  });

  it("check handleChipClicked", () => {
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.instance().handleChipClicked("hello");
    expect(wrapper.state().topic).toBe("hello");
  });

  it("check handleCourseChip", () => {
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.instance().handleCourseChip("hello");
    expect(wrapper.state().course).toBe("hello");
  });

  it("check handleCourseChange", () => {
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.instance().handleCourseChange({ target: { value: "hello" } });
    expect(wrapper.state().course).toBe("hello");
  });

  it("check handleTopicChange", () => {
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.instance().handleTopicChange({ target: { value: "hello" } });
    expect(wrapper.state().topic).toBe("hello");
  });

  it("check handleDateTimeChanges", () => {
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.instance().handleDateTimeChanges("start")({
      target: { value: "hello" }
    });
    expect(wrapper.state().start).toBe("hello");
  });

  //mock dbs.getTopicsFromHistoricalVisits
  it("check handleChange", () => {
    DatabaseService.getTopicsFromHistoricalVisits = jest.fn("wolfebd@gcc.edu", done => {
      done(["firebase", "test"]);
    });
    var wrapper = shallow(<MakeAppointment email="kp@kp.com" />);
    wrapper.instance().handleChange();
    expect(wrapper.state().topicSuggestions).not.toBe(null);
  });
});
