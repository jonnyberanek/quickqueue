/*
testing suite for professorNavBar

*/
import ProfessorNavBar from "../professorNavBar.jsx";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Adapter from "enzyme-adapter-react-16";
import DatabaseService from "../../../services/DatabaseService";
var dbs = new DatabaseService();

Enzyme.configure({ adapter: new Adapter() });

describe("ProfessorNavBar", () => {
  it("should be defined", () => {
    expect(ProfessorNavBar).toBeDefined();
  });

  it("ProfessorNavBar should be render", () => {
    const wrapper = shallow(<ProfessorNavBar />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new ProfessorNavBar();
    expect(check).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("window location should change when clicked", () => {
    const wrapper = mount(<ProfessorNavBar />);
    wrapper.find(Typography).simulate("click");
    expect(wrapper.find(Typography).props.onClick).toHaveBeenCalled();
  });
});
