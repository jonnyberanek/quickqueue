/*
testing suite for StudentInLineCard

1 test fails

TODO:
- figure out the node business
- and the mock business - i dont think its running through like i want it to
*/
import StudentInLineCard from "../studentInLineCard.jsx";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";

import Typography from "@material-ui/core/Typography";

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Adapter from "enzyme-adapter-react-16";
import DatabaseService from "../../../services/DatabaseService";
const dbs = new DatabaseService();

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

//mocking the parseUrlGetData function so that it doesnt actually run
//jest.mock("../../../services/DatabaseService", () => ({

describe("StudentInLineCard ", () => {
  it("should be defined", () => {
    expect(StudentInLineCard).toBeDefined();
  });

  it("StudentInLineCard should be render", () => {
    dbs.getStudentQueueNumber = jest.fn().mockResolvedValue(3);
    const wrapper = render(<StudentInLineCard profId="wolfebd@gcc.edu" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    dbs.getStudentQueueNumber = jest.fn().mockResolvedValue(3);
    var check = new StudentInLineCard({ profId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("StudentInLineCard should call DBS's deleteStudentFromQueue when clicking on finish", () => {
    dbs.getStudentQueueNumber = jest.fn().mockResolvedValue(3);
    const wrapper = mount(<StudentInLineCard profId="wolfebd@gcc.edu" />);
    let spy = jest.spyOn(wrapper.instance(), "onFinishedClick");
    wrapper.find(Button).simulate("click");
    expect(spy).toHaveBeenCalled();
  });

  it("StudentInLineCard should/should not render upon shouldRender", () => {
    dbs.getStudentQueueNumber = jest.fn().mockResolvedValue(3);
    const wrapper = shallow(<StudentInLineCard profId="wolfebd@gcc.edu" />);
    wrapper.setState({ shouldRender: false });
    expect(wrapper.type()).toEqual(null);
    wrapper.setState({ shouldRender: true });
    expect(wrapper.type()).not.toEqual(null);
  });
});
