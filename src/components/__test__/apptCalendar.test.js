/*
TESTING SUITE FOR APPTCALENDAR

*** CHANGED FROM THE MOCKING STRUCTURE TO THE SPY 3.18.19 KP
*** over 60% 3.24.19 KP
*** jonny broke everything 3.25.19 - stay tuned

TODO:
- when checking the constructor, the getAllAppointments comes back null
     -----I think its because firebase isnt init
- get to 100%
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";

import ApptCalendar from "../apptCalendar.jsx";
import ApptCalWeek from "../apptCalWeek.jsx";
import Adapter from "enzyme-adapter-react-16";
import Typography from "@material-ui/core/Typography";
import ArrowRight from "@material-ui/icons/ArrowRight";
import ArrowLeft from "@material-ui/icons/ArrowLeft";

const moment = require("moment");
const $ = require("jquery");

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("apptCalendar", () => {
  it("should be defined", () => {
    expect(ApptCalendar).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    const wrapper = shallow(<ApptCalendar profId="wolfebd@gcc.edu" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    //For this test, check in the db, and make sure
    //there are appts that fall on each day of the week
    const check = new ApptCalendar({ profId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  it("setting the state should test a lot of the render function", () => {
    const wrapper = shallow(<ApptCalendar profId="wolfebd@gcc.edu" />);
    wrapper.setState({
      appts: [
        [
          [
            "UzaOsmo1Q6J9Z4RLRft2",
            "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
            "Arrays",
            "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
            "Karen Postupac"
          ]
        ],
        [
          [
            "UzaOsmo1Q6J9Z4RLRft2",
            "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
            "Arrays",
            "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
            "Karen Postupac"
          ]
        ],
        [
          [
            "UzaOsmo1Q6J9Z4RLRft2",
            "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
            "Arrays",
            "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
            "Karen Postupac"
          ]
        ],
        [[]],
        [
          [
            "UzaOsmo1Q6J9Z4RLRft2",
            "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
            "Arrays",
            "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
            "Karen Postupac"
          ],
          [
            "UzaOsmo1Q6J9Z4RLRft2",
            "Mon Dec 31 2018 23:30:00 GMT-0500 (Eastern Standard Time)",
            "Arrays",
            "Tue Jan 01 2019 00:30:00 GMT-0500 (Eastern Standard Time)",
            "Karen Postupac"
          ]
        ]
      ]
    });
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("the function changeWeek should exist and change the state when 'left' is passed in", () => {
    //For this test, check in the db, and make sure
    //there are appts that fall on each day of the week
    const wrapper = shallow(<ApptCalendar profId="wolfebd@gcc.edu" />);
    expect(wrapper.instance().changeWeek).toBeDefined();
    let oldState = wrapper.state().week;
    wrapper.instance().changeWeek("left");
    expect(wrapper.state().week).toBe(wrapper.state().week.day(-6));
  });

  it("simulate the click of the 'left' arrow", () => {
    const spy = jest.spyOn(ApptCalendar.prototype, "changeWeek");
    const wrapper = mount(<ApptCalWeek change={() => {}} />);
    wrapper.find(ArrowLeft).simulate("click");
    expect(spy).toHaveBeenCalled();
  });

  it("the function changeWeek should change the state when 'right' is passed in", () => {
    //For this test, check in the db, and make sure
    //there are appts that fall on each day of the week
    const wrapper = shallow(<ApptCalendar profId="wolfebd@gcc.edu" />);
    var oldState = wrapper.state().week;
    wrapper.instance().changeWeek("right");
    expect(wrapper.state().week).toBe(wrapper.state().week.day(8));
  });

  it("simulate the click of the 'right' arrow", () => {
    const spy = jest.spyOn(ApptCalendar.prototype, "changeWeek");
    const wrapper = mount(<ApptCalWeek change={() => {}} />);
    wrapper.find(ArrowRight).simulate("click");
    expect(spy).toHaveBeenCalled();
  });
});
