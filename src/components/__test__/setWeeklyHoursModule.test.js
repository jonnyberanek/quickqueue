/*
testing suite for  SetWeeklyHourModule

2 TESTS FAILS

TODO:
- get confirmClicked get through the dbs calls
- test dateChanged
- test doSessionsMatch
- line 90 (of this file), make the cache state useful
*/
import DatabaseService from "../../../services/DatabaseService";
import formatDayCharacter from "../../../services/FormatDayCharacter";

import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import SetWeeklyHourModule from "../setWeeklyHoursModule.jsx";
import "../../style/setWeeklyHoursModule.css";

import MomentUtils from "@date-io/moment";
import {
  MuiPickersUtilsProvider,
  TimePicker,
  Calendar
} from "material-ui-pickers";

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Adapter from "enzyme-adapter-react-16";

const moment = require("moment");
const uuid = require("uuid/v4");

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

var dbs = new DatabaseService();

describe("makeAppointment", () => {
  it("should be defined", () => {
    expect(SetWeeklyHourModule).toBeDefined();
  });
  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    var wrapper = shallow(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new SetWeeklyHourModule("wolfebd@gcc.edu");
    expect(check).toBeDefined();
  });

  it("check if functions are defined", () => {
    var wrapper = shallow(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    expect(wrapper.instance().dateChanged).toBeDefined();
    expect(wrapper.instance().refreshDate).toBeDefined();
    expect(wrapper.instance().doSessionsMatch).toBeDefined();
    expect(wrapper.instance().addClicked).toBeDefined();
    expect(wrapper.instance().deleteSession).toBeDefined();
    expect(wrapper.instance().confirmClicked).toBeDefined();
    expect(wrapper.instance().onDialogClose).toBeDefined();
  });

  it("test dateChanged", () => {
    var wrapper = mount(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    wrapper.instance().dateChanged(moment(), true);
    expect(wrapper.state().selectedDate).not.toBe(null);
  });

  it("test refreshDate", () => {
    var wrapper = mount(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    wrapper.instance().refreshDate(moment());
    expect(wrapper.state().cache).not.toBe(null);
  });

  it("test doSessionsMatch", () => {
    var wrapper = mount(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    let session1 = { start: "0800", end: "0900", day: "M", week: "1" };
    let session2 = { start: "0800", end: "0900", day: "M", week: "1" };
    let session3 = { start: "0800", end: "1000", day: "M", week: "1" };
    expect(wrapper.instance().doSessionsMatch(session1, session2)).toBe(true);
    expect(wrapper.instance().doSessionsMatch(session1, session3)).toBe(false);
  });

  it("test addClicked", () => {
    var wrapper = mount(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    wrapper.instance().addClicked();
    expect(wrapper.state().openDialog).toBe(true);
  });

  it("test addClicked is called when clicked", () => {
    var wrapper = mount(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    let spy = jest.spyOn(wrapper.instance(), "addClicked");
    wrapper
      .find(Button)
      .find(".add")
      .simulate("click");
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it("test addSession", () => {
    var wrapper = mount(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    let oldState = wrapper.state();
    wrapper.instance().addSession("0800", "0900", "W");
    expect(oldState).not.toBe(wrapper.state());
  });

  it("test addSession is called when clicked", () => {
    var wrapper = mount(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    let spy = jest.spyOn(wrapper.instance(), "addSession");
    wrapper.setState({ cache: "hello" });
    wrapper.find(CustomDialog).simulate("handleClose");
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it("test deleteSession", () => {
    var wrapper = shallow(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    wrapper.instance().additions["Monday"]["id"] = {
      start: "0800",
      end: "0900",
      day: formatDayCharacter("Monday"),
      id: "id",
      isNew: true
    };
    let spy = jest.spyOn(wrapper.instance(), "deleteSession");
    //in the future, this should actually be something useful
    wrapper.setState({ cache: "initialized", selectedDate: "M" });
    wrapper.instance().deleteSession(0, "sessionId");
    //trip the beginning if stmt of the deleteSession function
    wrapper.instance().deleteSession(0);
    expect(spy).toHaveBeenCalled();
  });

  it("test confirmClicked", () => {
    let spy = jest.spyOn(window, "alert");
    var wrapper = shallow(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    wrapper.instance().confirmClicked();
    expect(spy).toHaveBeenCalled();
  });

  it("test onDialogClose", () => {
    var wrapper = shallow(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    wrapper.instance().onDialogClose();
    expect(wrapper.state().openDialog).toBe(false);
  });

  it("test formatFirestoreDate", () => {
    var wrapper = shallow(<SetWeeklyHourModule profId="wolfebd@gcc.edu" />);
    let time = wrapper
      .instance()
      .formatFirestoreDate(firebase.firestore.Timestamp.now());
    expect(time).not.toBe(null);
    expect(time).toBe(
      moment(firebase.firestore.Timestamp.now(), "HHmm").format("HH:mm")
    );
  });

  //--------------TEST FormatDayCharacter

  it("test formatDayCharacter", () => {
    //var mockCallBack = jest.fn();
    var day = formatDayCharacter(moment("March 17, 2019"));
    //expect(mockCallBack.mock.calls.length).toEqual(1);
    expect(day).toBe("U");
    day = formatDayCharacter(moment("March 18, 2019"));
    expect(day).toBe("M");
    day = formatDayCharacter(moment("March 19, 2019"));
    expect(day).toBe("T");
    day = formatDayCharacter(moment("March 20, 2019"));
    expect(day).toBe("W");
    day = formatDayCharacter(moment("March 21, 2019"));
    expect(day).toBe("R");
    day = formatDayCharacter(moment("March 22, 2019"));
    expect(day).toBe("F");
    day = formatDayCharacter(moment("March 23, 2019"));
    expect(day).toBe("S");
  });
});
