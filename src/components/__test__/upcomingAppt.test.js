/*
testing suite for upcomingApptCard

1 test fails

TODO:
- figure out the node business
- and the mock business - i dont think its running through like i want it to
*/
import UpcomingApptCard from "../upcomingApptCard.jsx";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

import Typography from "@material-ui/core/Typography";

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import Adapter from "enzyme-adapter-react-16";
import DatabaseService from "../../../services/DatabaseService";
const dbs = new DatabaseService();

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

//mocking the parseUrlGetData function so that it doesnt actually run
//jest.mock("../../../services/DatabaseService", () => ({

describe("UpcomingApptCard ", () => {
  it("should be defined", () => {
    expect(UpcomingApptCard).toBeDefined();
  });

  it("UpcomingApptCard should be render", () => {
    const wrapper = render(
      <UpcomingApptCard profId="wolfebd@gcc.edu" studId="10g49" />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new UpcomingApptCard({ profId: "wolfebd@gcc.edu", studId: "10g49" });
    expect(check).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("StudentInLineCard should call DBS's deleteStudentFromQueue when clicking on finish", () => {
    const wrapper = mount(<UpcomingApptCard profId="wolfebd@gcc.edu" studId="10g49" />);
    wrapper.setState({ appts: ["hi"] });
    let spy = jest.spyOn(wrapper.instance(), "deleteItem");
    wrapper.find(Button).simulate("click");
    expect(spy).toHaveBeenCalled();
  });

  it("StudentInLineCard should/should not render upon shouldRender", () => {
    const wrapper = shallow(
      <UpcomingApptCard profId="wolfebd@gcc.edu" studId="10g49" />
    );
    wrapper.setState({ shouldRender: false });
    expect(wrapper.type()).toEqual(null);
    wrapper.setState({ shouldRender: true });
    expect(wrapper.type()).not.toEqual(null);
  });
});
