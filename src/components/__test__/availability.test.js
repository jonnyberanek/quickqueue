/*
TESTING SUITE FOR APPTCALENDAR

*** CHANGED FROM THE MOCKING STRUCTURE TO THE SPY 3.24.19 KP
*** over 60% 3.24.19 KP
2 tests fail

TODO:
- figure out why the simulate function isnt run
- get 60%
*/
import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import Availability from "../studentAvailability.jsx";
import AvailabilityItem from "../availabilityCard.jsx";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import DatabaseService from "../../../services/DatabaseService";

import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

const dbs = new DatabaseService();

const defaultProps = {
  courses: ["COMP 141 A"],
  availability: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ]
};

const availabilityItemProps = {
  freq: 5,
  time: "0800"
};

/* spying on example:
const video = require('./video');

test('plays video', () => {
  const spy = jest.spyOn(video, 'play');
  const isPlaying = video.play();

  expect(spy).toHaveBeenCalled();
  expect(isPlaying).toBe(true);

  spy.mockRestore();
});*/

describe("Availability", () => {
  //does the Queue exist
  it("Availability should be defined", () => {
    expect(Availability).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("Availability should render correctly", () => {
    const wrapper = shallow(<Availability profId="wolfebd@gcc.edu" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new Availability({ profId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  it("test that the courseSelect function works", () => {
    const spy = jest.spyOn(DatabaseService.prototype, "getAllMembersOfCourse");
    const spy2 = jest.spyOn(DatabaseService.prototype, "getScheduleOfStudent");
    DatabaseService.getAllProfsCourses = jest
      .fn()
      .mockReturnValue(["COMP 141 A"]);
    DatabaseService.getAllMembersOfCourse = jest
      .fn()
      .mockReturnValue(["10g49"]);
    DatabaseService.getScheduleOfStudent = jest
      .fn()
      .mockReturnValue([["COMP 141 A"]]);
    const wrapper = shallow(<Availability profId="wolfebd@gcc.edu" />);
    wrapper.instance().courseSelect("ABRD 300 A");
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it("change state", () => {
    const wrapper = shallow(<Availability profId="wolfebd@gcc.edu" />);
    wrapper.setState({
      availability: {
        M: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        T: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        W: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        R: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        F: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0]
      }
    });
    expect(wrapper).toMatchSnapshot();
  });

  it("change state with courses", () => {
    const wrapper = shallow(<Availability profId="wolfebd@gcc.edu" />);
    wrapper.setState({
      courses: ["COMP 141 A"],
      availability: {
        M: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        T: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        W: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        R: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0],
        F: [0, 0, 0, 0, 0, 14, 15, 14, 12, 0, 0]
      }
    });
    expect(wrapper).toMatchSnapshot();
  });

  /*describe("when button is clicked", () => {
  it("should call onButtonClick", () => {
    const app = shallow(<App />);
    const onButtonClickSpy = jest.spyOn(app.instance(), "onButtonClick");

    # This should do the trick
    app.update();
    app.instance().forceUpdate();

    const button = app.find("button");
    button.simulate("click");
    expect(onButtonClickSpy).toHaveBeenCalled();
  });
});*/
  it("simulate the click of the course", () => {
    const wrapper = mount(<Availability profId="wolfebd@gcc.edu" />);
    //set the state so that the ListItemText is defined
    wrapper.setState({
      courses: ["COMP 141 A"]
    });
    const spy = jest.spyOn(wrapper.instance(), "courseSelect");
    wrapper.find(ListItemText).simulate("click");
    expect(spy).toHaveBeenCalled();
  });
  /* -------------- END OF Availability TESTS --------------------------   */

  /* --------------AVAILABILITY ITEM TESTS --------------------------   */
  it("check constructor", () => {
    const wrapper = new AvailabilityItem();
    expect(wrapper).toBeDefined();
  });

  it("AvailabilityItem exists on its own", () => {
    const wrapper = shallow(<AvailabilityItem />);
    expect(wrapper).toBeDefined();
  });

  /* --------------END OF AVAILABILITY ITEM TESTS -------------------   */
});
