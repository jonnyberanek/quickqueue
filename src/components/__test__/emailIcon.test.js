/*
TESTING SUITE FOR EMAILICON

*** BEGAN TESTING IN THE WEE HOURS OF THE NIGHT ON 3.22.19 KP

TODO:
- graduate and live a happy life
- get to this test to 100%
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";

import Email from "../emailIcon.jsx";

import Adapter from "enzyme-adapter-react-16";

const moment = require("moment");
const $ = require("jquery");

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("EmailIcon", () => {
  it("should be defined", () => {
    expect(Email).toBeDefined();
  });

  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    const wrapper = shallow(<Email />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    //For this test, check in the db, and make sure
    //there are appts that fall on each day of the week
    const check = new Email();
    expect(check).toBeDefined();
  });

  it("check that logout function be clicked", () => {
    let spy = "";
    const email = mount(
      <Email
        onClick={() => {
          spy = "hello";
        }}
      />
    );
    email.find(Button).simulate("click");
    expect(spy).toBe("hello");
  });
});
