/*
testing suite for RegOfficeHours & Session & AddIconForReg

*** Beginning to clean and add great tests 3.22.19 KP

4 tests fail

TODO:
- test updateSession
- get last test to pass
---- change the jquery in reg-office-hours file (line 76)
---- coordinate with Jonny to see what can be changed in Session
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import RegOfficeHours from "../reg-office-hours.jsx";
import Session from "../session.jsx";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIconForReg from "../addIconForReg.jsx";
import AddIcon from "@material-ui/icons/Add";

import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

var rand;
const dbs = new DatabaseService();

import DatabaseService from "../../../services/DatabaseService.js";
const firebase = require("firebase/app");
require("../../../services/FirebaseInit").init(firebase);

describe("RegOfficeHours", () => {
  //does the Queue exist
  it("should be defined", () => {
    expect(RegOfficeHours).toBeDefined();
  });
  //returns a Snapshot that we can look at
  it("should render correctly", () => {
    const tree = shallow(<RegOfficeHours profId="wolfebd@gcc.edu" />);
    expect(tree).toMatchSnapshot();
  });

  it("check constructor", () => {
    const check = new RegOfficeHours({ profId: "wolfebd@gcc.edu" });
    expect(check).toBeDefined();
  });

  it("check that functions exist", () => {
    const wrapper = shallow(<RegOfficeHours profId="wolfebd@gcc.edu" />);
    expect(wrapper.instance().deleteSession).toBeDefined();
    expect(wrapper.instance().makeRandomId).toBeDefined();
    expect(wrapper.instance().addSession).toBeDefined();
    expect(wrapper.instance().editSession).toBeDefined();
    expect(wrapper.instance().updateSession).toBeDefined();
  });

  it("check that addSession is functional", () => {
    var days = { asdf: ["0800", "1000", "M"] };
    const wrapper = mount(<RegOfficeHours profId="wolfebd@gcc.edu" />);
    wrapper.setState({ days: days });
    wrapper.instance().addSession("W");
    expect(Object.keys(wrapper.state().days).length).toEqual(2);
  });

  it("check that deleteSession is functional", () => {
    var days = { asdf: ["0800", "1000", "M"] };
    const wrapper = mount(<RegOfficeHours profId="wolfebd@gcc.edu" />);
    wrapper.setState({ days: days });
    wrapper.instance().deleteSession("asdf");
  });

  it("check that makeRandomId is functional", () => {
    const wrapper = mount(<RegOfficeHours profId="wolfebd@gcc.edu" />);
    rand = wrapper.instance().makeRandomId();
    expect(rand).toBeDefined();
  });

  it("check that editSession is changes the state", () => {
    const wrapper = mount(<RegOfficeHours profId="wolfebd@gcc.edu" />);
    let oldState = wrapper.state();
    wrapper.instance().editSession(rand);
    expect(oldState).not.toEqual(wrapper.state());
  });

  //need to check what updateSession does

  //---------- SESSION TESTS -----------------
  it("Session - should be defined", () => {
    expect(Session).toBeDefined();
  });
  //returns a Snapshot that we can look at
  it("Session - should render correctly", () => {
    const wrapper = shallow(<Session start="0800" end="0850" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("Session - check constructor", () => {
    const check = new Session({ start: "0800", end: "0850" });
    expect(check).toBeDefined();
  });

  it("Session - check state", () => {
    const wrapper = shallow(<Session start="0800" end="0850" />);
    expect(wrapper.state().start).toBe("0800");
    wrapper.setState({ day: "M" });
    expect(wrapper.state().day).toBe("M");
  });

  it("Session - check componentWillReceiveProps", () => {
    const wrapper = shallow(<Session start="0800" end="0850" />);
    wrapper
      .instance()
      .componentWillReceiveProps({ start: "0900", end: "0950" });
    expect(wrapper.state().start).toBe("0900");
  });

  it("Session - simulate change in a Session's textfields click", () => {
    const wrapper = shallow(<Session profId="wolfebd@gcc.edu" onEdit={() => {}} />);
    wrapper.setState({ start: "0800", end: "0900" });
    wrapper.setState = jest.fn().mockImplementation("test", done => {
      done();
    });
    wrapper
      .find(TextField)
      .at(0)
      .simulate("change", { target: { value: 24 } });
    expect(wrapper.instance().props.onEdit()).toHaveBeenCalledTimes(1);
  });

  it("Session - simulate change in a Session's textfields click", () => {
    const wrapper = shallow(<Session profId="wolfebd@gcc.edu" onEdit={() => {}} />);
    wrapper.setState({ start: "0800", end: "0900" });
    wrapper.setState = jest.fn().mockImplementation("test", done => {
      done();
    });
    wrapper
      .find(TextField)
      .at(1)
      .simulate("change", { target: { value: 24 } });
    expect(wrapper.instance().props.onEdit()).toHaveBeenCalledTimes(2);
  });

  it("Session - simluate delete click", () => {
    const wrapper = shallow(<Session profId="wolfebd@gcc.edu" onDelete={() => {}} />);
    wrapper.setState({ days: { asdf: ["0800", "1000", "M"] } });
    wrapper.update();
    wrapper.find(DeleteIcon).simulate("click");
    expect(wrapper.instance().props.onDelete()).toHaveBeenCalled();
  });

  //---------------AddIconForReg tests

  it("AddIconForReg - simluate click", () => {
    const wrapper = mount(<AddIconForReg onAdd={() => {}} />);
    wrapper.find(AddIcon).simulate("click");
    expect(wrapper.instance().props.onAdd()).toHaveBeenCalled();
  });
});
