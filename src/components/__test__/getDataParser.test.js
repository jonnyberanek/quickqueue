/*
--- DONE ---

testing suite for GetDataParser

*** Created 3.25.29

all tests pass! hooray!
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";

import parsingUtils from "../../../services/GetDataParser.js";

describe("GetDataParser", () => {
  //-----------Test GetDataParser ----------------

  it("check parseUrlGetData", () => {
    const check = parsingUtils.parseUrlGetData(
      "www.hello.com/?a=100&b=45&c#resopnsibiilties"
    );
    expect(check).toEqual({ a: ["100"], b: ["45"], c: [null] });
    const check2 = parsingUtils.parseUrlGetData("www.hello.com/?a=30&a=100");
    expect(check2).toEqual({ a: ["30", "100"] });
  });
});
