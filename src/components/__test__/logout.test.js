/*
TESTING SUITE FOR LOGINMODULE

***a very poor implementation of the spy but its 1 a.m. and I'm tired
*** doing my best though :) 3.22.19 KP

1 TEST FAILS BUT AT 100%

TODO:
- get last test to pass
*/

import React from "react";
import Enzyme, { shallow, render, mount } from "enzyme";
import LogoutButton from "../logoutButton.jsx";
import Button from "@material-ui/core/Button";

import Adapter from "enzyme-adapter-react-16";

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

Enzyme.configure({ adapter: new Adapter() });

describe("LogoutButton", () => {
  it("should be defined", () => {
    expect(LogoutButton).toBeDefined();
  });

  it("should render correctly", () => {
    const wrapper = shallow(<LogoutButton />);
    expect(wrapper).toMatchSnapshot();
  });

  it("check constructor", () => {
    var check = new LogoutButton();
    expect(check).toBeDefined();
  });

  it("check that logout function be clicked", () => {
    const spy = jest.spyOn(firebase.auth(), "signOut");
    const button = mount(<LogoutButton />);
    button.find(Button).simulate("click");
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it("check that logout function exists", () => {
    const wrapper = shallow(<LogoutButton />);
    expect(wrapper.instance().logout).toBeDefined();
  });

  it("check that logout function works", () => {
    //maybe spy on the firebase.auth().signOut() call?
    const button = mount(<LogoutButton />);
    firebase.auth().signOut = jest.fn().mockResolvedValue(true);
    button.instance().logout();
    expect(firebase.auth().signOut).toHaveBeenCalledTimes(1);
    //expect to get to login window
  });
});
