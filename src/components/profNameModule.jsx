import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import DatabaseService from "./../../services/DatabaseService";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

const dbs = new DatabaseService();

class ProfNameModule extends Component {
  state = {};

  goToProfProfile = () => {
    var profName = this.props.name;
    dbs.getProfIdUsingName(profName, function(id) {
      window.location.href =
        "/student/professor-office-hours?professorId=" + id;
    });
  };

  render() {
    return (
      <ListItem>
        <Button>
          <ListItemText
            primary={this.props.name}
            secondary={this.props.inOffice}
            onClick={() => this.goToProfProfile()}
            style={{
              paddingLeft: "5px",
              paddingRight: "5px",
              textAlign: "center",
              textTransform: "capitalize"
            }}
          />
        </Button>
      </ListItem>
    );
  }
}

export default ProfNameModule;
