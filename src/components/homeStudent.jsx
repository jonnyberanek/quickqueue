import React, { Component } from "react";
import ProfNameModule from "./profNameModule.jsx";

import Typography from "@material-ui/core/Typography";

import { withTheme } from "@material-ui/core/styles";

import DatabaseService from "./../../services/DatabaseService";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

const dbs = new DatabaseService();

var moment = require("moment");

class StudentHome extends Component {
  state = { name: [], inOffice: [] };

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    var scope = this;
    var nameArray = [];
    var inOfficeArray = [];
    props.student.favorites.forEach(profRef => {
      dbs.getProfUsingID(profRef.id, name => {
        nameArray.push(name);
        this.setState({ name: nameArray });
      });

      dbs.getCurrentSession(profRef.id, target => {
        if (target) {
          inOfficeArray.push("In Office");
          this.setState({ inOffice: inOfficeArray });
        } else {
          inOfficeArray.push("Out of Office");
          this.setState({ inOffice: inOfficeArray });
        }
      });
    });
  }

  chooseProf(event) {}

  render() {
    var that = this;
    var children = [];
    children.push(
      <Typography variant="h5" id="favTitle" color="primary">
        Favorites
      </Typography>
    );
    if (that.state.name.length) {
      for (var prof in that.state.name) {
        children.push(
          <span>
            <ProfNameModule
              name={this.state.name[prof]}
              inOffice={this.state.inOffice[prof]}
            />
          </span>
        );
      }
    } else {
      children.push(
        <Typography
          variant="h6"
          style={{
            marginTop: "24px",
            marginBottom: "48px",
            color: "darkgrey",
            fontSize: "90%"
          }}
        >
          No favorites. Star a professor to favorite.
        </Typography>
      );
    }

    return (
      <span
        children={children}
        style={{
          margin: "16px",
          padding: "16px",
          paddingBottom: "8px",
          borderRadius: "5px",
          align: "center"
        }}
      />
    );
  }
}

export default withTheme()(StudentHome);
