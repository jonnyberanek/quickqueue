import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

class AvailabilityItem extends Component {
  state = {};

  render() {
    return (
      <ListItem>
        <ListItemText
          primary={this.props.freq}
          secondary={"at " + this.props.time}
        />
      </ListItem>
    );
  }
}

export default AvailabilityItem;
