import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Chip from "@material-ui/core/Chip";
import Typography from "@material-ui/core/Typography";
import "../style/appointments.css";

import SimpleSnackbar from "./SimpleSnackbar.jsx";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

import DatabaseService from "../../services/DatabaseService";
import formatDayCharacter from "../../services/FormatDayCharacter";

const dbs = new DatabaseService();
require("../style/makeAppt.css");

const moment = require("moment");

class TopicList extends Component {
  state = { topics: [] };

  constructor(props) {
    super(props);
    let that = this;
    firebase
      .firestore()
      .collection("queue")
      .doc(that.props.profId)
      .collection("visits")
      .onSnapshot(query => {
        this.updateScreen();
      });
  }

  updateScreen() {
    let that = this;
    firebase
      .firestore()
      .collection("queue")
      .doc(that.props.profId)
      .collection("visits")
      .get()
      .then(querySnapshot => {
        var topics = [],
          topicsStr = "";
        querySnapshot.forEach(doc => {
          var include = true;
          for (var i = 0; i < topics.length; i++) {
            if (topics[i] === doc.data().topic) {
              include = false;
            }
          }
          if (include) {
            topics.push(doc.data().topic);
          }
        });
        for (var i = 0; i < topics.length; i++) {
          if (i === topics.length - 1) {
            topicsStr += topics[i];
          } else {
            topicsStr += topics[i] + "  |  ";
          }
        }
        that.setState({ topics: topicsStr });
      });
  }
  render() {
    if (this.state.topics == "") {
      return <span />;
    } else {
      return (
        <span>
          <Typography variant="subtitle2" style={{ marginTop: "30px" }}>
            Students in line are asking about:
          </Typography>
          <Typography variant="subtitle1">
            if you are asking about one of these topics you may be able to jump
            in with someone ahead of you
          </Typography>
          <Typography variant="h6"> {this.state.topics}</Typography>
          <br />
        </span>
      );
    }
  }
}
export default TopicList;
