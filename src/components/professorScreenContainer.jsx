import React, { Component } from "react";

import ProfessorNavBar from "./professorNavBar.jsx";

import ProfessorAuthService from "../../services/ProfessorAuthService";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

class ProfessorScreenContainer extends Component {
  state = {};

  constructor(props) {
    super(props);
    this.state.user = null;
    const authService = new ProfessorAuthService(firebase);
    authService.onValidUserLoad(user => {
      this.setState({ user: user });
      this.render();
    });
  }

  // if this makes no sense see: https://reactjs.org/docs/render-props.html
  render() {
    return (
      <React.Fragment>
        <ProfessorNavBar />
        {this.props.render && this.state.user && this.props.render(this.state)}
      </React.Fragment>
    );
  }
}

export default ProfessorScreenContainer;
