import React, { Component } from "react";
import Button from "@material-ui/core/Button";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

class LogoutButton extends Component {
  state = {};

  logout = () => {
    firebase.auth().signOut();
    window.location.href = "/login";
  };

  render() {
    return (
      <Button
        id="logoutButton"
        onClick={() => this.logout()}
        variant="contained"
        color="primary"
      >
        Log out
      </Button>
    );
  }
}

export default LogoutButton;
