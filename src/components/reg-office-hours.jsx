import React, { Component } from "react";

import LogoutButton from "./logoutButton.jsx";
import Session from "./session.jsx";
import AddIconForReg from "./addIconForReg.jsx";
import SimpleSnackbar from "./SimpleSnackbar.jsx";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography";
import * as Math from "mathjs";

import { withTheme } from "@material-ui/core/styles";

import formatDayCharacter from "../../services/FormatDayCharacter";
import DatabaseService from "../../services/DatabaseService.js";

import "./../style/reg-office-hours.css";

const firebase = require("firebase");
require("./../../services/FirebaseInit").init(firebase);

const moment = require("moment");
const $ = require("jquery");

class RegOfficeHours extends Component {
  state = {
    numConflicts: 0,
    days: {},
    openUpdatedSnackbar: false,
    edited: false,
    apptsToDelete: {}
  };

  additions = {};
  deletions = {};
  appointments = {};

  constructor(props) {
    super(props);
    ("");

    this.dbs = new DatabaseService();
    firebase
      .firestore()
      .collection("professors")
      .doc(this.props.profId)
      .collection("sessions")
      .get()
      .then(querySnapshot => {
        var days = {};
        //foreach duration in the deletion table
        querySnapshot.forEach(doc => {
          days[doc.id] = doc.data();
        });
        this.setState({ days: days });
      });

    // for the two functions below to not duplicate code
    const addToCachePartArray = (arrayRef, dateSnapshots) => {
      for (const date in dateSnapshots) {
        const dayChar = formatDayCharacter(moment(date));
        arrayRef[dayChar] = {};
        dateSnapshots[date].docs.forEach(doc => {
          arrayRef[dayChar][doc.id] = doc.data();
        });
      }
    };

    this.dbs
      .getAdditionsForWeekWithDate(props.profId, moment())
      .then(dateSnapshots => {
        addToCachePartArray(this.additions, dateSnapshots);
      });
    this.dbs
      .getDeletionsForWeekWithDate(props.profId, moment())
      .then(dateSnapshots => {
        addToCachePartArray(this.deletions, dateSnapshots);
      });
    this.dbs
      .getProfAppointmentsForWeek(props.profId, formatDayCharacter(moment()))
      .then(daySnapshots => {
        for (const day in daySnapshots) {
          if (!daySnapshots[day].empty && !this.appointments[day])
            this.appointments[day] = {};
          daySnapshots[day].docs.forEach(doc => {
            this.appointments[day][doc.id] = doc.data();
          });
        }
        this.checkAppointments("F");
      });
  }

  //delete a session from the state
  deleteSession = id => {
    this.setState({ edited: true });
    var days = this.state.days;
    const dayChar = days[id].day;
    delete days[id];
    this.setState({ days }, () => {
      this.checkConflicts();
      this.checkAppointments(dayChar);
    });
  };

  makeRandomId = () => {
    var text = "";
    var possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 15; i++)
      text += possible.charAt(Math.random() * possible.length);
    return text;
  };

  //add a session to the table
  addSession = day => {
    this.setState({ edited: true });
    var days = this.state.days;
    var id = this.makeRandomId();
    days[id] = {
      start: "1200",
      end: "1300",
      day: day,
      edited: true
    };
    this.setState({ days }, () => {
      this.checkConflicts();
      this.checkAppointments(day);
    });
  };

  editSession = (id, session) => {
    this.setState({ edited: true });
    var days = { ...this.state.days };
    days[id].start = moment(session.state.start, "HH:mm").format("HHmm");
    days[id].end = moment(session.state.end, "HH:mm").format("HHmm");
    days[id].day = days[id].day;
    days[id].edited = true;
    this.setState({ days }, () => {
      this.checkConflicts();
      this.checkAppointments(days[id].day);
    });
  };

  updateSession = () => {
    // stop them from saving again unless they make more changes
    this.setState({ edited: false });

    firebase
      .firestore()
      .collection("professors")
      .doc(this.props.profId)
      .collection("sessions")
      .get()
      .then(querySnapshot => {
        return Promise.all(
          querySnapshot.docs.map(doc =>
            firebase
              .firestore()
              .collection("professors")
              .doc(this.props.profId)
              .collection("sessions")
              .doc(doc.id)
              .delete()
          )
        );
      })
      .then(() => {
        var promises = [];
        for (var day in this.state.days) {
          promises.push(
            firebase
              .firestore()
              .collection("professors")
              .doc(this.props.profId)
              .collection("sessions")
              .doc()
              .set({
                start: this.state.days[day].start,
                end: this.state.days[day].end,
                day: this.state.days[day].day
              })
          );
        }
        return Promise.all(promises);
      })
      .then(() => {
        return this.dbs.updateAppointments(
          this.props.profId,
          this.state.apptsToDelete
        );
      });
  };

  resetProgress = () => {
    firebase
      .firestore()
      .collection("professors")
      .doc(this.props.profId)
      .collection("sessions")
      .get()
      .then(querySnapshot => {
        var days = {};
        //foreach duration in the deletion table
        querySnapshot.forEach(doc => {
          days[doc.id] = doc.data();
        });
        this.setState({ days: days }, () => {
          this.checkConflicts();
          ["M", "T", "W", "R", "F"].forEach(d => this.checkAppointments(d));
        });
      });
    this.setState({ edited: false });
  };

  receiveSessionErrorState = (id, internalError) => {
    const newDays = { ...this.state.days };
    newDays[id].invalid = 0;
    for (var key in internalError) {
      if (internalError[key] === true) {
        newDays[id].invalid++;
      }
    }
    this.setState({ days: newDays });
  };

  createCacheForDay(char) {
    var cache = [];

    // populate with current edit of recurring sessions which are not deleted
    const days = this.state.days;
    for (const id in days) {
      if (days[id].day === char) {
        cache.push(days[id]);
      }
    }

    return cache;
  }

  checkAppointments = editedDayChar => {
    //no need to check for appointments for day if none exist or has a cache
    if (
      !this.appointments[editedDayChar] ||
      Object.values(this.additions[editedDayChar]).length
    ) {
      return;
    }
    const cache = this.createCacheForDay(editedDayChar);

    // start with all appts being assumed to be deleted

    var apptsOfDay = {
      ...this.appointments[editedDayChar]
    };

    // if there is a session which contains the appointment, remove it from the 'to be deleted' list
    const toGoodDateFormat = timestamp =>
      moment(timestamp.toDate()).format("HHmm");
    for (const id in apptsOfDay) {
      // reformat inconsistencies in data field and types *upside down smiley emoji*
      const { start_time, end_time } = apptsOfDay[id];
      const appt = {
        start: toGoodDateFormat(start_time),
        end: toGoodDateFormat(end_time)
      };

      var isWithSession = cache.some(s =>
        this.doesSessionContainAppointment(s, appt)
      );
      if (isWithSession) {
        delete apptsOfDay[id];
      }
    }
    console.log(this.state.apptsToDelete);
    this.state.apptsToDelete[editedDayChar] = apptsOfDay;
    this.setState({ apptsToDelete: this.state.apptsToDelete });
  };

  checkConflicts() {
    const newDays = { ...this.state.days };
    //reset conflicts
    var numConflicts = 0;
    for (var id in newDays) {
      newDays[id].error = false;
    }
    const keys = Object.keys(newDays);
    for (var i = 0; i < keys.length; i++) {
      for (var j = i + 1; j < keys.length; j++) {
        if (this.doSessionsOverlap(newDays[keys[i]], newDays[keys[j]])) {
          numConflicts++;
          newDays[keys[i]].error = true;
          newDays[keys[j]].error = true;
        }
      }
    }
    this.setState({ days: newDays, numConflicts: numConflicts });
  }

  doSessionsOverlap(session1, session2) {
    return (
      session1.day === session2.day &&
      ((session1.end > session2.start && session1.start < session2.end) ||
        (session2.end > session1.start && session2.start < session1.end))
    );
  }

  doesSessionContainAppointment(session, appt) {
    return session.start <= appt.start && session.end >= appt.end;
  }

  formatTimestamp(timestamp) {
    return moment(timestamp.toDate()).format("HH:mm");
  }

  render() {
    var children = [];
    var daysToDisplay = [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday"
    ];
    var daysOfTheWeek = ["M", "T", "W", "R", "F"];
    var i = 0;
    for (var d in daysOfTheWeek) {
      var daySaved = daysOfTheWeek[d];
      var content = [];
      content.push(
        <Typography variant="h5" id="day">
          {daysToDisplay[d]}
          <AddIconForReg day={daysOfTheWeek[d]} onAdd={this.addSession} />
        </Typography>
      );
      for (var day in this.state.days) {
        if (this.state.days[day].day == daySaved) {
          content.push(
            <Session
              className="session"
              id={day}
              start={
                this.state.days[day].start.charAt(0) +
                this.state.days[day].start.charAt(1) +
                ":" +
                this.state.days[day].start.charAt(2) +
                this.state.days[day].start.charAt(3)
              }
              end={
                this.state.days[day].end.charAt(0) +
                this.state.days[day].end.charAt(1) +
                ":" +
                this.state.days[day].end.charAt(2) +
                this.state.days[day].end.charAt(3)
              }
              day={this.state.days[day].day}
              onDelete={this.deleteSession}
              onEdit={this.editSession}
              onUpdateErrorState={this.receiveSessionErrorState}
              error={this.state.days[day].error}
              edited={this.state.days[day].edited}
            />
          );
        }
      }
      children.push(
        <div
          style={
            i++ % 2 == 1
              ? { backgroundColor: this.props.theme.palette.primary[50] }
              : null
          }
          className="recurring-day-item"
        >
          {content}
          {content.length == 1 ? (
            <Typography
              variant="caption"
              style={{ padding: "24px", textAlign: "center" }}
            >
              {`You don't have any recurring sessions for ${daysToDisplay[d]}
              yet.`}
            </Typography>
          ) : null}
        </div>
      );
    }

    // generage conflicts info
    const dayValues = Object.values(this.state.days);
    const { numConflicts } = this.state;
    var numInvalids = 0;
    dayValues.forEach(x => {
      if (x.invalid) numInvalids += x.invalid;
    });

    // generate deleted appts info
    const toUsableTimeFormat = timestamp =>
      moment(timestamp.toDate()).format("hh:mm a");
    var apptsToDelete = Object.values(this.state.apptsToDelete).flatMap(
      dayAppts =>
        Object.values(dayAppts).map(appt => (
          <li>
            <Typography variant="body1">{`${moment(
              appt.start_time.toDate()
            ).format("dddd")}: ${toUsableTimeFormat(
              appt.start_time
            )} - ${toUsableTimeFormat(appt.end_time)}`}</Typography>
          </li>
        ))
    );

    return (
      <span className="RegOfficeHours">
        <span>{children}</span>
        <div id="buttons">
          <Typography variant="caption" style={{ margin: "4px" }}>
            <b>Note:</b> Dates edited in {"'Modify Occurrences'"} will not be
            updated by any changes made above.
          </Typography>
          <Button
            style={this.props.styling}
            onClick={() => this.updateSession()}
            variant="contained"
            disabled={!this.state.edited}
            color="primary"
          >
            Confirm
          </Button>
          <Button
            style={this.props.styling}
            onClick={() => this.resetProgress()}
          >
            Reset
          </Button>
          {/* Conditionally adds message if there are errors. */}
          {numInvalids || numConflicts ? (
            <Typography variant="body1" color="error" inline align="right">
              Fix your{" "}
              {numInvalids
                ? `${numInvalids} invalid time${numInvalids > 1 ? "s" : ""}`
                : ""}
              {numInvalids && numConflicts ? " and " : null}
              {numConflicts
                ? `${numConflicts} conflict${numConflicts > 1 ? "s" : ""}`
                : ""}
              .
            </Typography>
          ) : null}
          {apptsToDelete.length ? (
            <div style={{ margin: "4px" }}>
              <Typography
                variant="subtitle2"
                style={{ fontSize: "110%", marginTop: "5px" }}
              >
                The current schedule excludes the following appointments:
              </Typography>
              <ul>{apptsToDelete}</ul>
              <Typography variant="subtitle1">
                <b>Note:</b> They will be deleted if the schedule is not
                modified
              </Typography>
            </div>
          ) : null}
        </div>
        <SimpleSnackbar
          open={this.state.openUpdatedSnackbar}
          important
          msg="Sessions updated!"
        />
      </span>
    );
  }
}

export default withTheme()(RegOfficeHours);
