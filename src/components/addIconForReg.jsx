import React, { Component } from "react";
import AddIcon from "@material-ui/icons/Add";

class AddIconForReg extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <span>
        <AddIcon
          className="styleIcon"
          onClick={() => this.props.onAdd(this.props.day)}
        />
      </span>
    );
  }
}
export default AddIconForReg;
