import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

var moment = require("moment");

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

class AppointmentItem extends Component {
  state = {};

  getStyle = () => {
    var style = {
      padding: "10px",
      margin: "10px"
    };

    return style;
  };
  render() {
    var start = moment(String(this.props.start)).format("MMM D, h:mm a");
    var end = moment(String(this.props.end)).format(" h:mm a");

    return (
      <ListItem style={this.getStyle()}>
        <ListItemText
          primary={this.props.name}
          secondary={start + " - " + end + "  |  " + this.props.topic}
        />
        <Button
          variant="contained"
          color="primary"
          className="delete-button"
          onClick={() => this.props.onDelete(this.props.id)}
        >
          Delete{" "}
        </Button>
      </ListItem>
    );
  }
}

export default AppointmentItem;
