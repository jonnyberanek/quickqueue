import React from "react";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

/*
 * How to use:
 * - paste into parent component
 * - set open as a boolean in the parent's state
 * - set said boolean to true when the toast is needed
 * - set other properties as necessary
 *
 *  (look at makeAppointment.jsx and reg-office-hours.jsx for examples)
 *
 * ---- Properties: ----
 * open (REQUIRED): defines whether it's open or note
 * msg (REQUIRED): the message which will display
 * important: wont go away when screen is clicked
 * persist: doesn't timeout
 * actionText: text for action button (requried for it to appear) (ex. undo)
 * onAction: triggered when action is clicked
 * onClose: something that happens when closes
 * anchorOrigin: where the snackbar displays on the screen
 *
 */

class SimpleSnackbar extends React.Component {
  state = {
    open: false
  };

  constructor(props) {
    super(props);
    console.log(props);
    this.state.open = this.props.open;
  }

  componentWillReceiveProps(props) {
    this.setState({ open: props.open });
  }

  handleClick = () => {
    this.setState({ open: true });
    this.props.onClick && this.props.onClick(this);
  };

  handleClose = (event, reason) => {
    if (this.props.important && reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
    this.props.onClose && this.props.onClose(this);
  };

  handleAction = () => {
    this.props.onAction && this.props.onAction();
  };

  render() {
    const { anchorOrigin, persist, msg, actionText } = this.props;
    var actions = [];
    if (actionText) {
      actions.push(
        <Button
          key="undo"
          color="secondary"
          size="small"
          onClick={this.handleAction}
        >
          {actionText}
        </Button>
      );
    }
    actions.push(
      <IconButton
        key="close"
        aria-label="Close"
        color="inherit"
        onClick={this.handleClose}
      >
        <CloseIcon />
      </IconButton>
    );
    return (
      <div>
        <Snackbar
          anchorOrigin={
            anchorOrigin || {
              vertical: "top",
              horizontal: "center"
            }
          }
          open={this.state.open}
          autoHideDuration={persist ? null : 6000}
          onClose={this.handleClose}
          message={<span id="message-id">{msg}</span>}
          action={actions}
        />
      </div>
    );
  }
}

export default SimpleSnackbar;
