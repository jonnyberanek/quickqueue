import React, { Component } from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import DatabaseService from "../../services/DatabaseService.js";
import Typography from "@material-ui/core/Typography";

var moment = require("moment");
const firebase = require("firebase");
require("./../../services/FirebaseInit").init(firebase);

import "./../style/apptCalendar.css";

class ApptCalendar extends Component {
  state = {
    appts: [[], [], [], [], []],
    week: moment().day(1),
    shouldRender: false
  };

  dbs;

  constructor(props) {
    super(props);

    this.dbs = new DatabaseService();
    //check to see if the prof uses appt
    this.dbs.doesProfUseAppts(this.props.profId, appts => {
      if (appts) {
        //grabbing all appts
        this.updateAppts();
      }
    });
  }

  updateAppts() {
    var appointments = [[], [], [], [], []];
    this.dbs.getProfAppointments(this.props.profId, appts => {
      for (var appt in appts) {
        console.log(appts[appt]);
        if (
          moment(appts[appt][1]).dayOfYear() <
            this.state.week.dayOfYear() + 5 &&
          moment(appts[appt][1]).dayOfYear() >= this.state.week.dayOfYear()
        ) {
          switch (moment(appts[appt][1]).day()) {
            case 1:
              appointments[0].push(appts[appt]);
              break;
            case 2:
              appointments[1].push(appts[appt]);
              break;
            case 3:
              appointments[2].push(appts[appt]);
              break;
            case 4:
              appointments[3].push(appts[appt]);
              break;
            case 5:
              appointments[4].push(appts[appt]);
              break;
          }
        }
      }
      this.setState({ appts: appointments, shouldRender: true });
    });
  }

  render() {
    var children = [];
    var childEachDay = [];
    if (this.state.shouldRender) {
      for (var day in this.state.appts) {
        childEachDay = [];
        childEachDay.push(
          <Typography variant="h5">
            {moment()
              .day(parseInt(day) + 1)
              .format("dddd")}
          </Typography>
        );
        if (!Object.keys(this.state.appts[day]).length) {
          childEachDay.push(<ListItemText secondary="None" />);
        }
        for (var appt in this.state.appts[day]) {
          if (this.state.appts[day][appt].length != 0) {
            childEachDay.push(
              <ListItemText
                primary={this.state.appts[day][appt][4]}
                secondary={moment(
                  String(this.state.appts[day][appt][1])
                ).format("h:mm a")}
              />
            );
          }
        }
        children.push(<span id="eachDay">{childEachDay}</span>);
      }
      return (
        <span>
          <Typography variant="h4">
            Week of {this.state.week.format("MMMM D, YYYY")}
          </Typography>
          <span children={children} />
        </span>
      );
    } else {
      return (
        <span>
          <Typography variant="h5"> Appointments not enabled.</Typography>
        </span>
      );
    }
  }
}

export default ApptCalendar;
