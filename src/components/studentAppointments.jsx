import React, { Component } from "react";
import AppointmentItem from "./appointmentItem.jsx";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

import DatabaseService from "./../../services/DatabaseService";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

class StudentAppointments extends Component {
  state = {
    items: [],
    shouldRender: false
  };

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    var scope = this;
    firebase
      .firestore()
      .collection("professors")
      .doc(this.props.profId)
      .onSnapshot(querySnapshot => {
        this.updateScreen();
      });
    firebase
      .firestore()
      .collection("professors")
      .doc(this.props.profId)
      .collection("appointments")
      .onSnapshot(querySnapshot => {
        this.updateScreen();
      });
  }

  updateScreen() {
    let finalized = [];
    var scope = this;
    this.dbs.doesProfUseAppts(this.props.profId, shouldRenderAppts => {
      if (shouldRenderAppts) {
        scope.dbs.getProfAppointmentsOfWeek(this.props.profId, appts => {
          if (appts.length - 1) {
            finalized = [];
            //start at 1 to skip the first appointment
            for (var i = 1; i < appts.length; i++) {
              finalized.push({
                id: appts[i][0],
                start_time: appts[i][1],
                topic: appts[i][2],
                end_time: appts[i][3],
                name: appts[i][4]
              });
            }
            scope.setState({ items: finalized, shouldRender: true });
          } else {
            scope.setState({ items: [], shouldRender: true });
          }
        });
      } else {
        scope.setState({ shouldRender: false });
      }
    });
  }

  deleteItem = id => {
    var that = this;
    var finalized = this.state.items;
    this.dbs.deleteStudentAppointment(that.props.profId, id);
    for (var i = 0; i < that.state.items.length; i++) {
      if (that.state.items[i].id === id) {
        //remove 1 item at index i
        finalized.splice(i, 1);
      }
    }
    that.setState({ items: finalized });
  };

  render() {
    if (this.state.shouldRender) {
      if (this.state.items.length == 0) {
        return (
          <span>
            <Typography
              variant="h6"
              style={{
                marginBottom: "48px",
                color: "darkgrey",
                fontSize: "110%"
              }}
            >
              No appointments
            </Typography>
            <br />
          </span>
        );
      } else {
        return (
          <List
            style={{
              maxWidth: "550px",
              minWidth: "450px"
            }}
          >
            {this.state.items.map(item => (
              <React.Fragment>
                <AppointmentItem
                  id={item.id}
                  start={item.start_time}
                  topic={item.topic}
                  end={item.end_time}
                  name={item.name}
                  onDelete={this.deleteItem}
                />
              </React.Fragment>
            ))}
          </List>
        );
      }
    } else {
      return (
        <Typography
          variant="h6"
          style={{
            marginBottom: "48px",
            color: "darkgrey",
            fontSize: "110%"
          }}
        >
          Appointments not enabled.
        </Typography>
      );
    }
  }
}

export default StudentAppointments;
