import React, { Component } from "react";

import LogoutButton from "./StudentLogoutButton.jsx";
import TopicButton from "./topic-course-button.jsx";
import CourseButton from "./course-button.jsx";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Typography from "@material-ui/core/Typography";

import SimpleSnackbar from "./simpleSnackbar.jsx";

import DatabaseService from "../../services/DatabaseService.js";
var dbs = new DatabaseService();

import parsingUtils from "../../services/GetDataParser";
var getData = parsingUtils.parseUrlGetData(window.location.href);

const firebase = require("firebase");
require("./../../services/FirebaseInit").init(firebase);

class TopicCourse extends Component {
  state = {
    topics: [],
    courses: [],
    selected: ["", "None"],
    snackbarIsOpen: false,
    snackbarMessage: ""
  };

  constructor(props) {
    super(props);

    var topics = [];

    // the 'then' itself is a promise, don't need the outer promise code
    // grabbing the student from student collection
    dbs.getTopicsFromHistoricalVisits(getData.professorId[0], suggestions => {
      for (let j = 0; j < suggestions.length; j++) {
        let include = true;
        for (let i = 0; i < topics.length; i++) {
          if (topics[i].toLowerCase() === suggestions[j].toLowerCase()) {
            include = false;
          }
        }
        if (include) {
          topics.push(suggestions[j]);
        }
      }
      this.setState({ topics: topics });
    });

    // grabbing the courses that a prof teaches
    dbs.getAllProfsCourses(getData.professorId[0], coursesArray => {
      this.setState({
        courses: coursesArray
      });
    });
  }

  addToQueue = studId => {
    dbs.addStudentToQueue(
      this.props.studId,
      this.state.selected[0],
      this.state.selected[1],
      getData.professorId[0],
      promise => {
        promise.then(() => {
          this.setState({
            snackbarIsOpen: true,
            snackbarMessage: "Got in line! Redirecting..."
          });
          setTimeout(() => (window.location.href = "../home"), 2000);
        });
      }
    );
  };

  topicSelected = topic => {
    var selected = this.state.selected;
    //if the topic is already selected
    if (selected[0] == topic) {
      selected[0] = "";
    } else {
      selected[0] = topic;
    }
    this.setState({ selected: selected });
  };

  handleChange = event => {
    event.persist();
    let change = event.target.value;
    var selected = this.state.selected;
    if (change == "None") {
      selected[1] = "None";
    } else {
      selected[1] = change;
    }
    console.log(selected);
    this.setState({ selected: selected });
  };

  topicTextChange = evt => {
    var selected = this.state.selected;
    selected[0] = evt.target.value;
    this.setState({ selected: selected });
  };

  render() {
    console.log(this.state);
    let storeCourses = [],
      children = [];
    storeCourses.push(
      <FormControlLabel
        id={"None"}
        value={"None"}
        control={<Radio />}
        label={"None"}
      />
    );
    for (var course in this.state.courses) {
      storeCourses.push(
        <FormControlLabel
          id={this.state.courses[course]}
          value={this.state.courses[course]}
          control={<Radio />}
          label={this.state.courses[course]}
        />
      );
    }
    children.push(
      <span>
        <FormControl id="pickCourse" component="fieldset">
          <FormLabel component="legend">
            Select the course that you are in, if any
          </FormLabel>
          <RadioGroup id="pickCourse" onChange={this.handleChange}>
            {storeCourses}
          </RadioGroup>
        </FormControl>
      </span>
    );

    return (
      <React.Fragment>
        <span children={children} style={{ marginBottom: "48px" }} />
        <Typography variant="subtitle1">
          Enter topic or select one below
        </Typography>
        <TextField
          id="student_topic"
          className="mdc-text-field__input"
          margin="normal"
          value={this.state.selected[0]}
          onChange={this.topicTextChange}
          inputProps={{ maxLength: 20 }}
        />
        <span style={{ marginBottom: "24px" }}>
          {this.state.topics.map(item => (
            <TopicButton
              key={item.id}
              name={item}
              onTopicSelect={this.topicSelected}
              selected={this.state.selected[0] == item}
            />
          ))}
        </span>
        <Button onClick={this.addToQueue}>confirm</Button>
        <SimpleSnackbar
          open={this.state.snackbarIsOpen}
          important
          msg={this.state.snackbarMessage}
        />
      </React.Fragment>
    );
  }
}

export default TopicCourse;
