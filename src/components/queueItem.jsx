import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

class QueueItem extends Component {
  state = {};

  getStyle = () => {
    var style = {
      padding: "10px",
      margin: "10px"
    };
    if (this.props.item.isFirst) {
      style.fontWeight = "bold";
    }
    return style;
  };

  render() {
    //need to add doc id somewhere - wait do I?

    // makes pushabck only if it's first
    const pushbackButton = this.props.item.isFirst && (
      <Button
        color="primary"
        className="push-button m-2"
        onClick={() => this.props.onPushback()}
      >
        Push Back
      </Button>
    );

    return (
      <ListItem style={this.getStyle()}>
        <ListItemText
          primary={this.props.item.studentName}
          secondary={this.props.item.courseId + " | " + this.props.item.topic}
        />
        <ListItemSecondaryAction>
          {pushbackButton}
          <Button
            variant="contained"
            color="primary"
            style={{
              marginLeft: "10px"
            }}
            className="delete-button"
            onClick={() =>
              this.props.onDelete(this.props.item.studentId, this.props.item.id)
            }
          >
            Delete
          </Button>
        </ListItemSecondaryAction>
      </ListItem>
    );
  }
}

export default QueueItem;
