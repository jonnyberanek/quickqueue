import React, { Component } from "react";

import "../style/session.css";

import DatabaseService from "../../services/DatabaseService.js";
const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import TextField from "@material-ui/core/TextField";
import "../theme/reg-office-hours.css";

const moment = require("moment");

class Session extends Component {
  state = {
    edited: false,
    internalError: {
      start: false,
      end: false
    }
  };
  dbs = new DatabaseService();
  constructor(props) {
    super(props);
    this.init(props);
  }

  componentWillReceiveProps(newProps) {
    this.init(newProps);
  }

  init = props => {
    this.state.start = props.start;
    this.state.end = props.end;
    this.state.error = props.error;
    this.state.edited = props.edited;
  };

  createClasses = () => {
    var classes = "Session";
    if (this.state.error) {
      classes += " Session-error";
    } else if (this.state.edited) {
      classes += " Session-edited";
    }
    return classes;
  };

  render() {
    return (
      <span className={this.createClasses()}>
        <form>
          <TextField
            id={"startTime" + this.props.id}
            label="Start"
            type="time"
            inputProps={{
              step: 300, // 5 min
              value: this.state.start
            }}
            error={this.state.internalError.start}
            onChange={event => {
              var newState = { ...this.state };
              newState.start = event.target.value;
              newState.internalError.start = newState.start === "";
              if (newState.start >= this.state.end) {
                newState.end = moment(newState.start, "HH:mm")
                  .add(1, "h")
                  .format("HH:mm");
              }
              newState.edited = true;
              this.setState(newState, () => {
                this.props.onEdit(this.props.id, this);
                this.props.onUpdateErrorState &&
                  this.props.onUpdateErrorState(
                    this.props.id,
                    newState.internalError
                  );
              });
            }}
          />
        </form>
        <form>
          <TextField
            id={"endTime" + this.props.id}
            label="End"
            type="time"
            inputProps={{
              step: 300, // 5 min
              value: this.props.end
            }}
            error={this.state.internalError.end}
            onChange={event => {
              var newState = { ...this.state };
              newState.end = event.target.value;
              newState.internalError.end = newState.end === "";
              if (newState.end <= this.state.start) {
                newState.start = moment(newState.start, "HH:mm")
                  .subtract(1, "h")
                  .format("HH:mm");
              }
              newState.edited = true;
              this.setState(newState, () => {
                this.props.onEdit(this.props.id, this);
                this.props.onUpdateErrorState &&
                  this.props.onUpdateErrorState(
                    this.props.id,
                    newState.internalError
                  );
              });
            }}
          />
        </form>
        <DeleteIcon
          className="styleIcon"
          onClick={() =>
            this.props.onDelete(this.props.id, this.props.sessionId)
          }
        />
      </span>
    );
  }
}

export default Session;
