import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

import Session from "./session.jsx";
import SimpleSnackbar from "./SimpleSnackbar.jsx";

import "../style/setWeeklyHoursModule.css";

import MomentUtils from "@date-io/moment";
import {
  MuiPickersUtilsProvider,
  TimePicker,
  Calendar
} from "material-ui-pickers";

import DatabaseService from "../../services/DatabaseService";
import EmailJsService from "../../services/EmailJsService";
import formatDayCharacter from "../../services/FormatDayCharacter";

const moment = require("moment");
const uuid = require("uuid/v4");

const firebase = require("firebase");
require("./../../services/FirebaseInit").init(firebase);

/* TODO: From your host, Jonny
    1. If an add or change clashes with anything that already exists - Done! I think...
    2. If start of an add is after the end of the add - buggy but mostly fixed
    3.     "     but for an edit - see #1
        Sub-point of #1-3: disallow confirming changes until all collisions are fixed? - addressed
*/

/* Session Logic
Each session has uid

Pulls from recurring, additions, and deletions.

-- On select of new day D
Pull all S existing/recurring, additions, & deletions where S is in D.
Add to this [page] instance's edits.

-- On add new Session S
Create new session, S, to be added to additions

-- On delete of existing Session S
Add an entry to deletions of uid of target session, S

-- On change of existing Session S to new time
Add an entry to deletions of uid of target session, S, then
Create new session, S_1, to be added to additions.
  note:
  Could possibly doc(S.id).update for if target, S, is an addition

-- On confirm/submit
Update additions and deletions to mirror all info in current editing instance

*/

const dayFormat = "DDDD-YY";

export default class SetWeeklyHourModule extends Component {
  dbs = new DatabaseService();
  emailService = new EmailJsService();
  state = {
    // open Dialog
    openDialog: false,
    openConfirmSnackbar: false,
    openWarningSnackbar: false,
    messageWarningSnackbar: "",
    actionWarningSnackbar: () => {},
    closeWarningSnackbar: () => {},
    //selected date on calendar
    selectedDate: moment(),
    // to show the user: emulates what cache will look like on confirm
    cache: {},
    noErrors: true,
    noConflicts: true,
    conflicts: {},
    errors: {},
    // appts which will be deleted given the current schedule
    apptsToDelete: {},
    edtied: false
  };

  // loaded base sessions
  recurring = {
    M: {},
    T: {},
    W: {},
    R: {},
    F: {}
  };
  // any additions made while on the page
  additions = {};
  // any deletions made while on the page
  deletions = {};
  // any pending edits, will be applied on confirm?
  pendingEdits = {};
  // keeps track if each date's cache is edited or not (aka there exists an addition or deletion)
  dateIsEdited = {};

  constructor(props) {
    super(props);

    //TODO: load recurring and split them into days
    var scoped = this;
    this.dbs.getAllSessions(this.props.profId).then(querySnapshot => {
      if (querySnapshot.docs) {
        querySnapshot.docs.forEach(
          doc =>
            (this.recurring[doc.data().day][doc.id] = Object.assign(
              doc.data(),
              {
                id: doc.id
              }
            ))
        );
      }
      this.dateChanged(moment(), true);
    });
  }

  dateChanged = (newDate, b) => {
    const formattedNewDate = newDate.format(dayFormat);
    if (!this.state.cache[formattedNewDate]) {
      this.additions[formattedNewDate] = {};
      this.deletions[formattedNewDate] = {};

      // get addition for day and add
      //querySnapshot => looks like session vars {day: , start: , end: }
      var getAddsDonePromise = this.dbs
        .getAdditionsForDate(this.props.profId, newDate)
        .then(querySnapshot => {
          //check if there are any additions
          if (!querySnapshot.empty) {
            this.dateIsEdited[formattedNewDate] = true;
            querySnapshot.docs.forEach(
              doc =>
                (this.additions[formattedNewDate][doc.id] = Object.assign(
                  doc.data(),
                  { id: doc.id }
                ))
            );
          }
        });

      // get deletions for day and add
      //querySnapshot => {deleting : "an-id"}
      var getDelsDonePromise = this.dbs
        .getDeletionsForDate(this.props.profId, newDate)
        .then(querySnapshot => {
          //check if there are any deletions
          if (!querySnapshot.empty) {
            this.dateIsEdited[formattedNewDate] = true;
            querySnapshot.docs.forEach(
              doc => (this.deletions[formattedNewDate][doc.id] = doc.data())
            );
          }
        });

      // once additions and deletions logic is complete we update the cache accordingly
      Promise.all([getAddsDonePromise, getDelsDonePromise]).then(() => {
        this.refreshDate(newDate);
      });
    }
    this.setState({ selectedDate: newDate });
  };

  refreshDate(date) {
    const formattedDate = date.format(dayFormat);
    var newCache = { ...this.state.cache };
    newCache[formattedDate] = {
      ...this.recurring[formatDayCharacter(date)]
    };

    const todayAdditions = this.additions[formattedDate];
    for (var sessionId in todayAdditions) {
      //TODO: should additions be checked for id overlap (shouldnt happen but idk)
      newCache[formattedDate][sessionId] = todayAdditions[sessionId];
    }
    //run through deletions second and remove any
    const todayDeletions = this.deletions[formattedDate];
    for (var sessionId in todayDeletions) {
      var toDeleteId = todayDeletions[sessionId].deleting;
      if (newCache[formattedDate][toDeleteId]) {
        delete newCache[formattedDate][toDeleteId];
      }
    }
    // this.setState with new cache
    this.setState({ cache: newCache });
  }

  addClicked = () => {
    this.setState({ openDialog: true });
  };

  addSession(start, end, day) {
    //TODO: check if collides with existing sessions?
    //  check if uuid already been used (lol)
    var id = uuid();
    var dayString = day.format(dayFormat);

    this.dateIsEdited[dayString] = true;

    // add to addition
    this.additions[dayString][id] = {
      start: start,
      end: end,
      day: formatDayCharacter(day),
      id: id,
      isNew: true
    };
    // update day's cache
    var newCache = { ...this.state.cache };
    newCache[dayString][id] = { ...this.additions[dayString][id] };
    this.setState({ cache: newCache, edited: true }, this.checkConflicts);
  }

  deleteSession = (index, sessionId) => {
    if (!sessionId) {
      console.log("error in delete!");
      return;
    }
    var id = uuid();
    var formatString = this.state.selectedDate.format(dayFormat);

    this.dateIsEdited[formatString] = true;

    // if its a new addition, just delete it from the additions JSON
    var addition = this.additions[formatString][sessionId];
    if (addition && addition.isNew) {
      delete this.additions[formatString][sessionId];
      if (this.pendingEdits[sessionId]) {
        //remove from edits since no longer exists
        delete this.pendingEdits[sessionId];
      }
    }
    // otherwise add a deletion
    else {
      this.deletions[formatString][id] = {
        deleting: sessionId,
        isNew: true
      };
    }

    var newCache = { ...this.state.cache };
    if (newCache[formatString][sessionId]) {
      delete newCache[formatString][sessionId];
      this.setState({ cache: newCache, edited: true }, () => {
        this.forceUpdate();
        this.checkConflicts();
        this.checkAppointments();
      });
    }
  };

  checkAppointments = () => {
    const formatString = this.state.selectedDate.format(dayFormat);
    const dayCache = this.state.cache[formatString];

    this.state.apptsToDelete[formatString] = {};

    // get appointments for the day and see if they will be in bounds of after the change
    this.dbs
      .getProfessorApptsForDay(
        this.props.profId,
        formatDayCharacter(this.state.selectedDate)
      )
      .then(qSnapshot => {
        qSnapshot.docs.forEach(doc => {
          const { start_time, end_time } = doc.data();
          for (var id in dayCache) {
            if (
              this.doesSessionContainAppointment(dayCache[id], {
                start: this.formatTimestampToFirestoreDate(start_time),
                end: this.formatTimestampToFirestoreDate(end_time)
              })
            ) {
              return;
            }
          }
          //adds to to list if is not contained in remaining sessions
          this.state.apptsToDelete[formatString][doc.id] = doc.data();
        });
        this.setState({ apptsToDelete: this.state.apptsToDelete });
      });
  };

  editSession = (id, sender) => {
    //TODO:this stuff
    // // check if session is in recurring, if not check additions, if not track error
    //
    this.dateIsEdited[this.state.selectedDate.format(dayFormat)] = true;
    var partOf = "";
    var editedSession = this.recurring[
      formatDayCharacter(this.state.selectedDate)
    ][id];
    if (!editedSession) {
      editedSession = this.additions[this.state.selectedDate.format(dayFormat)][
        id
      ];
      if (!editedSession) {
        console.error("No session found with ID");
        return;
      } else {
        partOf = "additions";
      }
    } else {
      partOf = "recurring";
    }

    this.pendingEdits[id] = {
      start: this.toDumbFormat(sender.state.start),
      end: this.toDumbFormat(sender.state.end),
      day: formatDayCharacter(this.state.selectedDate),
      date: this.state.selectedDate.format(dayFormat),
      editing: id,
      partOf: partOf
    };

    const dayString = this.state.selectedDate.format(dayFormat);

    var newCache = { ...this.state.cache };
    var cacheDay = newCache[dayString];
    cacheDay[id].start = this.toDumbFormat(sender.state.start);
    cacheDay[id].end = this.toDumbFormat(sender.state.end);
    cacheDay[id].edited = true;

    this.setState(
      {
        cache: newCache,
        edited: true
      },
      () => {
        this.checkConflicts();
        this.checkAppointments();
      }
    );
  };

  confirmClicked = () => {
    // check if confirm button somehow does not disable
    if (!(this.state.noErrors && this.state.noConflicts)) {
      alert("Please fix your errors before saving!");
      return;
    }

    // stop them from saving again unless they make more changes
    this.setState({ edited: false });

    // get rid of caches which weren't actually edited at all
    var updatedCache = {};
    for (var date in this.state.cache) {
      if (this.dateIsEdited[date]) {
        updatedCache[date] = this.state.cache[date];
      }
    }

    // apply edits to adds if new, or del and new add if not new or rec
    for (var id in this.pendingEdits) {
      var edit = this.pendingEdits[id];

      if (edit.partOf === "additions") {
        var addition = this.additions[edit.date][id];
        if (addition && addition.isNew) {
          addition.start = edit.start;
          addition.end = edit.end;
          continue;
        }
      }

      if (
        (() => {
          for (var day in this.deletions) {
            for (var id in this.deletions[day]) {
              if (this.deletions[day][id].deleting == edit.editing) {
                return true;
              }
            }
          }
          return false;
        })()
      ) {
        continue;
      }

      //add to deletion then add new addition
      var id = uuid();
      this.deletions[edit.date][id] = {
        deleting: edit.editing,
        isNew: true
      };
      // add to addition
      id = uuid();
      this.additions[edit.date][id] = {
        start: edit.start,
        end: edit.end,
        day: edit.day,
        id: id,
        isNew: true
      };
      this.refreshDate(moment(edit.date, dayFormat));
    }

    var addUpdates = this.dbs.updateAdditions(
      this.props.profId,
      this.additions
    );
    var delUpdates = this.dbs.updateDeletions(
      this.props.profId,
      this.deletions
    );
    var cacheUpdates = this.dbs.updateCache(this.props.profId, updatedCache);
    var apptsUpdates = this.dbs.updateAppointments(
      this.props.profId,
      this.state.apptsToDelete
    );

    Promise.all([addUpdates, delUpdates, cacheUpdates, apptsUpdates]).then(
      () => {
        this.setState({ openConfirmSnackbar: true });
        const deletedApptStudentEmailPromises = Object.values(
          this.state.apptsToDelete
        ).flatMap(apptDays =>
          Object.values(apptDays).map(appt =>
            this.dbs.getStudentInfo(appt.student.id)
          )
        );

        Promise.all(deletedApptStudentEmailPromises).then(students => {
          const recipients = students.map(s => s.email);
          const message = `An appointment you have with ${
            this.props.profId
          } was deleted since they changed their office hours. Go to www.quick-queue.com/student/professor-office-hours/?professorId=${
            this.props.profId
          } to reschedule for another time.`;
          this.emailService.sendEmail(
            recipients,
            "Your appointment was deleted.",
            message
          );
        });
      }
    );
  };

  onDialogClose = (objectToSave = undefined) => {
    //TODO: CHANGE THIS
    this.setState({ openDialog: false });
    if (objectToSave) {
      this.addSession(
        this.toDumbFormat(objectToSave.start),
        this.toDumbFormat(objectToSave.end),
        this.state.selectedDate
      );
    }
  };

  receiveSessionErrorState = (id, internalError) => {
    var today = this.state.selectedDate.format(dayFormat);
    var newErrors = { ...this.state.errors };
    if (!newErrors[today]) {
      newErrors[today] = {};
    }
    newErrors[today][id] = false;
    for (var key in internalError) {
      if (internalError[key] === true) {
        newErrors[today][id] = true;
        break;
      }
    }
    if (newErrors[today][id]) {
      this.setState({ noErrors: false });
    } else {
      //checking if any errors
      var noErrors = true;
      for (var key in newErrors[today]) {
        if (newErrors[today][key] === true) {
          noErrors = false;
          break;
        }
      }
      this.setState({ noErrors: noErrors, errors: newErrors });
    }
  };

  checkConflicts() {
    var newCache = { ...this.state.cache };
    var dayString = this.state.selectedDate.format(dayFormat);
    var cacheDay = newCache[dayString];

    // conflict checking
    const dayKeys = Object.keys(cacheDay);
    var newConflicts = { ...this.state.conflicts };
    var newNoConflicts = true;
    newConflicts[dayString] = 0;

    for (var key in cacheDay) {
      cacheDay[key].error = false;
    }

    for (var i = 0; i < dayKeys.length; i++) {
      for (var j = i + 1; j < dayKeys.length; j++) {
        if (
          this.doSessionsOverlap(cacheDay[dayKeys[i]], cacheDay[dayKeys[j]])
        ) {
          newConflicts[dayString]++;
          cacheDay[dayKeys[i]].error = true;
          cacheDay[dayKeys[j]].error = true;
        }
      }
    }

    //for all days, if no conflicts, the newNoConflicts = true;
    for (var d in newConflicts) {
      if (newConflicts[d] > 0) {
        newNoConflicts = false;
        break;
      }
    }

    this.setState({
      conflicts: newConflicts,
      noConflicts: newNoConflicts,
      cache: newCache
    });
  }

  doSessionsOverlap(session1, session2) {
    return (
      (session1.end > session2.start && session1.start < session2.end) ||
      (session2.end > session1.start && session2.start < session1.end)
    );
  }

  doesSessionContainAppointment(session, appt) {
    return session.start <= appt.start && session.end >= appt.end;
  }

  formatTimestamp(timestamp) {
    return moment(timestamp.toDate()).format("h:mm a");
  }

  formatTimestampToFirestoreDate(timestamp) {
    return moment(timestamp.toDate()).format("HHmm");
  }

  formatFirestoreDate(dateString) {
    return moment(dateString, "HHmm").format("HH:mm");
  }

  toDumbFormat = timeString => moment(timeString, "HH:mm").format("HHmm");

  render() {
    var sessions = [];
    const day = this.state.cache[this.state.selectedDate.format(dayFormat)];
    if (day) {
      for (var sessionId in day) {
        sessions.push(day[sessionId]);
      }
    }
    var conflicts = [];
    for (var c in this.state.conflicts) {
      this.state.conflicts[c] > 0 &&
        conflicts.push(
          <li>
            {moment(c, dayFormat).format("MMMM Do")}: {this.state.conflicts[c]}
          </li>
        );
    }
    var errors = [];
    for (var d in this.state.errors) {
      if (Object.values(this.state.errors[d]).includes(true)) {
        errors.push(<li>{moment(d, dayFormat).format("MMM Do")}</li>);
      }
    }
    var apptsToDelete = [];
    for (var d in this.state.apptsToDelete) {
      const dayAppts = Object.values(this.state.apptsToDelete[d]);
      dayAppts.length &&
        apptsToDelete.push(
          <List>
            {dayAppts.map(appt => (
              <ListItem>
                <ListItemText
                  style={{ marginTop: "-15px", paddingTop: "0px" }}
                  primary={
                    this.formatTimestamp(appt.start_time) +
                    "-" +
                    this.formatTimestamp(appt.end_time)
                  }
                />
              </ListItem>
            ))}
          </List>
        );
    }
    return (
      <div className="SetWeeklyHourModule">
        <div id="module-root">
          <div id="module-left">
            <MuiPickersUtilsProvider utils={MomentUtils}>
              <span>
                <Paper id="calendar-root">
                  <Calendar
                    date={this.state.selectedDate}
                    onChange={this.dateChanged}
                    disablePast
                  />
                </Paper>
              </span>
            </MuiPickersUtilsProvider>
            <div id="buttons">
              <Button
                variant="contained"
                color="primary"
                disabled={
                  !(
                    this.state.noErrors &&
                    this.state.noConflicts &&
                    this.state.edited
                  )
                }
                onClick={this.confirmClicked}
              >
                Confirm
              </Button>
              <Button className="add" onClick={this.addClicked}>
                Add Session
              </Button>
            </div>
            <div id="errors">
              {!(this.state.noErrors && this.state.noConflicts) ? (
                <Typography
                  variant="subtitle2"
                  style={{ fontSize: "110%", marginTop: "5px" }}
                  color="error"
                >
                  You have errors that need to be resolved:
                </Typography>
              ) : null}
              {!this.state.noErrors ? (
                <div style={{ margin: "4px" }}>
                  <Typography variant="subtitle1">Invalid dates</Typography>
                  <ul>{errors.map(e => e)}</ul>
                </div>
              ) : null}
              {!this.state.noConflicts ? (
                <div style={{ margin: "4px" }}>
                  <Typography variant="subtitle1">Conflicts</Typography>
                  <ul id="conflict-list">{conflicts.map(c => c)}</ul>
                </div>
              ) : null}
              {apptsToDelete.length ? (
                <React.Fragment>
                  <Typography
                    variant="subtitle2"
                    style={{ fontSize: "110%", marginTop: "5px" }}
                  >
                    The current schedule excludes the following appointments:
                  </Typography>
                  <Typography variant="subtitle1" style={{ marginTop: "15px" }}>
                    {moment(d, dayFormat).format("MMM Do")}
                  </Typography>
                  {apptsToDelete}
                  <Typography variant="subtitle1">
                    <b>Note:</b> They will be deleted if not the schedule is not
                    modified.
                  </Typography>
                </React.Fragment>
              ) : null}
            </div>
          </div>
          <div id="module-right">
            {// says if it exists then do the mapping
            sessions.map((elem, index) => {
              return (
                <Session
                  key={"session" + index}
                  id={elem.id}
                  start={this.formatFirestoreDate(elem.start)}
                  end={this.formatFirestoreDate(elem.end)}
                  error={elem.error}
                  edited={elem.edited}
                  onEdit={this.editSession}
                  onDelete={this.deleteSession}
                  onUpdateErrorState={this.receiveSessionErrorState}
                  sessionId={elem.id}
                />
              );
            })}
          </div>
          <CustomDialog
            open={this.state.openDialog}
            handleClose={this.onDialogClose}
          />
          <SimpleSnackbar
            id="confirm-snackbar"
            open={this.state.openConfirmSnackbar}
            important
            msg="Sessions updated!"
          />
          <SimpleSnackbar
            id="warning-snackbar"
            open={this.state.openWarningSnackbar}
            important
            msg={this.state.messageWarningSnackbar}
            actionText="Undo"
            onAction={this.state.actionWarningSnackbar}
          />
        </div>
      </div>
    );
  }
}

class CustomDialog extends React.Component {
  info = {
    start: "12:00",
    end: "13:00"
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  //TODO: make sessions instead into two text fields and return their data on save

  render() {
    return (
      <div>
        <Dialog
          onClose={() => {
            this.props.handleClose();
          }}
          aria-labelledby="customized-dialog-title"
          open={this.props.open}
        >
          <DialogTitle
            id="customized-dialog-title"
            onClose={() => this.props.handleClose()}
          >
            Add a Session
          </DialogTitle>
          <DialogContent>
            <TextField
              label="Start"
              type="time"
              defaultValue={this.info.start}
              inputProps={{
                step: 300 // 5 min
              }}
              onChange={event => {
                this.info.start = event.target.value;
              }}
            />
            <TextField
              label="End"
              type="time"
              defaultValue={this.info.end}
              inputProps={{
                step: 300 // 5 min
              }}
              onChange={event => {
                this.info.end = event.target.value;
              }}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() =>
                this.props.handleClose({
                  start: this.info.start,
                  end: this.info.end
                })
              }
              color="primary"
            >
              Add
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
