import React, { Component } from "react";

import "../style/studentInLineCard.css";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";

import Typography from "@material-ui/core/Typography";

import DatabaseService from "./../../services/DatabaseService";
import parsingUtils from "./../../services/GetDataParser";
const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

export default class StudentInLineCard extends Component {
  state = {
    ticket_number: 0,
    queues: []
  };

  hasBecomeEmpty() {
    /*to override*/
  }

  constructor(props) {
    super(props);
    this.dbs = new DatabaseService();
    firebase
      .firestore()
      .collection("students")
      .doc(this.props.studId)
      .onSnapshot(querySnapshot => {
        this.updateScreen();
      });
  }

  updateScreen() {
    var finalized = [];
    let that = this;
    if (this.props.onBecomeEmpty) {
      this.hasBecomeEmpty = this.props.onBecomeEmpty;
    }
    this.dbs.getQueuesOfAStudent(this.props.studId, queues => {
      if (queues.length) {
        finalized = [...queues];
        for (const queue in queues) {
          this.dbs.getNameOfProf(queues[queue].data.professor, name => {
            finalized[queue].profName = name;
            this.setState({ queues: finalized });
          });
        }
      } else {
        this.setState({ queues: [] });
      }
    });
  }

  onFinishedClick = (profId, studId, queueId) => {
    this.dbs.deleteStudentFromQueue(profId, studId, queueId).then(() => {
      const newQueues = this.state.queues.filter(
        queue => queue.data.professor !== profId
      );
      this.setState({ queues: newQueues }, () => {
        if (
          this.props.profId &&
          !newQueues.some(queue => queue.data.professor === this.props.profId)
        ) {
          this.hasBecomeEmpty();
        }
      });
    });
  };

  render() {
    console.log(this.state);
    if (this.state.queues.length) {
      let children = [];
      children = this.state.queues
        .filter(queue => {
          if (this.props.profId) {
            return this.props.profId == queue.data.professor;
          } else {
            return true;
          }
        })
        .map(queue => (
          <span>
            <span id="place-head" style={{ paddingTop: "125px" }}>
              <Typography variant="h5" id="place-in-line">
                In line for {queue.profName}
              </Typography>
              <br />
              <Typography variant="h6" id="place-info">
                {"Ticket Number #" + queue.data.ticket_number}
              </Typography>
            </span>
            <span id="action-buttons">
              <Button
                className="outOfLine"
                onClick={() => {
                  this.onFinishedClick(
                    queue.data.professor,
                    this.props.studId,
                    queue.id
                  );
                }}
              >
                Get out of Line
              </Button>
            </span>
            <br />
          </span>
        ));
      return (
        <Card className="StudentInLineCard">
          <span children={children} />
        </Card>
      );
    } else {
      return "";
    }
  }
}
