import React, { Component } from "react";

import DatabaseService from "../../services/DatabaseService.js";
const firebase = require("firebase");
require("./../../services/FirebaseInit").init(firebase);

import Chip from "@material-ui/core/Chip";
import "../theme/reg-office-hours.css";

class TopicButton extends Component {
  state = {};
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <span>
        <Chip
          className="topicButton"
          label={this.props.name}
          onClick={() => this.props.onTopicSelect(this.props.name)}
        >
          {this.props.name}
        </Chip>
      </span>
    );
  }
}

export default TopicButton;
