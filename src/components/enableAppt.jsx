import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";

import DatabaseService from "./../../services/DatabaseService";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);
require("../style/enableAppt.css");
import theme from "../theme/appTheme";

class EnableAppts extends Component {
  state = {};

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    var scope = this;
    this.dbs.doesProfUseAppts(this.props.profId, shouldRenderAppts => {
      if (shouldRenderAppts) {
        scope.setState({ shouldCheck: true });
      } else {
        scope.setState({ shouldCheck: false });
      }
    });
  }

  changeState = () => {
    let that = this;
    let oldState = this.state.shouldCheck;
    if (oldState) {
      that.setState({ shouldCheck: false });
      that.dbs.changeProfUseAppts(that.props.profId, false);
    } else {
      that.setState({ shouldCheck: true });
      that.dbs.changeProfUseAppts(that.props.profId, true);
    }
  };

  render() {
    if (this.state.shouldCheck) {
      return (
        <span id="enableAppt" theme={theme}>
          <Typography variant="subtitle2">Appointments Enabled :</Typography>
          <Checkbox onChange={this.changeState} checked={true} />
          <br />
        </span>
      );
    } else {
      return (
        <span id="enableAppt" theme={theme}>
          <Typography variant="subtitle2">Appointments Enabled :</Typography>
          <Checkbox onChange={this.changeState} checked={false} />
          <br />
        </span>
      );
    }
  }
}

export default EnableAppts;
