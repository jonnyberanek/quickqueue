import React, { Component } from "react";

import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

import SetWeeklyHourModule from "./setWeeklyHoursModule.jsx";
import RegOfficeHours from "./reg-office-hours.jsx";

class EditHoursTypePicker extends React.Component {
  state = {
    value: 0
  };

  constructor(props) {
    super(props);
    this.tabs = [
      {
        label: "Modify Recurring",
        component: <RegOfficeHours profId={props.profId} />
      },
      {
        label: "Modify Occurrences",
        component: <SetWeeklyHourModule profId={props.profId} />
      }
    ];
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;

    return (
      <Paper square className="EditHoursTypePicker">
        <Tabs
          value={this.state.value}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          onChange={this.handleChange}
        >
          {this.tabs.map(tab => (
            <Tab label={tab.label} />
          ))}
        </Tabs>
        {this.tabs.map((tab, index) => (
          <div
            className="tab-content"
            style={{ display: this.state.value != index ? "none" : "block" }}
          >
            {tab.component}
          </div>
        ))}
      </Paper>
    );
  }
}
export default EditHoursTypePicker;
