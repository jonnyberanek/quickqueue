import React, { Component } from "react";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Chip from "@material-ui/core/Chip";
import Typography from "@material-ui/core/Typography";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

import "../style/appointments.css";
import "../style/makeAppt.css";

import SimpleSnackbar from "./SimpleSnackbar.jsx";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

import DatabaseService from "../../services/DatabaseService";
import formatDayCharacter from "../../services/FormatDayCharacter";

const dbs = new DatabaseService();

const moment = require("moment");

import parsingUtils from "../../services/GetDataParser";
var getData = parsingUtils.parseUrlGetData(window.location.href);

class MakeAppointment extends Component {
  start;
  date;
  end;
  dateToDisplay;
  startToDisplay;
  endToDisplay;

  constructor(props) {
    super(props);
    var date = new Date();
    var dateForTest;
    var currentDate;
    dbs.getLoggedInUser(getData.professorId[0], user => {
      this.setState({ profName: user.name });
    });
    if (getData.cacheId[0] != "") {
      firebase
        .firestore()
        .collection("professors")
        .doc(getData.professorId[0])
        .collection("cache")
        .doc(getData.cacheId[0])
        .collection("sessions")
        .doc(getData.sessionId[0])
        .get()
        .then(docSnapshot => {
          this.setTime(docSnapshot);
        });
    } else {
      firebase
        .firestore()
        .collection("professors")
        .doc(getData.professorId[0])
        .collection("sessions")
        .doc(getData.sessionId[0])
        .get()
        .then(docSnapshot => {
          this.setTime(docSnapshot);
        });
    }

    this.state = {
      profName: "",
      topicSuggestions: [],
      courseList: [],
      professorId: getData.professorId[0],
      course: "None",
      topic: "",
      student: "",
      snackbarIsOpen: false,
      snackbarMessage: "null"
    };

    dbs.getTopicsFromHistoricalVisits(getData.professorId[0], suggestions => {
      this.setState({ topicSuggestions: suggestions });
    });

    dbs.getAllProfsCourses(getData.professorId[0], courseList => {
      this.setState({
        courseList: courseList
      });
    });

    this.submitForm = this.submitForm.bind(this);
  }

  addColon(time) {
    return time[0] + time[1] + ":" + time[2] + time[3];
  }

  removeColon(time) {
    return time[0] + time[1] + time[3] + time[4];
  }

  formatTime(resultHour, resultMin) {
    return Number(resultHour) > 11
      ? (resultHour == "12" ? "12" : resultHour - 12) +
          ":" +
          resultMin +
          " p.m."
      : (resultHour == "00" ? "12" : resultHour) + ":" + resultMin + " a.m.";
  }

  setTime(docSnapshot) {
    var dateForTest = docSnapshot;
    if (getData.cacheId[0] == "") {
      switch (dateForTest.data().day) {
        case "M":
          this.date = moment().day(1);
          break;
        case "T":
          this.date = moment().day(2);
          break;
        case "W":
          this.date = moment().day(3);
          break;
        case "R":
          this.date = moment().day(4);
          break;
        case "F":
          this.date = moment().day(5);
          break;
        default:
      }
      this.dateToDisplay = this.date.format("dddd, MMMM Do, YYYY");
    } else {
      this.date = moment().dayOfYear(getData.cacheId[0].slice(0, 3));
      this.dateToDisplay = moment()
        .dayOfYear(getData.cacheId[0].slice(0, 3))
        .format("dddd, MMMM Do, YYYY");
    }
    this.start = dateForTest.data().start;
    this.startToDisplay = this.formatTime(
      this.start.slice(0, 2),
      this.start.slice(2, 4)
    );
    this.end = dateForTest.data().end;
    this.endToDisplay = this.formatTime(
      this.end.slice(0, 2),
      this.end.slice(2, 4)
    );
    let enteredHour = this.start[0] + this.start[1];
    let enteredMin = this.start[2] + this.start[3];
    var minutesEntered = Number(enteredHour) * 60 + Number(enteredMin) + 15;
    let initialEnd =
      String(
        parseInt(minutesEntered / 60) <= 9
          ? "0" + parseInt(minutesEntered / 60)
          : parseInt(minutesEntered / 60)
      ) +
      String(
        minutesEntered % 60 <= 9
          ? "0" + (minutesEntered % 60)
          : minutesEntered % 60
      );
    this.setState({
      date: this.date,
      start: this.addColon(this.start),
      end: this.addColon(initialEnd)
    });
  }

  validate() {
    return !(
      this.state.professorId == "" ||
      this.state.course == "" ||
      this.state.topic == "" ||
      this.state.student == ""
    );
  }

  componentDidMount() {
    dbs.getLoggedInUserID(this.props.email, userId => {
      this.setState({ student: userId });
    });
  }

  dateRangeOverlaps(a_start, a_end, b_start, b_end) {
    if (a_start.isSameOrBefore(b_start) && b_start.isBefore(a_end)) return true; // b starts in a
    if (a_start.isBefore(b_end) && b_end.isSameOrBefore(a_end)) return true; // b ends in a
    if (a_start.isSameOrBefore(b_start) && b_end.isSameOrBefore(a_end)) {
      //b in a
      return true;
    }
    return false;
  }

  submitForm() {
    var date = new Date();
    var that = this;
    var currentDate = date.toISOString();
    event.preventDefault();
    if (!this.validate()) {
      this.setState({
        snackbarIsOpen: true,
        snackbarMessage: "Fill all the fields"
      });
      return false;
    }
    dbs.getProfAppointments(this.state.professorId, appointmentList => {
      for (let appointment2 of appointmentList) {
        //if the appt is this week
        if (moment(appointment2[1]).isSameOrAfter(moment().day(1), "day")) {
          if (
            this.dateRangeOverlaps(
              moment(appointment2[1]),
              moment(appointment2[3]),
              moment(this.date.format("YYYY-MM-DD ") + this.state.start),
              moment(this.date.format("YYYY-MM-DD ") + this.state.end)
            )
          ) {
            //there is an overlap
            this.setState({
              snackbarIsOpen: true,
              snackbarMessage: "Professor is busy at that time."
            });
            return false;
          }
        }
      }
      if (this.removeColon(this.state.start) < this.start) {
        this.setState({
          snackbarIsOpen: true,
          snackbarMessage:
            "Time is invalid - Start time is before the beginning of the session"
        });
        return false;
      } else if (
        this.removeColon(this.state.start) > this.removeColon(this.state.end)
      ) {
        this.setState({
          snackbarIsOpen: true,
          snackbarMessage: "Time is invalid - Start time is after the end time"
        });
        return false;
      } else if (this.removeColon(this.state.end) > this.end) {
        this.setState({
          snackbarIsOpen: true,
          snackbarMessage:
            "Time is invalid - End time is after the end of the session"
        });
        return false;
      } else if (
        //moment.duration(x.diff(y))
        Number(
          moment.duration(
            moment(that.date.format("YYYY-MM-DD ") + that.state.end).diff(
              moment(that.date.format("YYYY-MM-DD ") + that.state.start),
              "minutes"
            )
          )
        ) > 15 ||
        Number(
          moment.duration(
            moment(that.date.format("YYYY-MM-DD ") + that.state.end).diff(
              moment(that.date.format("YYYY-MM-DD ") + that.state.start),
              "minutes"
            )
          )
        ) < 0
      ) {
        this.setState({
          snackbarIsOpen: true,
          snackbarMessage:
            "Time is invalid - Appointment is greater than 15 minutes"
        });
        return false;
      } else {
        let finalStart = moment(
          this.date.format("YYYY-MM-DD ") + this.state.start
        ).toDate();
        let finalEnd = moment(
          this.date.format("YYYY-MM-DD ") + this.state.end
        ).toDate();
        var data = {
          course: this.state.course == "None" ? "" : this.state.course,
          end: finalEnd,
          start: finalStart,
          student: this.state.student,
          topic: this.state.topic,
          id: getData.sessionId[0],
          day: formatDayCharacter(this.date)
        };
        dbs.pushAppointmentByDbRef(
          data,
          this.state.professorId,
          getData.cacheId[0],
          result => {
            this.setState({
              snackbarIsOpen: true,
              snackbarMessage: "Appointment successfully added! Redirecting..."
            });
            setTimeout(() => (window.location.href = "./../home"), 2000);
          }
        );
      }
    });
    return false;
  }

  handleDateTimeChanges = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };
  handleTopicChange = event => {
    this.setState({ topic: event.target.value });
  };
  handleChipClicked = label => {
    this.setState({ topic: label });
  };
  handleCourseChip = event => {
    event.persist();
    this.setState({ course: event.target.value });
  };

  render() {
    var topicChips = [];
    this.state.topicSuggestions.forEach(s => {
      var chip = (
        <Chip
          label={s}
          color={this.state.topic === s ? "primary" : "default"}
          onClick={() => this.handleChipClicked(s)}
          className="Chip"
        />
      );
      topicChips.push(chip);
    });

    let storeCourses = [],
      children = [];

    storeCourses.push(
      <FormControlLabel
        id={"None"}
        value={"None"}
        control={<Radio />}
        label={"None"}
      />
    );
    this.state.courseList.forEach(s => {
      var chip = (
        <FormControlLabel id={s} value={s} control={<Radio />} label={s} />
      );
      storeCourses.push(chip);
    });
    children.push(
      <span>
        <FormControl id="pickCourse" component="fieldset">
          <FormLabel component="legend">
            Select the course that you are in, if any
          </FormLabel>
          <RadioGroup id="pickCourse" onChange={this.handleCourseChip}>
            {storeCourses}
          </RadioGroup>
        </FormControl>
      </span>
    );

    return (
      <div className="Appointments">
        <Typography variant="h5" style={{ textAlign: "center" }}>
          Make Appointment for {this.state.profName}
        </Typography>
        <Typography variant="subheading" style={{ textAlign: "center" }}>
          on {this.dateToDisplay} {this.startToDisplay} - {this.endToDisplay}
        </Typography>
        <form
          onSubmit={this.submitForm}
          className={this.props.root}
          autoComplete="off"
        >
          <br />

          <br />
          <div style={{ width: "300px", flex: "wrap" }}>{children}</div>
          <br />
          <TextField
            id="datetime-local"
            label="Enter Start Time"
            type="time"
            value={this.state.start}
            className={this.props.textField}
            onChange={this.handleDateTimeChanges("start")}
            InputLabelProps={{
              shrink: true
            }}
            style={{ width: "130px" }}
          />
          <br />
          <br />
          <TextField
            id="datetime-local"
            label="Enter End Time"
            type="time"
            value={this.state.end}
            className={this.props.textField}
            onChange={this.handleDateTimeChanges("end")}
            InputLabelProps={{
              shrink: true
            }}
            style={{ width: "120px" }}
          />
          <br />
          <TextField
            id="Topic-name"
            label="Enter a Topic Name"
            className={this.props.textField}
            value={this.state.topic}
            onChange={this.handleTopicChange}
            margin="normal"
            inputProps={{ maxLength: 20 }}
          />
          <br />
          <div style={{ width: "300px", flex: "wrap" }}>{topicChips}</div>
          <br />
          <br />
          <Button
            onClick={this.submitForm}
            variant="contained"
            color="primary"
            className={this.props.button}
          >
            Confirm
          </Button>
        </form>
        <SimpleSnackbar
          open={this.state.snackbarIsOpen}
          important
          msg={this.state.snackbarMessage}
        />
      </div>
    );
  }
}

export default MakeAppointment;
