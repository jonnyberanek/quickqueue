import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

const spacing = { margin: "20 20 0 20" };

class LoginModule extends Component {
  inputData = {
    email: "",
    password: ""
  };

  formRef = React.createRef();

  login = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(this.inputData.email, this.inputData.password)
      .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        window.alert("Error : " + errorMessage);
      });
  };

  constructor(props) {
    super(props);
    document.addEventListener("submit", event => {
      event.preventDefault();
    });
  }

  render() {
    return (
      <Card>
        <form ref={this.formRef}>
          <TextField
            id="email"
            label="Email"
            variant="outlined"
            type="email"
            autoComplete="email"
            style={spacing}
            onChange={e => (this.inputData.email = e.target.value)}
          />
          <br />
          <TextField
            id="password"
            label="Password"
            variant="outlined"
            type="password"
            autoComplete="current-password"
            style={spacing}
            onChange={e => (this.inputData.password = e.target.value)}
          />
          <br />
          <Button
            variant="contained"
            color="primary"
            className="login-button"
            style={{ float: "right", margin: "20" }}
            onClick={() => this.login()}
            type="submit"
          >
            Log In
          </Button>
        </form>
      </Card>
    );
  }
}

export default LoginModule;
