import React, { Component } from "react";
import AppointmentItem from "./appointmentItem.jsx";
//import SVGIcon from "./baseline-calendar_today-24px.svg";
import SvgIcon from "@material-ui/core/SvgIcon";

import DatabaseService from "./../../services/DatabaseService";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

class CalendarButton extends Component {
  constructor(props) {
    super(props);
  }

  makeEvent = (prof, date, end) => {
    const ics = require("ics");
    let result = date.split(" ");
    let endResult = end.split(" ");

    //calculate the duration
    let startCalc = Number(result[3]) * 60 + Number(result[4]);
    let endCalc = Number(endResult[0]) * 60 + Number(endResult[1]);

    let durationOfEvent = endCalc - startCalc;
    const event = {
      //add 4 for the time because ics is weird?
      start: [
        Number(result[0]),
        Number(result[1]),
        Number(result[2]),
        Number(result[3]) + 4,
        Number(result[4])
      ],
      duration: {
        hours: parseInt(durationOfEvent / 60),
        minutes: durationOfEvent % 60
      },
      title: "Office Hour Appointment with " + prof
    };

    ics.createEvent(event, (error, value) => {
      if (error) {
        console.log(error);
        return;
      }
      window.location.href = "data:text/calendar;charset=utf8," + escape(value);
    });
  };

  render() {
    return (
      <SvgIcon
        onClick={() =>
          this.makeEvent(this.props.prof, this.props.date, this.props.end)
        }
      >
        <path d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V8h16v13z" />
      </SvgIcon>
    );
  }
}

export default CalendarButton;
