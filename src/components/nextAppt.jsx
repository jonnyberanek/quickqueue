import React, { Component } from "react";
import AppointmentItem from "./appointmentItem.jsx";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

import DatabaseService from "./../../services/DatabaseService";

const firebase = require("firebase");
require("../../services/FirebaseInit").init(firebase);

class NextAppt extends Component {
  state = {
    appt: [],
    shouldRender: false
  };

  dbs = new DatabaseService();

  constructor(props) {
    super(props);
    firebase
      .firestore()
      .collection("professors")
      .doc(this.props.profId)
      .onSnapshot(querySnapshot => {
        this.updateScreen();
      });

    firebase
      .firestore()
      .collection("professors")
      .doc(this.props.profId)
      .collection("appointments")
      .onSnapshot(querySnapshot => {
        this.updateScreen();
      });
  }

  updateScreen() {
    var scope = this;
    this.dbs.doesProfUseAppts(this.props.profId, appts => {
      if (appts) {
        this.dbs.getNextAppointment(this.props.profId, appt => {
          if (appt.length) {
            this.setState({
              appt: [
                appt[0][0],
                appt[0][1],
                appt[0][2],
                appt[0][3],
                appt[0][4]
              ],
              shouldRender: true
            });
          } else {
            this.setState({ appt: [], shouldRender: true });
          }
        });
      } else {
        this.setState({ appt: [], shouldRender: false });
      }
    });
  }

  deleteItem = id => {
    this.dbs.deleteStudentAppointment(this.props.profId, id).then(() => {
      this.updateScreen();
    });
  };

  render() {
    console.log(this.state);
    if (this.state.shouldRender) {
      if (this.state.appt.length == 0) {
        return (
          <span>
            <Typography
              variant="h6"
              style={{
                marginBottom: "48px",
                color: "darkgrey",
                fontSize: "110%"
              }}
            >
              No appointments scheduled.
            </Typography>
            <br />
          </span>
        );
      }
      return (
        <span>
          <List
            style={{
              maxWidth: "550px",
              minWidth: "450px",
              marginBottom: "48px"
            }}
          >
            <React.Fragment>
              <AppointmentItem
                id={this.state.appt[0]}
                start={this.state.appt[1]}
                topic={this.state.appt[2]}
                end={this.state.appt[3]}
                name={this.state.appt[4]}
                onDelete={this.deleteItem}
              />
            </React.Fragment>
          </List>
        </span>
      );
    } else {
      return (
        <span>
          <Typography
            variant="h6"
            style={{
              marginBottom: "48px",
              color: "darkgrey",
              fontSize: "110%"
            }}
          >
            Appointments not enabled.
          </Typography>
          <br />
        </span>
      );
    }
  }
}

export default NextAppt;
