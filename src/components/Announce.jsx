import React, { Component } from "react";
import {
  Checkbox,
  Grid,
  FormControl,
  Button,
  FormGroup,
  FormControlLabel,
  TextField,
  FormHelperText
} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import DatabaseService from "../../services/DatabaseService.js";
const dbs = new DatabaseService();
import EmailJsService from "../../services/EmailJsService";
import SimpleSnackbar from "./SimpleSnackbar.jsx";

var classEmails = {};

class Announce extends Component {
  state = {
    professorsCourses: [],
    openConfirmSnackbar: false,
    recipients: []
  };
  emailService = new EmailJsService();

  componentDidMount() {
    var final = [];
    // Fetch professors course here
    dbs.getAllProfsCourses(this.props.profId, data => {
      for (const d in data) {
        final.push([data[d], false]);

        // caches all emails of students in all of the prof's courses
        dbs.getAllMembersEmailOfCourse(data[d], emails => {
          classEmails[data[d]] = emails;
          console.log(classEmails);
        });
      }
      this.setState({ professorsCourses: final });
    });
  }

  onCheckBoxSelect = (value, courseId, index) => {
    let proffCourses = this.state.professorsCourses;
    proffCourses[index][1] = value;

    this.setState({ professorsCourses: proffCourses });

    // need to initialize recipients for first passthrough in flatMap
    let recipients = [];
    recipients = proffCourses.flatMap((course, index) => {
      if (course[1]) {
        // filter-some checks filters out email addresses that already exists within recipients
        return classEmails[course[0]].filter(
          addrToAdd => !recipients.some(addr => addr === addrToAdd)
        );
      } else return [];
    });
    this.setState({ recipients: recipients });
  };

  renderCheckBoxes = () => {
    let list = this.state.professorsCourses.map((course, index) => {
      return (
        <FormControlLabel
          key={course}
          control={
            <Checkbox
              //checked={course.checked}
              onChange={() =>
                this.onCheckBoxSelect(!course[1], course[0], index)
              }
              checked={course[1]}
            />
          }
          label={course[0]}
        />
      );
    });
    return list;
  };

  handleChangeSub = event => {
    this.setState({ subject: event.target.value });
  };

  handleChangeBody = event => {
    this.setState({ body: event.target.value });
  };

  render() {
    console.log(this.state.recipients);
    return (
      // Use grid for responsive design
      <Paper style={{ padding: "16px" }}>
        <div className="container">
          <FormControl>
            <Grid container direction="column">
              <Grid style={{ marginBottom: "10px" }}>
                <FormHelperText>
                  Which of your classes are being notified?
                </FormHelperText>
                <FormGroup
                  row
                  style={{ paddingLeft: "10px", paddingRight: "10px" }}
                >
                  {this.renderCheckBoxes()}
                </FormGroup>
              </Grid>
              <Grid style={{ marginBottom: "10px", width: "500px" }}>
                <TextField
                  className="subject"
                  placeholder="Subject"
                  fullWidth
                  onChange={this.handleChangeSub}
                />
              </Grid>
              <Grid style={{ marginBottom: "10px", width: "500px" }}>
                <TextField
                  id="standard-multiline-static"
                  placeholder="Type message to students here"
                  multiline
                  rows="10"
                  fullWidth
                  className="textField"
                  margin="dense"
                  onChange={this.handleChangeBody}
                />
              </Grid>
              <Grid>
                <Button
                  variant="contained"
                  color="primary"
                  disabled={this.state.recipients.length === 0}
                  onClick={() => {
                    this.emailService
                      .sendEmail(
                        this.state.recipients,
                        this.state.subject,
                        this.state.body
                      )
                      .finally(info => console.log(info));

                    this.setState({ openConfirmSnackbar: true });

                    setTimeout(
                      () => (window.location.href = "./../queue"),
                      2000
                    );
                  }}
                >
                  Send
                </Button>
              </Grid>
            </Grid>
          </FormControl>

          <SimpleSnackbar
            id="confirm-snackbar"
            open={this.state.openConfirmSnackbar}
            important
            msg="Message Sent!"
          />
        </div>
      </Paper>
    );
  }
}

export default Announce;
