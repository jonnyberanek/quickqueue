import React, { Component } from "react";

import "../style/studentInLineCard.css";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";

import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

import DatabaseService from "./../../services/DatabaseService";
import parsingUtils from "./../../services/GetDataParser";
import StudentInLineCard from "./studentInLineCard.jsx";

const firebase = require("firebase");
const moment = require("moment");
require("../../services/FirebaseInit").init(firebase);

export default class GetInLineCard extends Component {
  state = { people_in_line: 0 };

  dbs = new DatabaseService();
  getData = parsingUtils.parseUrlGetData(window.location.href);

  constructor(props) {
    super(props);
    var that = this;
    firebase
      .firestore()
      .collection("queue")
      .doc(that.getData.professorId[0])
      .collection("visits")
      .onSnapshot(querySnapshot => {
        that.updateCard();
      });
  }

  updateCard() {
    var that = this;
    this.dbs.getCurrentSessionWholeDoc(this.getData.professorId[0], session => {
      let workingDoc = session;
      console.log(workingDoc);
      let endComparison = moment(
        moment().format("MMM DD, YYYY") +
          " " +
          workingDoc.data().end[0] +
          workingDoc.data().end[1] +
          ":" +
          workingDoc.data().end[2] +
          workingDoc.data().end[3]
      );
      console.log(endComparison);
      var duration = moment.duration(endComparison.diff(moment()));
      var minutes = parseInt(duration.asMinutes());
      that.setState({
        time: minutes + " minutes remaining in the session"
      });
      that.dbs.getNumOfPeopleInLine(that.getData.professorId[0], number => {
        that.setState({ people_in_line: number, sessionId: workingDoc.id });
      });
    });
  }

  render() {
    console.log(this.state);
    return (
      <Card className="GetInLineCard">
        {!this.state.sessionId ? (
          <Typography variant="subtitle1" id="place-head">
            The professor does not currently have office hours. Make an
            appointment below
          </Typography>
        ) : (
          <React.Fragment>
            <span id="place-head">
              <Typography variant="h5" id="place-in-line">
                {this.state.people_in_line}
              </Typography>
              <Typography variant="h6" id="place-info">
                people in line
              </Typography>
            </span>
            <Typography variant="subtitle1">{this.state.time}</Typography>
            <br />
            <span id="action-buttons">
              <Button
                onClick={() =>
                  (window.location.href =
                    "/student/topic-and-course?professorId=" +
                    this.getData.professorId)
                }
              >
                Get in line
              </Button>
            </span>
          </React.Fragment>
        )}
      </Card>
    );
  }
}
