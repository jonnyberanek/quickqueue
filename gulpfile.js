"use strict;";

// dependencies
const gulp = require("gulp"),
  merge = require("merge-stream"),
  path = require("path"),
  glob = require("glob"),
  babelify = require("babelify"),
  browserify = require("browserify"),
  watchify = require("watchify"),
  uglify = require("gulp-uglify"),
  changed = require("gulp-changed"),
  source = require("vinyl-source-stream"),
  buffer = require("vinyl-buffer"),
  factor = require("factor-bundle"),
  concat = require("gulp-concat"),
  addsrc = require("gulp-add-src"),
  log = require("gulplog"),
  livereload = require("gulp-livereload"),
  jest = require("gulp-jest").default;

//add modules common to many files as necessary
var nodeModules = [
  "jquery",
  "firebase",
  "firebase/auth",
  "firebase/app",
  "react",
  "react-dom",
  "@material-ui/core"
];

function bundle(b, out) {
  return b

    .bundle()
    .on("error", log.error)
    .pipe(source("bundle.js"))
    .pipe(addsrc.prepend("public/common.js"))
    .pipe(buffer())
    .pipe(concat("bundle.js"))
    .pipe(gulp.dest(path.dirname(out)))
    .pipe(livereload());
}

// builds all index files to make ES2015 possible
gulp.task("build", () => {
  var files = glob.sync("./public/**/index*(.jsx|.js)");
  return merge(
    files.map(file => {
      // return browserify({
      //   entries: file,
      //   debug: true
      // })
      //   .external(nodeModules)
      //   .transform(require("browserify-css"))
      //   .transform(
      //     babelify.configure({
      //       presets: ["es2015", "react", "babel-preset-stage-1"]
      //     })
      //   )
      //   .bundle()
      //   .pipe(source("bundle.js"))
      //   .pipe(addsrc("public/common.js"))
      //   .pipe(buffer())
      //   .pipe(concat("bundle.js"))
      //   .pipe(gulp.dest(path.dirname(file)));
      return bundle(
        browserify({
          entries: file,
          debug: true
        })
          .external(nodeModules)
          .transform(require("browserify-css"))
          .transform(
            babelify.configure({
              presets: ["es2015", "react", "babel-preset-stage-1"]
            })
          ),
        file
      );
    })
  );
});

gulp.task("watch", () => {
  livereload.listen();
  var files = glob.sync("./public/**/index*(.jsx|.js)");
  files.map(file => {
    var bundler = watchify(
      browserify({
        entries: file,
        debug: true,
        cache: {},
        packageCache: {}
      })
    )
      .external(nodeModules)
      .transform(require("browserify-css"))
      .transform(
        babelify.configure({
          presets: ["es2015", "react", "babel-preset-stage-1"]
        })
      );
    bundler.on("update", () => {
      bundle(bundler, file);
    });
    bundler.on("log", info => {
      log.info(file + ": " + info);
    });
    bundle(bundler, file);
  });
});

//bundles external dependencies aka nodeModules
gulp.task("bundle-ext", () => {
  return browserify()
    .require(nodeModules)
    .bundle()
    .pipe(source("common.js"))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest("public"));
});

// uglifies all bundles to make them not terribly large
gulp.task("uglify", () => {
  var files = glob.sync("./public/**/bundle.js");
  return merge(
    files.map(file => {
      return gulp
        .src(file)
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest(path.dirname(file)));
    })
  );
});

gulp.task("jest", function() {
  return gulp.src("src/components/__test__").pipe(
    jest({
      preprocessorIgnorePatterns: [
        "<rootDir>/dist/",
        "<rootDir>/node_modules/"
      ],
      transformIgnorePatterns: ["<rootDir>/node_modules/"],
      transform: {
        "^.+\\.jsx?$": "babel-jest"
      },
      automock: false
    })
  );
});

// //builds files as they are modified
// gulp.task("watcher", () => {
//   gulp.watch(
//     ["services/**/*.js", "public/**/*.js", "!public/**/bundle.js"],
//     gulp.series("build")
//   );
// });

// //runs build and watcher together
// gulp.task("watch", gulp.series("build", "watcher"));

//default task to build common files the build index bundles
gulp.task("default", gulp.series("bundle-ext", "build"));
