# QuickQueue

1. [Getting Started](#getting-started)
2. [Coding Everything](#coding-everything)
3. [Testing and Deploying Code](#testing-and-deplying-code)

### Getting Started

1. Install npm to your computer (however you want)

2. Once installed, run the following commands anywhere:

```
npm install
npm i -g gulp --save-dev
npm i -g firebase-tools
firebase login
```

**Note:** Log in with your email address connected to the project.

You're set to start developing!

---

### Coding Everything

To start, make sure gulp is installed according to [Getting Started](#getting-started). Then run `gulp`

Run `gulp watch` to have changes to javascript be applied.

**Important!:** To have changes apply automatically, run the file: `watch.bat` or the command: `gulp watch`. Changes will update in the background as you save your work.

##### Troubleshooting: Does applying your code not work?

- <sup>A (imported) class or require module is not found:</sup>  
  &nbsp;&nbsp;&nbsp;&nbsp; run `gulp`

- <sup>Bundle.js doesn't exist / error in index.js:</sup>  
  &nbsp;&nbsp;&nbsp;&nbsp; run `gulp build`

- <sup>Upon `gulp build`, a package cannot be found:</sup>  
  &nbsp;&nbsp;&nbsp;&nbsp; run `npm install`

- <sup>An error shows up after running `gulp build`:</sup>  
  &nbsp;&nbsp;&nbsp;&nbsp; fix it in your code lol

- <sup>Your changes haven't been applied on page load:</sup>  
  &nbsp;&nbsp;&nbsp;&nbsp; Hard refresh (Ctrl+Shift+F5) or look at your running `gulp watch` script for errors in code

---

### Testing and Deploying Code

Depending on your goal, different commands are required.

First, navigate to your repository location in the terminal.

##### 1. Local Machine Testing

For dynamic testing to changes in the code, run `firebase serve`.
The site will be hosted locally and should update dynamically on code changes.

##### 2. Deploying to the Firebase Server

When a version is ready to go public, run `firebase deploy`.
The Hosting URL will be the public access url.
