const functions = require("firebase-functions");

const admin = require("firebase-admin");
admin.initializeApp();

const moment = require("moment");

function formatDayCharacter(date) {
  switch (date.day()) {
    case 0:
      return "U";
    case 1:
      return "M";
    case 2:
      return "T";
    case 3:
      return "W";
    case 4:
      return "R";
    case 5:
      return "F";
    case 6:
      return "S";
  }
}

exports.updateCaches = functions.https.onRequest((req, res) => {
  const professorId = req.query.profId;
  if (!professorId || !req.method === "PUT") {
    return res.send(400);
  }
  res
    .set("Access-Control-Allow-Origin", "*")
    .set("Access-Control-Allow-Methods", "GET, POST, PUT");
  const db = admin.firestore();
  var recurring = {};
  var cacheIds = [];
  var additions = {};
  var deletions = {};
  const recPromise = db
    .collection("professors")
    .doc(String(professorId))
    .collection("sessions")
    .get()
    .then(querySnapshot => {
      if (querySnapshot.empty) {
        // do something
      }
      querySnapshot.docs.forEach(doc => {
        recurring[doc.id] = Object.assign(doc.data(), {
          id: doc.id
        });
      });
      return;
    });

  const weeklyPromise = db
    .collection("professors")
    .doc(String(professorId))
    .collection("cache")
    .where(
      admin.firestore.FieldPath.documentId(),
      ">=",
      moment()
        .subtract(1, "day")
        .format("DDDD-yy")
    )
    .get()
    .then(querySnapshot => {
      cacheIds = querySnapshot.docs.map(doc => doc.id);
      return Promise.all(
        querySnapshot.docs
          .map(doc => [
            doc.ref
              .collection("additions")
              .get()
              .then(qs => {
                if (!qs.empty) {
                  additions[doc.id] = qs.docs.map(doc =>
                    Object.assign({ id: doc.id }, doc.data())
                  );
                }
                return false;
              }),
            doc.ref
              .collection("deletions")
              .get()
              .then(qs => {
                if (!qs.empty) {
                  Array.prototype.push.apply(
                    deletions,
                    qs.docs.forEach(
                      doc => (deletions[doc.data().deleting] = doc.data())
                    )
                  );
                }
                return false;
              }),
            doc.ref
              .collection("sessions")
              .get()
              .then(querySnapshot => {
                querySnapshot.docs.forEach(doc => {
                  return doc.ref.delete();
                });
                return false;
              })
          ])
          .reduce((arr, val) => arr.concat(val), []) //is essentially flatmap
      );
    })
    .catch(() => {
      return res.send(500);
    });

  return Promise.all([recPromise, weeklyPromise])
    .then(() => {
      var newCaches = {};
      cacheIds.forEach(id => {
        const dayChar = formatDayCharacter(moment(id, "DDDD-yy"));
        newCaches[id] = [];
        for (var key in recurring) {
          if (recurring[key].day === dayChar) {
            // if it DNE in deletions then...
            !deletions[key] && newCaches[id].push(recurring[key]);
          }
        }
        //if additions exist then...
        additions[id] &&
          Array.prototype.push.apply(
            newCaches[id],
            additions[id].filter(s => !deletions[s.id])
          );
      });
      var batch = db.batch();
      const outerColl = db
        .collection("professors")
        .doc(String(professorId))
        .collection("cache");

      cacheIds.forEach(id => {
        const innerColl = outerColl.doc(id).collection("sessions");
        newCaches[id].forEach(s => batch.set(innerColl.doc(s.id), s));
      });

      return batch.commit();
    })
    .then(() => {
      return res.send(200);
    })
    .catch(() => {
      return res.send(500);
    });
});
