import DatabaseService from "../../../services/DatabaseService.js";
import StudentAuthService from "../../../services/StudentAuthService.js";

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

import React from "react";
import ReactDOM from "react-dom";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

import StudNavBar from "../../../src/components/studNavBar.jsx";
import MakeAppointment from "../../../src/components/makeAppointment.jsx";

const $ = require("jquery");

var dbs = new DatabaseService(),
  user;

const authService = new StudentAuthService(firebase);

const domContainer = document.querySelector("#react-container");
// TODO: decide if keeping LogoutButton, used mostly for testing

function render(user) {
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <span>
        <StudNavBar />
        {user ? <MakeAppointment isStudent={true} email={user.email} /> : null}
      </span>
    </MuiThemeProvider>,
    domContainer
  );
}

render();

$(document).ready(function() {
  authService.onValidUserLoad(user => {
    render(user);
    $("#studentName").html(user.name);
  });
});
