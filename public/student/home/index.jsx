import DatabaseService from "../../../services/DatabaseService.js";
import StudentAuthService from "../../../services/StudentAuthService";

const firebase = require("firebase");
require("../../../services/FirebaseInit").init(firebase);

import React from "react";
import ReactDOM from "react-dom";

import StudentInLineCard from "../../../src/components/studentInLineCard.jsx";
import UpcomingApptCard from "../../../src/components/UpcomingApptCard.jsx";
import StudNavBar from "../../../src/components/studNavBar.jsx";
import ProfNameModule from "../../../src/components/profNameModule.jsx";
import StudentHome from "../../../src/components/homeStudent.jsx";
import SearchModule from "../../../src/components/searchModule.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

const $ = require("jquery");

var dbs, user;

var authService = new StudentAuthService(firebase);

const domContainer = document.querySelector("#react-container");
// TODO: decide if keeping LogoutButton, used mostly for testing

//TODO: figure out how to load which prof the student is in line for
function render(user) {
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <React.Fragment>
        <StudNavBar />
        {user ? <UpcomingApptCard studId={user.id} /> : null}
        {user ? <StudentInLineCard studId={user.id} /> : null}
        <StudentHome student={user} />
        <SearchModule />
      </React.Fragment>
    </MuiThemeProvider>,
    domContainer
  );
}

$(document).ready(function() {
  dbs = new DatabaseService();
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      user = firebase.auth().currentUser;
      dbs.getLoggedInUser(user.email, user => {
        render(user);
      });
      dbs.getName(user.email, function(name) {
        $("#studentName").html(name);
      });
    } else {
      // No user is signed in.
    }
  });
});
