import React from "react";
import ReactDOM from "react-dom";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

import StudentAuthService from "../../../services/StudentAuthService.js";

const $ = require("jquery");

const firebase = require("firebase/app");
require("../../../services/FirebaseInit").init(firebase);

import "./topic-and-course.css";

import TopicCourse from "../../../src/components/topic-and-course.jsx";
import StudNavBar from "../../../src/components/studNavBar.jsx";

const authService = new StudentAuthService(firebase);

//create the React element for this screen
const domContainer = document.querySelector("#topic-course-container");

function render(user) {
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <React.Fragment>
        <StudNavBar styling={{ marginRight: "15px" }} />
        {user ? <TopicCourse studId={user.id} /> : null}
      </React.Fragment>
    </MuiThemeProvider>,
    domContainer
  );
}

render();

//when the page is loaded, print the name of the user at the top
$(document).ready(function() {
  authService.onValidUserLoad(user => {
    render(user);
    $("#studentName").html(user.name);
  });
});
