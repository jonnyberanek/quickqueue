const firebase = require("firebase/app");

import DatabaseService from "../../../services/DatabaseService.js";
import StudentAuthService from "../../../services/StudentAuthService.js";

import React from "react";
import ReactDOM from "react-dom";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Star from "@material-ui/icons/Star";
import StarBorder from "@material-ui/icons/StarBorder";

import parsingUtils from "../../../services/GetDataParser";

import StudNavBar from "../../../src/components/studNavBar.jsx";
import StudentProfAppointmentTable from "../../../src/components/studentProfAppointmentTable.jsx";
import GetInLineCard from "../../../src/components/getInLineCard.jsx";
import TopicList from "../../../src/components/topicList.jsx";
import StudentInLineCard from "../../../src/components/studentInLineCard.jsx";
import Email from "../../../src/components/EmailIcon.jsx";
import FavIcon from "../../../src/components/favIcon.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

const $ = require("jquery");

//init firebase app
require("./../../../services/FirebaseInit").init(firebase);

var getData = parsingUtils.parseUrlGetData(window.location.href);
var dbs = new DatabaseService();
var studentId, nameOfProf, loggedInUser, isInLine;

const authService = new StudentAuthService(firebase);

class InLineCardSwapper extends React.Component {
  state = {
    inLine: true,
    inLineCard: null
  };
  onBecomeEmpty = () => {
    this.setState({ inLine: false, inLineCard: <GetInLineCard /> });
  };

  constructor(props) {
    super(props);
    this.state.inLine = this.props.inLine;
    this.state.inLineCard = <GetInLineCard />;
  }

  render() {
    return this.state.inLine ? (
      <StudentInLineCard
        studId={this.props.studId}
        profId={this.props.profId}
        onBecomeEmpty={this.onBecomeEmpty}
      />
    ) : (
      this.state.inLineCard
    );
  }
}

const domContainer = document.querySelector("#react-container");
const render = (user, inLine) => {
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <React.Fragment>
        <StudNavBar />

        {user ? (
          <FavIcon studId={user.id} profId={getData.professorId[0]} />
        ) : null}

        {user ? (
          <InLineCardSwapper
            inLine={inLine}
            studId={user.id}
            profId={getData.professorId[0]}
          />
        ) : null}
        <TopicList profId={getData.professorId[0]} />
        <br />
        <Typography variant="h6"> Professor Office Hours </Typography>
        <StudentProfAppointmentTable professorId={getData.professorId[0]} />
        <Email />
      </React.Fragment>
    </MuiThemeProvider>,
    domContainer
  );
};

render();

$(document).ready(function() {
  authService.onValidUserLoad(user => {
    $("#studentName").html(user.name);
    loggedInUser = user;
    dbs.getProfUsingID(getData.professorId[0], name => {
      nameOfProf = name;
      firebase
        .firestore()
        .collection("queue")
        .doc(getData.professorId[0])
        .collection("visits")
        .get()
        .then(querySnapshot => {
          return (
            !querySnapshot.empty &&
            querySnapshot.docs.some(doc => {
              if (doc.data().student.id == user.id) {
                return true;
              }
            })
          );
        })
        .then(inLine => {
          render(loggedInUser, inLine);
        });
    });
  });
});
