const $ = require("jquery");
const firebase = require("firebase");
import DatabaseService from "../../services/DatabaseService.js";

import "../../src/App.css";

import React from "react";
import ReactDOM from "react-dom";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../src/theme/appTheme";

import LoginModule from "../../src/components/loginModule.jsx";

var dbs = new DatabaseService();

//init firebase
require("./../../services/FirebaseInit").init(firebase);

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    var user = firebase.auth().currentUser;
    if (user != null) {
      dbs
        .getLoggedInUser(user.email, () => {})
        .then(user => {
          if (user.type === "student") {
            window.location.href = "../../student/home";
          } else if (user.type === "professor") {
            window.location.href = "../../professor/queue";
          } else {
            console.log("Type of user not found");
          }
        });
    }
  }
});

const domContainer = document.querySelector("#login-module-container");
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <span
      style={{
        margin: "auto",
        display: "table"
      }}
    >
      <h3
        style={{
          textAlign: "center"
        }}
      >
        Welcome to QUICKQUEUE
      </h3>
      <LoginModule />
    </span>
  </MuiThemeProvider>,
  domContainer
);
