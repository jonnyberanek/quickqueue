const firebase = require("firebase/app");
import DatabaseService from "../../../services/DatabaseService.js";

import React, { Component } from "react";
import ReactDOM from "react-dom";
import Radio from "@material-ui/core/Radio";

import ProfessorScreenContainer from "../../../src/components/professorScreenContainer.jsx";
import TopicGraph from "../../../src/components/TopicGraph.jsx";
import OfficeHourGraph from "../../../src/components/OfficeHourGraph.jsx";
import EditGraphTypePicker from "../../../src/components/graphTypePicker.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

var plotly = require("plotly")("smithdr1", "Z7jY2rVwPsW0PRKlwtKw");

const $ = require("jquery");

//init firebase app
require("./../../../services/FirebaseInit").init(firebase);
import "./style.css";

var dbs;

const domContainer = document.querySelector("#react-container");
ReactDOM.render(
  <React.Fragment>
    <MuiThemeProvider theme={theme}>
      <ProfessorScreenContainer
        render={state => <EditGraphTypePicker profId={state.user.id} />}
      />
    </MuiThemeProvider>
  </React.Fragment>,
  domContainer
);

$(document).ready(function() {
  dbs = new DatabaseService();
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      var user = firebase.auth().currentUser;
      dbs.getName(user.email, function(name) {
        //$("#studentName").html(name);
      });
    } else {
      // No user is signed in.
    }
  });
});
