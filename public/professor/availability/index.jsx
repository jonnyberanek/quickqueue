import React from "react";
import ReactDOM from "react-dom";

import ProfessorNavBar from "../../../src/components/professorNavBar.jsx";
import ProfessorScreenContainer from "../../../src/components/professorScreenContainer.jsx";
import Availability from "../../../src/components/studentAvailability.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

const domContainer = document.querySelector("#availability-container");
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <ProfessorScreenContainer
      render={state => (
        <span style={{ margin: "auto", display: "table" }}>
          <Availability profId={state.user.id} />
        </span>
      )}
    />
  </MuiThemeProvider>,
  domContainer
);
