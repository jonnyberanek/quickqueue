const firebase = require("firebase");

import DatabaseService from "../../../services/DatabaseService.js";

import "../../../src/App.css";
//import "bootstrap/dist/css/bootstrap.css";

import React from "react";
import ReactDOM from "react-dom";

import Queue from "../../../src/components/studentQueue.jsx";
import StudentAppointments from "../../../src/components/studentAppointments.jsx";
import NextAppt from "../../../src/components/nextAppt.jsx";
import EnableAppts from "../../../src/components/enableAppt.jsx";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

import ProfessorScreenContainer from "../../../src/components/professorScreenContainer.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

const $ = require("jquery");

//init firebase app
require("./../../../services/FirebaseInit").init(firebase);

var receiver;
var sender;
var dbs;

var studentIds = {};

const domContainer = document.querySelector("#queue-container");

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <ProfessorScreenContainer
      render={state => (
        <span style={{ margin: "auto", display: "table" }}>
          <h1 id="profName" class="mdc-typography--headline4" />
          <EnableAppts profId={state.user.id} />
          <br />
          <Typography variant="h5">Next Appointment:</Typography>
          <NextAppt profId={state.user.id} />

          <Typography variant="h5">Queue:</Typography>
          <Queue profId={state.user.id} />

          <Typography variant="h5">Upcoming Appointments:</Typography>
          <StudentAppointments profId={state.user.id} />
        </span>
      )}
    />
  </MuiThemeProvider>,
  domContainer
);

$(document).ready(function() {
  dbs = new DatabaseService();
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      var user = firebase.auth().currentUser;
      dbs.getName(user.email, function(name) {
        $("#profName").html("Queue for " + name);
      });
    }
  });
});
