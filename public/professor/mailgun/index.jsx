import React from "react";
import ReactDOM from "react-dom";

import ProfessorNavBar from "../../../src/components/professorNavBar.jsx";
import ProfessorScreenContainer from "../../../src/components/professorScreenContainer.jsx";
import Announce from "../../../src/components/Announce.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

const domContainer = document.querySelector("#react-container");
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <ProfessorScreenContainer
      render={state => (
        <span style={{ margin: "auto", display: "table" }}>
          <Announce profId={state.user.id} />
        </span>
      )}
    />
  </MuiThemeProvider>,
  domContainer
);

var myform = $("form#myform");
myform.submit(function(event) {
  event.preventDefault();

  // Change to your service ID, or keep using the default service
  var service_id = "default_service";
  var template_id = "contact_form";

  myform.find("button").text("Sending...");
  emailjs.sendForm(service_id, template_id, myform[0]).then(
    function() {
      alert("Sent!");
      myform.find("button").text("Send");
    },
    function(err) {
      alert("Send email failed!\r\n Response:\n " + JSON.stringify(err));
      myform.find("button").text("Send");
    }
  );
  return false;
});
