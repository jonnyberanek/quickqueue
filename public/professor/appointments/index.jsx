import React from "react";
import ReactDOM from "react-dom";

import ApptCalendar from "../../../src/components/apptCalendar.jsx";
import ProfessorScreenContainer from "../../../src/components/professorScreenContainer.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

const domContainer = document.querySelector("#appt-container");

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <ProfessorScreenContainer
      render={state => (
        <span style={{ margin: "auto", display: "table" }}>
          <ApptCalendar profId={state.user.id} />
        </span>
      )}
    />
  </MuiThemeProvider>,
  domContainer
);
