import React, { Component } from "react";
import ReactDOM from "react-dom";

import ProfessorScreenContainer from "../../../src/components/professorScreenContainer.jsx";
import EditHoursTypePicker from "../../../src/components/EditHoursTypePicker.jsx";

import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../../../src/theme/appTheme";

const firebase = require("firebase");

require("./../../../services/FirebaseInit").init(firebase);
import "./style.css";

const domContainer = document.querySelector("#react-container");

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <ProfessorScreenContainer
      render={state => <EditHoursTypePicker profId={state.user.id} />}
    />
  </MuiThemeProvider >,
  domContainer
);
